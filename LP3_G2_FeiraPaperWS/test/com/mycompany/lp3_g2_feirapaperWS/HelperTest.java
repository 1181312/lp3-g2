/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaperWS;

import entities.Client;
import entities.Order;
import entities.OrderLin;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import javax.ws.rs.core.Response;
import static org.junit.Assert.*;

/**
 *
 * @author tiagosantos
 */
public class HelperTest {
    
    public HelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    /**
     * Test of validarTextoComRegex method, of class Helper.
     */
    @Test
    public void testValidarTextoComRegex() {
        System.out.println("validarTextoComRegex");
        String strUsername = "3700-539";
        String regexValidator = "^\\d{4}(-\\d{3})?$";

        boolean result = helpers.Helper.validarTextoComRegex(strUsername, regexValidator);
        assertTrue(result);
    }
    
    /**
     * Test of validarInteger method, of class Helper.
     */
    @Test
    public void testValidarInteger() {
        System.out.println("validarInteger");
        String str = "3700";

        boolean result = helpers.Helper.validarInteger(str);
        assertTrue(result);
    }
    
    /**
     * Test of validarInteger method, of class Helper.
     */
    @Test
    public void testValidarNotInteger() {
        System.out.println("validarNotInteger");
        String str = "37o00";

        boolean result = helpers.Helper.validarInteger(str);
        assertFalse(result);
    }
    
    /**
     * Test of validarNome method, of class Helper.
     */
    @Test
    public void testValidarNome() {
        System.out.println("validarNome");
        String str = "Tiago Conner";

        boolean result = helpers.Helper.validarNome(str);
        assertTrue(result);
    }
    
    /**
     * Test of validarIdentificador method, of class Helper.
     */
    @Test
    public void testValidarIdentificador() {
        System.out.println("validarIdentificador");
        String str = "1234567";

        boolean result = helpers.Helper.validarIdentificador(str);
        assertTrue(result);
    }
    
    /**
     * Test of validarIdentificador method, of class Helper.
     */
    @Test
    public void testValidarNotIdentificador_1() {
        System.out.println("validarNotIdentificador_1");
        String str = "12345678";

        boolean result = helpers.Helper.validarIdentificador(str);
        assertFalse(result);
    }
    
    
    /**
     * Test of validarIdentificador method, of class Helper.
     */
    @Test
    public void testValidarNotIdentificador_2() {
        System.out.println("validarNotIdentificador_2");
        String str = "12E4567";

        boolean result = helpers.Helper.validarIdentificador(str);
        assertFalse(result);
    }
    
    /**
     * Test of ValidarCliente method, of class Helper.
     */
    @Test
    public void testValidarCliente() {
        System.out.println("ValidarCliente");
        
        entities.Client newclient = new Client();
        newclient.setNome("Teste Client Nome");
        newclient.setMorada1("testUnit_Client_Morada1");
        newclient.setMorada2("testUnit_Client_Morada2");
        newclient.setNif("testUnit_Client_Nif");
        newclient.setIdentificador(1233210);
        newclient.setIdPais(1);
        newclient.setCodPostal("0000-000");
        newclient.setAtivo(false);
        newclient.setEmail("email@email.com");
        
        String result = newclient.validar();

        assertEquals(result, "");   
    }
    
    /**
     * Test of ValidarEncomendasLinhas method, of class Helper.
     */
    @Test
    public void testValidarEncomendasLinhas() {
        System.out.println("ValidarEncomendasLinhas");
        
        entities.OrderLin line = new OrderLin();
        
        
        //PODERÁ DAR ERRO PORQUE O PRODUTO NÃO TEM QUANTIDADE SUFICIENTE
        line.setIdEncomenda(1);
        line.setIdIva(1);
        line.setIdMoeda(1);
        line.setIdProduto(16);
        line.setPrecoUni(0.0);
        line.setQuantidade(10);
        
        boolean result = line.validar();
        assertTrue(result);   
    }
    
    
    /**
     * Test of ValidarEncomendasLinhas method, of class Helper.
     */
    @Test
    public void testValidarEncomendas() {
        System.out.println("ValidarEncomendas");
        
        List<entities.OrderLin> lines= new ArrayList<entities.OrderLin>();
        entities.OrderLin line = new entities.OrderLin();
        line.setIdEncomenda(1);
        line.setIdIva(1);
        line.setIdMoeda(1);
        line.setIdProduto(16);
        line.setPrecoUni(0.0);
        line.setQuantidade(10);
        lines.add(line);
        
        
        entities.Order enc = new Order();
        enc.setData(new Date());
        enc.setIdCliente(1);
        enc.setNumEncomenda("1/2020");
        enc.setMoeda("EUR");
        enc.setPaisFaturacao(1);
        enc.setCodPostalFaturacao("0000-000");
        enc.setPaisEntrega(1);
        enc.setCodPostalEntrega("1111-111");
        enc.setEncomendasLinhas(lines);
        
        boolean result = enc.validar();
        assertTrue(result);   
    }
}
