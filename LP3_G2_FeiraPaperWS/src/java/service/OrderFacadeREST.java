/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.Order;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Hugo Costa (1181312)
 */
@Stateless
@Path("order")
public class OrderFacadeREST {

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response create(Order entity) {
        if (!entity.gravar()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(entity.getMessage())
                .type(MediaType.TEXT_PLAIN_TYPE).build();
        }
        
        return Response.ok("Successfully updated", MediaType.TEXT_PLAIN_TYPE).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Order> findAll() {
        
//        Order newOrder = new Order();
//        newOrder.setNumEncomenda("20200003");
//        
//        Calendar c = Calendar.getInstance();
//        c.set(2020, 1, 24);  
//        newOrder.setData(c.getTime());
//        newOrder.setIdCliente(33);
//        newOrder.setMoradaEntrega("Rua morada Entrega.");
//        newOrder.setPaisEntrega(1);
//        newOrder.setCodPostalEntrega("3720-000");
//        newOrder.setMoradaFaturacao("Rua morada Faturação.");
//        newOrder.setPaisFaturacao(1);
//        newOrder.setCodPostalFaturacao("3720-000");
//        
//        OrderLin newLin = new OrderLin();
//        newLin.setIdProduto(1);
//        newLin.setPrecoUni((Double) 1.25);
//        newLin.setQuantidade(10);
//        newLin.setIdIva(1);
//        newLin.setIdMoeda(1);
//        ArrayList<OrderLin> linhas = new ArrayList<>();
//        linhas.add(newLin);
//        
//        newOrder.setEncomendasLinhas(linhas);
//        newOrder.gravar();
        
        List<Order> retValue = Order.findAll();
        return retValue;
    }
}
