/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.Client;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Hugo Costa (1181312)
 */
@Stateless
@Path("client")
public class ClientFacadeREST {

    //@PersistenceContext(unitName = "LP3_G2_FeiraPaperWSPU")
    //private EntityManager em;
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response create(Client entity) {
        entity.setIdClient(0);
        String mensagem = entity.validar();
        if ("".equals(mensagem)) {
            mensagem = "Sucesso";
            Client.Criar(entity);
            return Response.status(Response.Status.OK).entity(mensagem).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(mensagem).build();
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response edit(@PathParam("id") int id, Client entity) {
        entity.setIdClient(id);
        String mensagem = entity.validar();
        if ("".equals(mensagem)) {
            mensagem = "Sucesso";
            Client.Alterar(id, entity);
            return Response.status(Response.Status.OK).entity(mensagem).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(mensagem).build();
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        Client.Eliminar(id);
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Client find(@PathParam("id") int id) {
        //return super.find(id);
        Client newClient = Client.find(id);        
        if (newClient.getIdClient() == -1) {
            return null;
        }
        return newClient;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Client> findAll() {
        //return super.findAll();
        List<Client> retValue = Client.findAll();
        return retValue;
    }

}
