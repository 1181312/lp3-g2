/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 
 */
public class ConnDB {

    private final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private final String hostName = "mail1.risi.pt";
    private final String dbName = "LP3G2";
    private final String userName = "sa";
    private final String password = "P1@2019.School";
    private final String url = String.format("jdbc:sqlserver://%s:9999;database=%s;user=%s;password=%s;"
            + "loginTimeout=30;", hostName, dbName, userName, password);

    public ConnDB() {
        try {
            Class.forName(driver).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ConnDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }

    public List executeQuery(String queryString) {
        List lst = null;

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            try {
                stmt = con.createStatement();
                rs = stmt.executeQuery(queryString);
                lst = resultSetToList(rs);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return lst;
    }

    private List<Map<String, Object>> resultSetToList(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, Object>> rows = new ArrayList<>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            rows.add(row);
        }
        return rows;
    }

    public boolean executeNonQuery(String queryString) {
        boolean sucess = false;

        Connection con = null;
        Statement stmt = null;

        try {
            con = getConnection();
            try {
                stmt = con.createStatement();
                int value = stmt.executeUpdate(queryString);
                sucess = true;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return sucess;
    }

    public int executeInsert(String queryString, ArrayList<String> parameters) {
        int key = 0;

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet keys = null;

        try {
            con = getConnection();
            try {
                pstmt = con.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);
                for (int i = 1; i <= parameters.size(); i++) {
                    pstmt.setString(i, parameters.get(i - 1));
                }

                pstmt.executeUpdate();
                keys = pstmt.getGeneratedKeys();

                if (keys.next()) {
                    key = keys.getInt(1);
                }

                if (keys != null) {
                    keys.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return key;
    }

    public boolean executeUpdate(String queryString, ArrayList<String> parameters) {
        boolean success = false;

        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = getConnection();
            try {
                pstmt = con.prepareStatement(queryString);
                
                for (int i = 1; i <= parameters.size(); i++) {
                    pstmt.setString(i, parameters.get(i - 1));
                }
                pstmt.executeUpdate();
                pstmt.close();
                
                if(pstmt != null) {
                    pstmt.close();
                }
                
                success = true;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return success;
    }
}
