/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Hugo Costa (1181312)
 */
public class Helper {
    
    
    public static final ConnDB con = new ConnDB();
    
    
    /**
     * Valida qualquer texto com base no regex enviado por parametro
     *
     * @param strUsername
     * @param regexValidator
     * @return
     */
    public static boolean validarTextoComRegex(String strUsername, String regexValidator) {
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }

    /**
     * Valida qualquer numero inteiro com base no texto enviado por parametro
     *
     * @param strNum
     * @return boolean
     */
    public static boolean validarInteger(String strNum) {
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    
    /**
     * Valida o nome do utilizador Aceita nomes"
     *
     * @param strUsername
     * @return
     */
    public static boolean validarNome(String strUsername) {
        String regexValidator = "(?=^.{2,255}$)^[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+(?:[ ](?:das?|dos?|de|e|[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+))*$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }
    
    /**
     * Valida o identificador
     *
     * @param strIdentificador
     * @return
     */
    public static boolean validarIdentificador(String strIdentificador) {
        boolean result = true;
        if (!validarInteger(strIdentificador)) {
            result = false;
        }
        if (strIdentificador.length() > 7) {
            result= false;
        }
        return result;
    }
}
