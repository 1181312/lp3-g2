/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author helder.oliveira
 */
@Entity
@XmlRootElement
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private int idEncomenda;
    private String numEncomenda;
    private Date data;
    private Integer idCliente;
    private String moradaEntrega;
    private String codPostalEntrega;
    private int paisEntrega;
    private String moradaFaturacao;
    private String codPostalFaturacao;
    private int paisFaturacao;
//    private Double valorTotal;
//    private Double valorIVA;
//    private Double percentagemIVA;
    private String moeda;
    private List<OrderLin> encomendasLinhas;
    private String message;

    public int getIdEncomenda() {
        return idEncomenda;
    }

    public void setIdEncomenda(int idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    public String getNumEncomenda() {
        return numEncomenda;
    }

    public void setNumEncomenda(String numEncomenda) {
        this.numEncomenda = numEncomenda;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getMoradaEntrega() {
        return moradaEntrega;
    }

    public void setMoradaEntrega(String moradaEntrega) {
        this.moradaEntrega = moradaEntrega;
    }

    public String getCodPostalEntrega() {
        return codPostalEntrega;
    }

    public void setCodPostalEntrega(String codPostalEntrega) {
        this.codPostalEntrega = codPostalEntrega;
    }

    public int getPaisEntrega() {
        return paisEntrega;
    }

    public void setPaisEntrega(int paisEntrega) {
        this.paisEntrega = paisEntrega;
    }

    public String getMoradaFaturacao() {
        return moradaFaturacao;
    }

    public void setMoradaFaturacao(String moradaFaturacao) {
        this.moradaFaturacao = moradaFaturacao;
    }

    public String getCodPostalFaturacao() {
        return codPostalFaturacao;
    }

    public void setCodPostalFaturacao(String codPostalFaturacao) {
        this.codPostalFaturacao = codPostalFaturacao;
    }

    public int getPaisFaturacao() {
        return paisFaturacao;
    }

    public void setPaisFaturacao(int paisFaturacao) {
        this.paisFaturacao = paisFaturacao;
    }

//    public Double getValorTotal() {
//        return valorTotal;
//    }
//
//    public void setValorTotal(Double valorTotal) {
//        this.valorTotal = valorTotal;
//    }
//
//    public Double getValorIVA() {
//        return valorIVA;
//    }
//
//    public void setValorIVA(Double valorIVA) {
//        this.valorIVA = valorIVA;
//    }
//
//    public Double getPercentagemIVA() {
//        return percentagemIVA;
//    }
//
//    public void setPercentagemIVA(Double percentagemIVA) {
//        this.percentagemIVA = percentagemIVA;
//    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public List<OrderLin> getEncomendasLinhas() {
        return encomendasLinhas;
    }

    public void setEncomendasLinhas(List<OrderLin> encomendasLinhas) {
        this.encomendasLinhas = encomendasLinhas;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean validar() {
        // Criar validações da ordem
        
        // Validar cliente
        if (this.idCliente == 0) {
            message = "Cliente inválido.";
            return false;
        }
        
        // Validar pais, codPostal entrega
        if (this.paisEntrega == 0) {
            message = "País de entrega inválido.";
            return false;
        } else if (this.codPostalEntrega == null || "".equals(this.codPostalEntrega)) {
            message = "Código postal de entrega inválida.";
            return false;
        }
        
        // Validar pais, codPostal faturação
        if (this.paisFaturacao == 0) {
            message = "País de faturação inválido.";
            return false;
        } else if (this.codPostalFaturacao == null || "".equals(this.codPostalFaturacao)) {
            message = "Código postal de faturação inválida.";
            return false;
        }
        
        // Validar número de linhas
        if (encomendasLinhas.isEmpty()) {
            message = "Nenhuma linha na encomenda.";
            return false;
        }
        // Validar todas as linhas
        for (int i = 0; i < encomendasLinhas.size(); i++) {
            if (!encomendasLinhas.get(i).validar()) {
                message = "Erro na linha de encomenda " + i + ".\n" + encomendasLinhas.get(i).getMessage();
                return false;
            }
        }

        return true;
    }

    public boolean gravar() {
        // VALIDAR ANTES DE GRAVAR
        if (!validar()) {
            return false;
        }

        helpers.ConnDB db = new helpers.ConnDB();
        Connection con = db.getConnection();

        String query = "EXEC spInserirEncomenda @idEncomenda = ?, @nrEnc = ?, @idCliente = ?, @moradaEntrega = ?, "
                + "@idPaisEntrega = ?, @codPostalEntrega = ?, @moradaFaturacao = ?, @idPaisFaturacao = ?, @codPostalFaturacao = ?";

        try {
            CallableStatement ps = con.prepareCall(query);
            ps.registerOutParameter(1, this.idEncomenda);
            ps.setString(2, this.numEncomenda);
            ps.setInt(3, this.idCliente);
            ps.setString(4, this.moradaEntrega);
            ps.setInt(5, this.paisEntrega);
            ps.setString(6, this.codPostalEntrega);
            ps.setString(7, this.moradaFaturacao);
            ps.setInt(8, this.paisFaturacao);
            ps.setString(9, this.codPostalFaturacao);

            ps.executeUpdate();
            
            ByteBuffer wrapped = ByteBuffer.wrap(ps.getBytes(1)); 
            int idEnc = wrapped.getInt();
            this.setIdEncomenda(idEnc);
            ps.close();

            if (con != null && !con.isClosed()) {
                con.close();
            }

        } catch (SQLException e) {
            this.message = e.getMessage();
            return false;
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }

        // GRAVAR LINHAS DA ENCOMENDA
        for (int i = 0; i < encomendasLinhas.size(); i++) {
            encomendasLinhas.get(i).setIdEncomenda(this.idEncomenda);
            if (!encomendasLinhas.get(i).gravar()) {
                this.message = "Erro ao gravar linha " + i + ".\n" + encomendasLinhas.get(i).getMessage();
                return false;
            }
        }

        return true;
    }
    
    
    public static ArrayList<Order> findAll() {
        ArrayList<Order> lst = new ArrayList();
        String sqlQuery = "SELECT idEncomenda, nrEnc, [data], idCliente, moradaEntrega, idPaisEntrega, codPostalEntrega, moradaFaturacao, idPaisFaturacao, codPostalFaturacao "
                + "FROM Encomendas ";
        
        helpers.ConnDB con = new helpers.ConnDB();
        List newlst = con.executeQuery(sqlQuery);
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                Order newItem = new Order();
                newItem.setIdEncomenda((int) ((Map<String, Object>) newlst.get(i)).get("idEncomenda"));
                newItem.setNumEncomenda((String) ((Map<String, Object>) newlst.get(i)).get("nrEnc"));
                newItem.setData((Date) ((Map<String, Object>) newlst.get(i)).get("data"));
                newItem.setIdCliente((int) ((Map<String, Object>) newlst.get(i)).get("idCliente"));
                newItem.setMoradaEntrega((String) ((Map<String, Object>) newlst.get(i)).get("moradaEntrega"));
                newItem.setPaisEntrega((int) ((Map<String, Object>) newlst.get(i)).get("idPaisEntrega"));
                newItem.setCodPostalEntrega((String) ((Map<String, Object>) newlst.get(i)).get("codPostalEntrega"));
                newItem.setMoradaFaturacao((String) ((Map<String, Object>) newlst.get(i)).get("moradaFaturacao"));
                newItem.setPaisFaturacao((int) ((Map<String, Object>) newlst.get(i)).get("idPaisFaturacao"));
                newItem.setCodPostalFaturacao((String) ((Map<String, Object>) newlst.get(i)).get("codPostalFaturacao"));
                newItem.setEncomendasLinhas(OrderLin.findAll(newItem.getIdEncomenda()));
                lst.add(newItem);
            }
        }
        con = null;
        return lst;
    }
}
