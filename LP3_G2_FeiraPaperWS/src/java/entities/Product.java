/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 *
 * @author ricardopinho
 */
@Entity
@XmlRootElement
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private int idProduto;
    private String codProduto;
    private String tipoPapel;
    private Integer quantidadeFolhas;
    private Double pesoProduto;
    private Double precoVenda;
    private List<ProductClient> clientes;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public String getCodProduto() {
        return codProduto;
    }

    public String getTipoPapel() {
        return tipoPapel;
    }

    public Integer getQuantidadeFolhas() {
        return quantidadeFolhas;
    }

    public Double getPesoProduto() {
        return pesoProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public void setTipoPapel(String tipoPapel) {
        this.tipoPapel = tipoPapel;
    }

    public void setQuantidadeFolhas(Integer quantidadeFolhas) {
        this.quantidadeFolhas = quantidadeFolhas;
    }

    public void setPesoProduto(Double pesoProduto) {
        this.pesoProduto = pesoProduto;
    }

    public Double getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(Double precoVenda) {
        this.precoVenda = precoVenda;
    }

    public void setClientes(List<ProductClient> clientes) {
        this.clientes = clientes;
    }

    public List<ProductClient> getClientes() {
        return clientes;
    }

    public static Product find(int idProduto) {
        helpers.ConnDB con = new helpers.ConnDB();

        Product item = null;

        List lst = con.executeQuery("EXEC spListarStock @idProduto = " + idProduto);
        if (lst != null && lst.size() > 0) {
            item = new Product();
            item.idProduto = (int) ((Map<String, Object>) lst.get(0)).get("idProduto");
            item.codProduto = (String) ((Map<String, Object>) lst.get(0)).get("CodProduto");
            item.tipoPapel = (String) ((Map<String, Object>) lst.get(0)).get("TipoPapel");
            item.quantidadeFolhas = (int) ((Map<String, Object>) lst.get(0)).get("QuantidadeFolhas");
            item.pesoProduto = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("peso").toString());
            item.precoVenda = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("pVenda").toString());
        }
        
        return item;
    }
    public static Product findPrice(int idProduto) {
        helpers.ConnDB con = new helpers.ConnDB();

        Product item = null;

        List lst = con.executeQuery("EXEC spListarStock @idProduto = " + idProduto);
        if (lst != null && lst.size() > 0) {
            item = new Product();
            item.idProduto = (int) ((Map<String, Object>) lst.get(0)).get("idProduto");
            item.codProduto = (String) ((Map<String, Object>) lst.get(0)).get("CodProduto");
            item.tipoPapel = (String) ((Map<String, Object>) lst.get(0)).get("TipoPapel");
            item.quantidadeFolhas = (int) ((Map<String, Object>) lst.get(0)).get("QuantidadeFolhas");
            item.pesoProduto = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("peso").toString());
            item.precoVenda = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("pVenda").toString());
            
            item.clientes = ProductClient.findAll(idProduto);
        }
        
        return item;
    }
    
    public static List<Product> findAll() {
        helpers.ConnDB con = new helpers.ConnDB();
        List<Product> rLst = new ArrayList();

        List lst = con.executeQuery("EXEC spListarStock");
        if (lst != null) {
            for (int i = 0; i < lst.size(); i++) {
                Product item = new Product();
                item.idProduto = (int) ((Map<String, Object>) lst.get(i)).get("idProduto");
                item.codProduto = (String) ((Map<String, Object>) lst.get(i)).get("CodProduto");
                item.tipoPapel = (String) ((Map<String, Object>) lst.get(i)).get("TipoPapel");
                item.quantidadeFolhas = (int) ((Map<String, Object>) lst.get(i)).get("QuantidadeFolhas");
                item.pesoProduto = Double.parseDouble(((Map<String, Object>) lst.get(i)).get("peso").toString());
                item.precoVenda = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("pVenda").toString());
                rLst.add(item);
            }
        }
        con = null;
        return rLst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProduto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if (this.idProduto != other.idProduto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Product[ idProduto=" + idProduto + " ]";
    }

}
