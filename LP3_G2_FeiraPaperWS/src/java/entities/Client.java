/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import helpers.Helper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hugo Costa (1181312)
 */
@Entity
@XmlRootElement
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idClient;

    private String nome;
    private String nif;
    private String morada1;
    private String morada2;
    private String codPostal;
    private Integer identificador;
    private boolean ativo;
    private Integer idPais;
    private String email;

    private static helpers.ConnDB con = new helpers.ConnDB();

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getMorada1() {
        return morada1;
    }

    public void setMorada1(String morada1) {
        this.morada1 = morada1;
    }

    public String getMorada2() {
        return morada2;
    }

    public void setMorada2(String morada2) {
        this.morada2 = morada2;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public Integer getIdentificador() {
        return this.identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Client() {
    }

    public Client(int id) {
        this.idClient = id;
        updateFromDBByID();
    }

    private void updateFromDBByID() {
        String sqlQuery = "SELECT idUtilizador, username, nome, morada1, morada2, nif, codPostal, identificador, ativo, email FROM Utilizadores WHERE idTipoUtilizador = 4 AND idUtilizador = " + this.idClient;
        List lst = con.executeQuery(sqlQuery);

        if (lst != null && lst.size() > 0) {
            this.idClient = (int) ((Map<String, Object>) lst.get(0)).get("idUtilizador");
            this.nome = (String) ((Map<String, Object>) lst.get(0)).get("nome");
            this.morada1 = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("morada1"), "");
            this.morada2 = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("morada2"), "");
            this.nif = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("nif"), "");
            this.codPostal = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("codPostal"), "");
            this.identificador = (Integer) ((Map<String, Object>) lst.get(0)).get("identificador");
            this.ativo = (Boolean) ((Map<String, Object>) lst.get(0)).get("ativo");
            this.email = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("email"), "");
        } else {
            this.idClient = -1;
        }
    }

    public String validar() {
        //VALIDAR NOME 
        if (nome == null || nome.isEmpty()) {
            return ("Obrigatório preencher o campo Nome!");
        } else {
            if (!Helper.validarNome(nome)) {
                return ("Nome inválido!");
            }
            if (nome.length() > 255) {
                return ("Nome inválido!");
            }
        }

        //VALIDAR MORADA
        if (morada1 == null || morada1.isEmpty()) {
            return ("Obrigatório preencher o campo Morada 1!");
        } else {
            if (morada1.length() > 255) {
                return ("Morada 1 inválida!");
            }
        }
        if (((morada2 != null)) && (!morada2.isEmpty())) {
            if (morada2.length() > 255) {
                return ("Morada 2 inválida!");
            }
        }

        //VALIDAR PAÍS
        String paisRegex = "";
        if (idPais == null || idPais == 0) {
            return ("Obrigatório preencher o campo do País!");
        } else {
            paisRegex = getPaisRegex(idPais);
            if (paisRegex == null || paisRegex.isEmpty()) {
                return ("País inválido! O país não foi encontrado na base de dados.");
            }
        }

        //VALIDAR CODIGO POSTAL
        if (codPostal == null || codPostal.isEmpty()) {
            return ("Obrigatório preencher o campo Código-Postal!");
        } else {
            if (codPostal.length() > 20) {
                return ("Código-Postal inválido!");
            }

            if (!paisRegex.isEmpty()) {
                if (!Helper.validarTextoComRegex(codPostal, paisRegex)) {
                    return ("Codigo Postal inválido!");
                }
            }
        }

        //VALIDAR NIF
        if (nif == null || nif.isEmpty()) {
            return ("Obrigatório preencher o campo NIF!");
        } else {
            if (nif.length() > 20) {
                return ("NIF inválido!");
            }
            if (idClient == 0) {
                if (Client.findAll("ativo = 1 AND idTipoUtilizador = 4 AND nif = '" + String.valueOf(nif) + "'").size() > 0) {
                    return ("NIF indicado já se encontra associado!");
                }
            } else {
                if (Client.findAll("ativo = 1 AND idTipoUtilizador = 4 AND nif = '" + String.valueOf(nif) + "' AND idUtilizador != " + String.valueOf(idClient)).size() > 0) {
                    return ("NIF indicado já se encontra associado!");
                }
            }
        }

        //VALIDAR IDENTIFICADOR
        if (identificador == null || identificador <= 0) {
            return ("Obrigatório preencher o campo identificador!");
        } else {
            if (!Helper.validarIdentificador(identificador.toString())) {
                return ("Identificador inválido!");
            }
            if (idClient == 0) {
                if (Client.findAll("ativo = 1 AND idTipoUtilizador = 4 AND  identificador = " + String.valueOf(identificador) + "").size() > 0) {
                    return ("Identificador indicado já se encontra associado!");
                }
            } else {
                if (Client.findAll("ativo = 1 AND idTipoUtilizador = 4 AND identificador = " + String.valueOf(identificador) + " AND idUtilizador != " + String.valueOf(idClient)).size() > 0) {
                    return ("Identificador indicado já se encontra associado!");
                }
            }
        }

        //VALIDAR EMAIL
        if (email == null || "".equals(email)) {
            return ("Email inválido");
        }
        
        return "";
    }

    public static Client find(int id) {
        Client retValue = new Client(id);
        return retValue;
    }

    public static ArrayList<Client> findAll() {
        return getLst("idTipoUtilizador = 4");
    }

    public static ArrayList<Client> findAll(String filter) {
        return getLst(filter);
    }

    public static ArrayList<Client> getLst(String filter) {
        ArrayList<Client> lst = new ArrayList();
        String sqlQuery = "SELECT idUtilizador, username, nome, morada1, morada2, nif, codPostal, identificador, ativo, email FROM Utilizadores" + (!filter.isEmpty() ? " WHERE " + filter : "");

        helpers.ConnDB con = new helpers.ConnDB();
        List newlst = con.executeQuery(sqlQuery);
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                Client newItem = new Client();
                newItem.setIdClient((int) ((Map<String, Object>) newlst.get(i)).get("idUtilizador"));
                newItem.setNome((String) ((Map<String, Object>) newlst.get(i)).get("nome"));
                newItem.setMorada1((String) ((Map<String, Object>) newlst.get(i)).get("morada1"));
                newItem.setMorada2((String) ((Map<String, Object>) newlst.get(i)).get("morada2"));
                newItem.setNif((String) ((Map<String, Object>) newlst.get(i)).get("nif"));
                newItem.setCodPostal((String) ((Map<String, Object>) newlst.get(i)).get("codPostal"));
                newItem.setIdentificador((Integer) ((Map<String, Object>) newlst.get(i)).get("identificador"));
                newItem.setAtivo((Boolean) ((Map<String, Object>) newlst.get(i)).get("ativo"));
                newItem.setEmail((String) ((Map<String, Object>) newlst.get(i)).get("email"));
                lst.add(newItem);
            }
        }
        con = null;
        return lst;
    }

    public static String getPaisRegex(int idPais) {
        String sqlQuery = "SELECT codPostalRegex FROM Paises WHERE idPais = " + String.valueOf(idPais);

        String CodPostalRegex = null;

        helpers.ConnDB con = new helpers.ConnDB();
        List newlst = con.executeQuery(sqlQuery);
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                CodPostalRegex = (String) ((Map<String, Object>) newlst.get(i)).get("codPostalRegex");
            }
        }
        con = null;
        return CodPostalRegex;
    }

    public static void Eliminar(long idUtilizador) {

        helpers.ConnDB db = new helpers.ConnDB();
        Connection con = db.getConnection();
        String query = "EXEC spInativarUtilizador @idUtilizador = ?";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setLong(1, idUtilizador);

            preparedStatement.executeUpdate();

            preparedStatement.close();

            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {

        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
    }

    public static void Criar(Client entity) {
        helpers.ConnDB db = new helpers.ConnDB();
        Connection con = db.getConnection();
        String query = "EXEC spInserirCliente @nome=?, @nif=?, @morada1=?, @morada2=?, @codPostal=?, @identificador=?, @email=?";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, entity.nome);
            preparedStatement.setString(2, entity.nif);
            preparedStatement.setString(3, entity.morada1);
            preparedStatement.setString(4, entity.morada2);
            preparedStatement.setString(5, entity.codPostal);
            preparedStatement.setInt(6, entity.identificador);
            preparedStatement.setString(7, entity.email);

            preparedStatement.executeUpdate();

            preparedStatement.close();

            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {

        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }

    }

    public static void Alterar(int id, Client entity) {
        helpers.ConnDB db = new helpers.ConnDB();
        Connection con = db.getConnection();
        String query = "EXEC spModificarUtilizador @idUtilizador=?, @nome=?, @nif=?, @morada1=?, @morada2=?, @codPostal=?, @identificador=?, @email=?";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, entity.nome);
            preparedStatement.setString(3, entity.nif);
            preparedStatement.setString(4, entity.morada1);
            preparedStatement.setString(5, entity.morada2);
            preparedStatement.setString(6, entity.codPostal);
            preparedStatement.setInt(7, entity.identificador);
            preparedStatement.setString(8, entity.email);

            preparedStatement.executeUpdate();

            preparedStatement.close();

            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {

        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }

    }

}
