/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hugo Costa (1181312)
 */
public class OrderLin {

    private int idEncomendaLinha;
    private int idEncomenda;
    private int idProduto;
    private double precoUni;
    private double quantidade;
    private int idIva;
    private int idMoeda;
    private String message;

    public int getIdEncomendaLinha() {
        return idEncomendaLinha;
    }

    public void setIdEncomendaLinha(int idEncomendaLinha) {
        this.idEncomendaLinha = idEncomendaLinha;
    }

    public int getIdEncomenda() {
        return idEncomenda;
    }

    public void setIdEncomenda(int idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public double getPrecoUni() {
        return precoUni;
    }

    public void setPrecoUni(double precoUni) {
        this.precoUni = precoUni;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public int getIdIva() {
        return idIva;
    }

    public void setIdIva(int idIva) {
        this.idIva = idIva;
    }

    public int getIdMoeda() {
        return idMoeda;
    }

    public void setIdMoeda(int idMoeda) {
        this.idMoeda = idMoeda;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean validar() {
        // Criar validações da linha

        // Quantidade maior que 0
//        if (this.idEncomenda <= 0) {
//            this.message = "Encomenda não foi identificada.";
//            return false;
//        }
        
        // Quantidade maior que 0
        if (this.quantidade <= 0) {
            this.message = "Quantidade inválida.";
            return false;
        }

        // IVA inválida
        if (this.idIva == 0) {
            this.message = "IVA inválido.";
            return false;
        }

        // Moeda inválida
        if (this.idMoeda == 0) {
            this.message = "Moeda inválida.";
            return false;
        }

        // Validar stock
        if (this.idProduto == 0) {
            this.message = "Produto inválido.";
            return false;
        } else {
            Product p = Product.find(this.idProduto);
            if (p == null || p.getIdProduto() == 0) {
                this.message = "Produto inválido.";
                return false;
            } else if (p.getQuantidadeFolhas() < this.quantidade) {
                this.message = "Quantidade em stock insuficiente para o produto " + p.getCodProduto();
                return false;
            }
        }

        return true;
    }

    public boolean gravar() {
        helpers.ConnDB db = new helpers.ConnDB();
        Connection con = db.getConnection();

        try {
            String query = "EXEC spInserirEncomendaLinha @idEncomenda = ?, @idProduto = ?, @precoUni = ?, @quantidade = ?, @idIva = ?, @idMoeda = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, this.getIdEncomenda());
            ps.setInt(2, this.getIdProduto());
            ps.setDouble(3, this.getPrecoUni());
            ps.setDouble(4, this.getQuantidade());
            ps.setInt(5, this.getIdIva());
            ps.setInt(6, this.getIdMoeda());

            ps.executeUpdate();
            ps.close();

            if (con != null && !con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {
            this.message = e.getMessage();
            return false;
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        return true;
    }

    public static ArrayList<OrderLin> findAll(int idEncomenda) {
        ArrayList<OrderLin> lst = new ArrayList();
        String sqlQuery = "SELECT idEncomendaLinha, idEncomenda, idProduto, precoUni, quantidade, idIva, idMoeda "
                + "FROM EncomendasLinhas "
                + "WHERE idEncomenda = " + idEncomenda;

        helpers.ConnDB con = new helpers.ConnDB();
        List newlst = con.executeQuery(sqlQuery);
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                OrderLin newItem = new OrderLin();
                newItem.setIdEncomendaLinha((int) ((Map<String, Object>) newlst.get(i)).get("idEncomendaLinha"));
                newItem.setIdEncomenda((int) ((Map<String, Object>) newlst.get(i)).get("idEncomenda"));
                newItem.setIdProduto((int) ((Map<String, Object>) newlst.get(i)).get("idProduto"));
                newItem.setPrecoUni(Double.parseDouble(((Map<String, Object>) newlst.get(i)).get("precoUni").toString()));
                newItem.setQuantidade(Double.parseDouble(((Map<String, Object>) newlst.get(i)).get("quantidade").toString()));
                newItem.setIdIva((int) ((Map<String, Object>) newlst.get(i)).get("idIva"));
                newItem.setIdMoeda((int) ((Map<String, Object>) newlst.get(i)).get("idMoeda"));
                lst.add(newItem);
            }
        }
        con = null;
        return lst;
    }
}
