/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author ricardopinho
 */
@Entity
public class ProductClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Client cliente;
    private Double precoVenda;

    public ProductClient(int idCliente, Double precoVenda) {
        this.cliente = new Client((int) idCliente);
        this.precoVenda = precoVenda;
    }

    public ProductClient() {
    }

    public Client getCliente() {
        return cliente;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public void setPrecoVenda(Double precoVenda) {
        this.precoVenda = precoVenda;
    }

    public Double getPrecoVenda() {
        return precoVenda;
    }

    public static List<ProductClient> findAll(int idProduto) {
        helpers.ConnDB con = new helpers.ConnDB();
        List<ProductClient> rLst = new ArrayList();

        List lst = con.executeQuery("EXEC spProdCliente @idProduto = " + idProduto);
        if (lst != null) {
            for (int i = 0; i < lst.size(); i++) {
            int idCliente;
            Double precoVenda;

            idCliente = (int) ((Map<String, Object>) lst.get(i)).get("idCliente");
            precoVenda = Double.parseDouble(((Map<String, Object>) lst.get(i)).get("precoVenda").toString());

            ProductClient item = new ProductClient(idCliente, precoVenda);
            rLst.add(item);
            }
        }
        return rLst;
    }


}
