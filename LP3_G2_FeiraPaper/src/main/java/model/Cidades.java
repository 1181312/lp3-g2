package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Cidades {
        
	private int idCidade;
	private String descricao;
	private int idPais;
        public static final String SQL_SELECT = "SELECT idCidade, idPais, descricao "
            + "FROM Cidades ";


	public int getIdCidade() {
		return this.idCidade;
	}

	/**
	 * 
	 * @param idCidade
	 */
	public void setIdCidade(int idCidade) {
		this.idCidade = idCidade;
	}

	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * 
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getIdPais() {
		return this.idPais;
	}

	/**
	 * 
	 * @param idPais
	 */
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
        
        // -------------------------
        // -- Métodos partilhados --
        // -------------------------
        public static ArrayList<Cidades> getLista() {
            return getLst("");
        }
        public static ArrayList<Cidades> getLista(String filter) {
            return getLst(filter);
        }
        /**
         * Retorna uma lista do tipo Cidades com base na informação que é retornada da base de dados
         * @param filter
         * @return 
         */
        private static ArrayList<Cidades> getLst(String filter) {
            ArrayList<Cidades> lst = new ArrayList();
            List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    Cidades newItem = new Cidades();
                    newItem.setIdCidade((int) ((Map<String, Object>) newlst.get(i)).get("idCidade"));
                    newItem.setIdPais((int) ((Map<String, Object>) newlst.get(i)).get("idPais"));
                    newItem.setDescricao((String) ((Map<String, Object>) newlst.get(i)).get("descricao"));
                    lst.add(newItem);
                }
            }
            return lst;
        }
        
    @Override
    public String toString() {
        return this.descricao;
    }

}