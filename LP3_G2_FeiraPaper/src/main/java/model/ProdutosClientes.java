/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

/**
 *
 * @author utilizador
 */
public class ProdutosClientes {

    private int idProduto;
    private int idCliente;
    private BigDecimal precoVenda;
    public static final String SQL_SELECT = "SELECT idProduto, idCliente, precoVenda" + "FROM ProdutosClientes";

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public BigDecimal getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(BigDecimal precoVenda) {
        this.precoVenda = precoVenda;
    }

    public static ArrayList<ProdutosClientes> getLista() {
        return getLst("");
    }

    public static ArrayList<ProdutosClientes> getLista(String filter) {
        return getLst(filter);
    }

    /**
     * Retorna uma lista do tipo ProdutosFornecedores com base na informação que
     * é retornada da base de dados
     *
     * @param filter
     * @return
     */
    private static ArrayList<ProdutosClientes> getLst(String filter) {
        ArrayList<ProdutosClientes> lst = new ArrayList();
        List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                ProdutosClientes newItem = new ProdutosClientes();
                newItem.setIdProduto((int) ((Map<String, Object>) newlst.get(i)).get("idProduto"));
                newItem.setIdCliente((int) ((Map<String, Object>) newlst.get(i)).get("idCliente"));
                newItem.setPrecoVenda((BigDecimal) ((Map<String, Object>) newlst.get(i)).get("precoVenda"));
                lst.add(newItem);
            }
        }
        return lst;
    }

    public boolean save() {
        boolean retValue = true;

        try {

            String query = "spProdutoClienteGuardar @idProduto=" + this.getIdProduto() + ", @idCliente=" + this.getIdCliente() + ", @precoVenda=" + this.getPrecoVenda().toString();
            //String query = String.format("spProdutoClienteGuardar @idProduto= %d, @idCliente=%d, @precoVenda=%s", this.getIdProduto(), this.getIdCliente(), this.getPrecoVenda().toString());
            Helper.con.executeQuery(query);
        } catch (Exception e) {
            Helper.showErrorAlert("Erro ao gravar " + e.getMessage());
            retValue = false;
        }

        return retValue;
    }

    public static class ListToClient {

        private int idProduto;
        private int idCliente;
        private BigDecimal precoVenda;
        private String descCliente;
        public static final String SQL_SELECT = "";

        public int getIdProduto() {
            return idProduto;
        }

        public void setIdProduto(int idProduto) {
            this.idProduto = idProduto;
        }

        public int getIdCliente() {
            return idCliente;
        }

        public void setIdCliente(int idCliente) {
            this.idCliente = idCliente;
        }

        public BigDecimal getPrecoVenda() {
            return precoVenda;
        }

        public void setPrecoVenda(BigDecimal precoVenda) {
            this.precoVenda = precoVenda;
        }

        public String getDescCliente() {
            return descCliente;
        }

        public void setDescCliente(String descCliente) {
            this.descCliente = descCliente;
        }

        public static ArrayList<ListToClient> getLista(int idProduto) {
            return getLst(idProduto);
        }

        private static ArrayList<ListToClient> getLst(int idProduto) {
            ArrayList<ListToClient> lst = new ArrayList();

            String query = "spProdCliente @idProduto={0}";
            query = query.replace("{0}", String.valueOf(idProduto));

            List newlst = Helper.con.executeQuery(query);
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    ListToClient item = new ListToClient();

                    item.setIdProduto((int) ((Map<String, Object>) newlst.get(i)).get("idProduto"));
                    item.setIdCliente((int) ((Map<String, Object>) newlst.get(i)).get("idCliente"));
                    item.setDescCliente((String) ((Map<String, Object>) newlst.get(i)).get("descCliente"));
                    item.setPrecoVenda((BigDecimal) ((Map<String, Object>) newlst.get(i)).get("precoVenda"));

                    lst.add(item);
                }
            }
            return lst;
        }

        public static void constructTblLinhas(TableView tblClientes, Button btn) {
            tblClientes.getColumns().clear();
            tblClientes.getItems().clear();
            tblClientes.setEditable(true);
            tblClientes.getSelectionModel().cellSelectionEnabledProperty().set(true);

            // idProduto --> HIDDEN
            TableColumn<ListToClient, Integer> colIdProduto = new TableColumn<>("idProduto");
            colIdProduto.setMinWidth(150);
            colIdProduto.setCellValueFactory(new PropertyValueFactory<>("idProduto"));
            colIdProduto.setVisible(false);
            colIdProduto.setEditable(false);

            // idCliente --> HIDDEN
            TableColumn<ListToClient, Integer> colIdCliente = new TableColumn<>("idCliente");
            colIdCliente.setMinWidth(150);
            colIdCliente.setCellValueFactory(new PropertyValueFactory<>("idCliente"));
            colIdCliente.setVisible(false);
            colIdCliente.setEditable(false);

            // descCliente
            TableColumn<ListToClient, String> colDescCliente = new TableColumn<>("Cliente");
            colDescCliente.setMinWidth(200);
            colDescCliente.setCellValueFactory(new PropertyValueFactory<>("descCliente"));
            colDescCliente.setEditable(false);

            // precoVenda
            TableColumn<ListToClient, BigDecimal> colprecoVenda = new TableColumn<>("Preco de Venda");
            colprecoVenda.setMinWidth(150);
            colprecoVenda.setCellValueFactory(new PropertyValueFactory<>("precoVenda"));
            colprecoVenda.setCellFactory(TextFieldTableCell.forTableColumn(new BigDecimalStringConverter() {
                @Override
                public BigDecimal fromString(String string) {
                    BigDecimal retValue = null;
                    try {
                        retValue = new BigDecimal(string);
                        if (retValue.compareTo(BigDecimal.ZERO) < 0) {
                            Helper.showErrorAlert("Não pode inserir valores inferores a 0.");
                            return BigDecimal.ZERO;
                        }
                    } catch (Exception e) {
                        Helper.showErrorAlert("Apenas pode inserir números.");
                    }
                    return retValue;
                }

            }));
            colprecoVenda.setOnEditStart((TableColumn.CellEditEvent<ListToClient, BigDecimal> event) -> {
                if (btn != null) {
                    btn.setDisable(true);
                }
            });
            colprecoVenda.setOnEditCancel((TableColumn.CellEditEvent<ListToClient, BigDecimal> event) -> {
                if (btn != null) {
                    btn.setDisable(true);
                }
            });
            colprecoVenda.setOnEditCommit((TableColumn.CellEditEvent<ListToClient, BigDecimal> t) -> {
                boolean cantSave = false;
                if (t.getNewValue() == null || t.getNewValue().compareTo(BigDecimal.ZERO) < 0) {
                    ((ListToClient) t.getTableView().getItems().get(t.getTablePosition().getRow())).setPrecoVenda(t.getOldValue());
                    cantSave = true;
                } else {
                    ((ListToClient) t.getTableView().getItems().get(t.getTablePosition().getRow())).setPrecoVenda(t.getNewValue());
                    cantSave = false;
                }
                if (btn != null) {
                    btn.setDisable(cantSave);
                }
            });

            colprecoVenda.setEditable(true);

            tblClientes.getColumns().addAll(colIdProduto, colIdCliente, colDescCliente, colprecoVenda);
        }

    }
}
