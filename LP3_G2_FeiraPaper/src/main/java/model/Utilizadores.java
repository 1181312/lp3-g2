package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.mycompany.lp3_g2_feirapaper.ConnDB;
import controller.ListaUtilizadoresController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

public class Utilizadores {

    private Integer idUtilizador = 0;
    private String username;
    private String hashKey;
    private String salt;
    private String nome;
    private String morada1;
    private String morada2;
    private Integer idCidade;
    private String codPostal;
    private Integer idTIpo;
    private boolean ativo;
    private Integer identificador;
    private String email; 
    public static final String SQL_SELECT = "SELECT idUtilizador,username,hashMD5,saltMD5,nome,morada1,morada2,idCidade,codPostal,idTipoUtilizador,ativo,identificador,email"
            + " FROM Utilizadores ";

    public Integer getIdUtilizador() {
        return this.idUtilizador;
    }

    public void setIdUtilizador(Integer id) {
        this.idUtilizador = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashKey() {
        return this.hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getSalt() {
        return this.salt;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada1() {
        return this.morada1;
    }

    public void setMorada1(String morada1) {
        this.morada1 = morada1;
    }

    public void setMorada2(String morada2) {
        this.morada2 = morada2;
    }

    public String getMorada2() {
        return this.morada2;
    }

    public Integer getIdCidade() {
        return this.idCidade;
    }

    public void setIdCidade(Integer idCidade) {
        this.idCidade = idCidade;
    }
    
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getCodPostal() {
        return this.codPostal;
    }

    public Integer getIdTIpo() {
        return this.idTIpo;
    }

    public void setIdTIpo(int idTIpo) {
        this.idTIpo = idTIpo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Integer getIdentificador() {
        return this.identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Utilizadores() {
        this.idUtilizador = 0;
    }

    /**
     * Função para carregar o utilizador com base no Username e password
     *
     * @param username
     * @param password
     */
    public Utilizadores(String username, String password) {
        this.idUtilizador = 0;
        this.username = username;
        getSaltByUsername();

        if (this.salt != null && !"".equals(this.salt)) {
            try {
                this.hashKey = Helper.cifrar(this.salt, password);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Utilizadores.class.getName()).log(Level.SEVERE, null, ex);
            }
            updateFromDBByAcesso();
        }

    }

    // -------------------------
    // -- Métodos partilhados --
    // -------------------------
    public static ArrayList<Utilizadores> getLista() {
        return getLst("");
    }

    public static ArrayList<Utilizadores> getLista(String filter) {
        return getLst(filter);
    }

    /**
     * Retorna uma lista do tipo Utilizadores com base na informação que é
     * retornada da base de dados
     *
     * @param filter
     * @return
     */
    private static ArrayList<Utilizadores> getLst(String filter) {
        ArrayList<Utilizadores> lst = new ArrayList();
        List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                Utilizadores newItem = new Utilizadores();
                newItem.setIdUtilizador((int) ((Map<String, Object>) newlst.get(i)).get("idUtilizador"));
                newItem.setUsername((String) ((Map<String, Object>) newlst.get(i)).get("username"));
                newItem.setHashKey((String) ((Map<String, Object>) newlst.get(i)).get("hashMD5"));
                newItem.setSalt((String) ((Map<String, Object>) newlst.get(i)).get("saltMD5"));
                newItem.setNome((String) ((Map<String, Object>) newlst.get(i)).get("nome"));
                newItem.setMorada1((String) ((Map<String, Object>) newlst.get(i)).get("morada1"));
                newItem.setMorada2((String) ((Map<String, Object>) newlst.get(i)).get("morada2"));
                newItem.setIdCidade((Integer) ((Map<String, Object>) newlst.get(i)).get("idCidade"));
                newItem.setCodPostal((String) ((Map<String, Object>) newlst.get(i)).get("codPostal"));
                newItem.setIdTIpo((int) ((Map<String, Object>) newlst.get(i)).get("idTipoUtilizador"));
                newItem.setAtivo((Boolean) ((Map<String, Object>) newlst.get(i)).get("ativo"));
                newItem.setIdentificador((Integer) ((Map<String, Object>) newlst.get(i)).get("identificador"));
                newItem.setEmail((String) ((Map<String, Object>) newlst.get(i)).get("email"));
                lst.add(newItem);
            }
        }
        return lst;
    }

    /**
     * Gravar utilizador
     */
    public void Gravar() {

        boolean atualizado = false;

        ConnDB db = new ConnDB();
        Connection con = db.getConnection();

        ResultSet keys = null;
        try {

            if (idUtilizador == 0) {

                String query = "EXEC spInserirUtilizador @username=?, @hash=?, @salt=?, @nome=?, @morada1=?, @morada2=?, @idCidade=?, @codPostal=?, @idTipoUtilizador=?, @identificador=?, @email=?";

                PreparedStatement preparedStatement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, this.username);
                preparedStatement.setString(2, this.hashKey);
                preparedStatement.setString(3, this.salt);
                preparedStatement.setString(4, this.nome);
                preparedStatement.setString(5, this.morada1);
                preparedStatement.setString(6, this.morada2);
                preparedStatement.setObject(7, this.idCidade);
                preparedStatement.setString(8, this.codPostal);
                preparedStatement.setInt(9, this.idTIpo);
                preparedStatement.setObject(10, this.identificador);
                preparedStatement.setString(11, this.email);

                preparedStatement.executeUpdate();
                
                preparedStatement.close();
            } else {

                String query = "EXEC spModificarUtilizador @idUtilizador=?, @nome=?, @morada1=?, @morada2=?, @idCidade=?, @codPostal=?, @identificador=?, @email=?";

                PreparedStatement preparedStatement = con.prepareStatement(query);
                preparedStatement.setInt(1, this.idUtilizador);
                preparedStatement.setString(2, this.nome);
                preparedStatement.setString(3, this.morada1);
                preparedStatement.setString(4, this.morada2);
                preparedStatement.setObject(5, this.idCidade);
                preparedStatement.setString(6, this.codPostal);
                preparedStatement.setObject(7, this.identificador);
                preparedStatement.setString(8, this.email);

                preparedStatement.executeUpdate();

                preparedStatement.close();
            }
        } catch (SQLException e) {
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
    }

    /**
     * Eliminar utilizador
     */
    public static void Eliminar(int idUtilizador) {
        ConnDB db = new ConnDB();
        Connection con = db.getConnection();
        String query = "EXEC spInativarUtilizador @idUtilizador = ?";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, idUtilizador);

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (SQLException e) {
            Logger.getLogger(ListaUtilizadoresController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
    }

    /**
     * Método para is buscar o salt com base no username
     */
    private void getSaltByUsername() {
        List lst = Helper.con.executeQuery("SELECT saltMD5 FROM Utilizadores WHERE username = '" + this.username + "'");
        if (lst != null && lst.size() > 0) {
            this.salt = (String) ((Map<String, Object>) lst.get(0)).get("saltMD5");
        }
    }

    /**
     * Método para ir buscar o utilizador com base no username e hashkey
     */
    private void updateFromDBByAcesso() {
        //TODO: ALTERAR FORMA DE VALIDAÇÃO DA PASSWORD
        List lst = Helper.con.executeQuery(SQL_SELECT
                + " WHERE username = '" + this.username + "' AND hashMD5 = '" + this.hashKey + "'");

        if (lst != null && lst.size() > 0) {
            this.idUtilizador = (Integer) ((Map<String, Object>) lst.get(0)).get("idUtilizador");
            this.username = (String) ((Map<String, Object>) lst.get(0)).get("username");
            this.nome = (String) ((Map<String, Object>) lst.get(0)).get("nome");
            this.morada1 = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("morada1"), "");
            this.morada2 = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("morada2"), "");
            this.idCidade = Integer.parseInt(Objects.toString(((Map<String, Object>) lst.get(0)).get("idCidade"), "0"));
            this.codPostal = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("codPostal"), "");
            this.idTIpo = Integer.parseInt(Objects.toString(((Map<String, Object>) lst.get(0)).get("idTipoUtilizador"), "0"));
            this.ativo = (Boolean) ((Map<String, Object>) lst.get(0)).get("ativo");
            this.identificador = (Integer) ((Map<String, Object>) lst.get(0)).get("identificador");
            this.email = (String) Objects.toString(((Map<String, Object>) lst.get(0)).get("email"), "");
        }
    }

    public static class UtilizadoresLista {

        private String Nome;
        private String Username;
        private String TipoUtilizador;
        private Integer idUtilizador;

        public String getNome() {
            return Nome;
        }

        public void setNome(String Nome) {
            this.Nome = Nome;
        }

        public String getUsername() {
            return Username;
        }

        public void setUsername(String Username) {
            this.Username = Username;
        }

        public String getTipoUtilizador() {
            return TipoUtilizador;
        }

        public void setTipoUtilizador(String TipoUtilizador) {
            this.TipoUtilizador = TipoUtilizador;
        }

        public Integer getIdUtilizador() {
            return this.idUtilizador;
        }

        public void setIdUtilizador(Integer id) {
            this.idUtilizador = id;
        }

        public static ArrayList<UtilizadoresLista> getListaUtilizadores(Integer TipoUtilizador) {
            ArrayList<UtilizadoresLista> lst = new ArrayList();
            ConnDB db = new ConnDB();
            Connection con = db.getConnection();
            String query = "spListarUtilizadores @TipoUtilizador=?";

            try {
                PreparedStatement preparedStatement = con.prepareStatement(query);
                preparedStatement.setInt(1, TipoUtilizador);

                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    UtilizadoresLista item = new UtilizadoresLista();
                    item.setIdUtilizador(rs.getInt("idUtilizador"));
                    item.setNome(rs.getString("Nome"));
                    item.setUsername(rs.getString("UserName"));
                    item.setTipoUtilizador(rs.getString("TipoUtilizador"));
                    lst.add(item);
                }
                preparedStatement.close();
            } catch (SQLException e) {

            } finally {
                try {
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                } catch (SQLException ex) {
                }
            }
            return lst;

        }
    }

    public static Integer FornecedorUpdate(Integer codFornecedor,
            String nome,
            String morada1,
            String morada2,
            String codPostal,
            String cidade,
            String pais,
            String ISO){
        
        Integer retValue = null;
        int indexQuery = 2;
        
        ConnDB db = new ConnDB();
        Connection con = db.getConnection();
        
        String query = "EXEC spFornecedorUpdate @codFornecedor = ?";
        if (nome != null) { query += ", @nome=?";}
        if (morada1 != null) { query += ", @morada1=?";}
        if (morada2 != null) { query += ", @morada2=?";}
        if (codPostal != null) { query += ", @codPostal=?";}
        if (cidade != null) { query += ", @cidade=?";}
        if (pais != null) { query += ", @pais=?";}
        if (ISO != null) { query += ", @ISO=?";}
        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, codFornecedor);
            if (nome != null) { preparedStatement.setString(indexQuery, nome); indexQuery++;}
            if (morada1 != null) { preparedStatement.setString(indexQuery, morada1); indexQuery++;}
            if (morada2 != null) { preparedStatement.setString(indexQuery, morada2); indexQuery++;}
            if (codPostal != null) { preparedStatement.setString(indexQuery, codPostal); indexQuery++;}
            if (cidade != null) { preparedStatement.setString(indexQuery, cidade); indexQuery++;}
            if (pais != null) { preparedStatement.setString(indexQuery, pais); indexQuery++;}
            if (ISO != null) { preparedStatement.setString(indexQuery, ISO); indexQuery++;}

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                retValue = rs.getInt("idUtilizador");
            }
                
            preparedStatement.close();
        } catch (SQLException e) {
            Logger.getLogger(Utilizadores.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
        
        return retValue;
    }
}
