package model;

import com.mycompany.lp3_g2_feirapaper.ConnDB;
import com.mycompany.lp3_g2_feirapaper.Helper;
import com.mycompany.lp3_g2_feirapaper.UnitConvert;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.Map;

public class Produtos {

    private int idProduto;
    private int idTipoFolha;
    private int idGramagem;
    private double peso;
    private String codProduto;
    private BigDecimal precoVenda;
    private ProdutosFornecedores produtoFornecedor;
    private ProdutosClientes produtoCliente;
    private ArrayList<ProdutosFornecedores> produtosFornecedores = new ArrayList<>();
    private ArrayList<ProdutosClientes> produtosCliente = new ArrayList<>();
    private boolean valid = false;
    public static final String SQL_SELECT = "SELECT idProduto, idTipoFolha, idGramagem, peso, codProduto, pVenda "
            + "FROM Produtos ";

    private boolean updatedBD = false;

    public int getIdProduto() {
        return this.idProduto;
    }

    /**
     *
     * @param idProduto
     */
    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public int getIdTipoFolha() {
        return this.idTipoFolha;
    }

    /**
     *
     * @param idTipoFolha
     */
    public void setIdTipoFolha(int idTipoFolha) {
        this.idTipoFolha = idTipoFolha;
    }

    public int getIdGramagem() {
        return this.idGramagem;
    }

    /**
     *
     * @param idGramagem
     */
    public void setIdGramagem(int idGramagem) {
        this.idGramagem = idGramagem;
    }

    public double getPeso() {
        return this.peso;
    }

    /**
     *
     * @param peso
     */
    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }
    
    public BigDecimal getPrecoVenda() {
        return this.precoVenda;
    }

    /**
     *
     * @param peso
     */
    public void setPrecoVenda(BigDecimal precoVenda) {
        this.precoVenda = precoVenda;
    }

    public ProdutosFornecedores getProdutoFornecedor(String idFornecedor) {
        if (!this.updatedBD) {
            if (idFornecedor.equals("")) {
                if (Helper.LoginUser != null && Helper.LoginUser.getIdentificador() != null) {
                    this.produtoFornecedor = new ProdutosFornecedores(this.idProduto, Integer.parseInt(Helper.LoginUser.getIdentificador().toString()));
                }
            } else {
                this.produtoFornecedor = new ProdutosFornecedores(this.idProduto, Integer.parseInt(idFornecedor));
            }
            this.updatedBD = true;
        }
        return produtoFornecedor;
    }

    public void setProdutoFornecedor(ProdutosFornecedores produtoFornecedor) {
        this.produtoFornecedor = produtoFornecedor;
    }

    public ArrayList<ProdutosFornecedores> getProdutosFornecedores() {
        return produtosFornecedores;
    }

    public void setProdutosFornecedores(ArrayList<ProdutosFornecedores> produtosFornecedores) {
        this.produtosFornecedores = produtosFornecedores;
    }
    public void setProdutosClientes(ProdutosClientes produtoCliente) {
        this.produtoCliente = produtoCliente;
    }
    public ArrayList<ProdutosClientes> getProdutosClientes() {
        return produtosCliente;
    }

    public void setProdutosClientes(ArrayList<ProdutosClientes> produtoCliente) {
        this.produtosCliente = produtosCliente;
    }
    public Produtos() {
    }

    public Produtos(int idProduto) {
        this.idProduto = idProduto;
        getProduto();
    }

    public Produtos(int idGramagem, int idTipoFolha, String idFornecedor) {
        this.idGramagem = idGramagem;
        this.idTipoFolha = idTipoFolha;
        getProdutoByGramagemTipoFolha(idFornecedor);
    }

    public static Produtos getProduto(int gramagem, int largura, int comprimento) {
        Produtos retValue = null;

        ConnDB db = new ConnDB();
        Connection con = db.getConnection();

        String query = "EXEC spProdutoByGramLargComp @gramagem=?, @largura=?, @comprimento=?";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, gramagem);
            preparedStatement.setInt(2, largura);
            preparedStatement.setInt(3, comprimento);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                retValue = new Produtos();
                retValue.setIdProduto(rs.getInt("idProduto"));
                retValue.setIdGramagem(rs.getInt("idGramagem"));
                retValue.setIdTipoFolha(rs.getInt("idTipoFolha"));
                retValue.setPeso(rs.getDouble("peso"));
            }

            preparedStatement.close();
        } catch (SQLException e) {
            Logger.getLogger(Produtos.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }

        return retValue;
    }

    private void getProduto() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idProduto = " + this.idProduto);
        if (lst != null && lst.size() > 0) {
            this.idProduto = (int) ((Map<String, Object>) lst.get(0)).get("idProduto");
            this.idGramagem = (int) ((Map<String, Object>) lst.get(0)).get("idGramagem");
            this.idTipoFolha = (int) ((Map<String, Object>) lst.get(0)).get("idTipoFolha");
            this.peso = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("peso").toString());
            this.codProduto = (String) ((Map<String, Object>) lst.get(0)).get("codProduto");
            this.precoVenda = Helper.StringToBigInteger(((Map<String, Object>) lst.get(0)).get("pVenda").toString());
            this.valid = true;

            getProdutoFornecedor("");
            this.setProdutosFornecedores(ProdutosFornecedores.getLista(String.format("idProduto = %d", this.idProduto)));
        }
    }

    private void getProdutoByGramagemTipoFolha(String idfornecedor) {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idGramagem = " + this.idGramagem + " AND idTipoFolha = " + this.idTipoFolha);
        if (lst != null && lst.size() > 0) {
            this.idProduto = (int) ((Map<String, Object>) lst.get(0)).get("idProduto");
            this.idGramagem = (int) ((Map<String, Object>) lst.get(0)).get("idGramagem");
            this.idTipoFolha = (int) ((Map<String, Object>) lst.get(0)).get("idTipoFolha");
            this.peso = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("peso").toString());
            this.codProduto = (String) ((Map<String, Object>) lst.get(0)).get("codProduto");
            this.precoVenda = Helper.StringToBigInteger(((Map<String, Object>) lst.get(0)).get("pVenda").toString());
            this.valid = true;

            getProdutoFornecedor(idfornecedor);
            this.setProdutosFornecedores(ProdutosFornecedores.getLista(String.format("idProduto = %d", this.idProduto)));
        }
    }

    public boolean isValid() {
        return this.valid;
    }

    public boolean save() {
        boolean retValue = true;
        
        atualizarPreco();

        //TODO: Criar mecanismo para gravar informação do produto
        for (ProdutosFornecedores item : this.produtosFornecedores) {
            if (!item.save()) {
                retValue = false;
                break;
            }
        }
        
        //TODO: Criar mecanismo para gravar informação do produto
        for (ProdutosClientes item : this.produtosCliente) {
            if (!item.save()) {
                retValue = false;
                break;
            }
        }

        return retValue;
    }
    
    public void atualizarPreco() {

        String query = "UPDATE Produtos SET pVenda=" + this.precoVenda.toString() +" WHERE idProduto=" + String.valueOf(this.idProduto);
        Helper.con.executeQuery(query);
        
    }
    
    public static class ListaStock {

        private String CodProduto;
        private String TipoPapel;
        private Integer QuantidadeFolhas;
        private Double PesoProduto;
        private BigDecimal pVenda;
        private Integer IdProduto;

        public String getCodProduto() {
            return CodProduto;
        }

        public String getTipoPapel() {
            return TipoPapel;
        }

        public String getQuantidadeFolhas() {
            return Helper.formatSheetsToString(UnitConvert.fromSheetsToAny(QuantidadeFolhas, PesoProduto));
        }

        public String getQuantidadeResmas() {
            return Helper.formatReamsToString(UnitConvert.fromSheetsToAny(QuantidadeFolhas, PesoProduto));
        }

        public String getQuantidadeKg() {
            return Helper.formatGramsToString(UnitConvert.fromSheetsToAny(QuantidadeFolhas, PesoProduto));
        }

        public String getPVenda() {
            return Helper.formatPriceToString(pVenda);
        }

        public Integer getIdProduto() {
            return IdProduto;
        }

        public static ArrayList<ListaStock> getListaStock() {
            ArrayList<ListaStock> lst = new ArrayList();
            ConnDB db = new ConnDB();
            Connection con = db.getConnection();
            String query = "spListarStock";

            try {
                PreparedStatement preparedStatement = con.prepareStatement(query);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    ListaStock item = new ListaStock();
                    item.IdProduto = (rs.getInt("idProduto"));
                    item.CodProduto = (rs.getString("CodProduto"));
                    item.TipoPapel = (rs.getString("TipoPapel"));
                    item.QuantidadeFolhas = (rs.getInt("QuantidadeFolhas"));
                    item.pVenda = (rs.getBigDecimal("pVenda"));
                    item.PesoProduto = (rs.getDouble("Peso"));
                    lst.add(item);
                }
                preparedStatement.close();
            } catch (SQLException e) {

            } finally {
                try {
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                } catch (SQLException ex) {
                }
            }
            return lst;

        }
    }
}
