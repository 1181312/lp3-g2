/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mycompany.lp3_g2_feirapaper.ConnDB;
import com.mycompany.lp3_g2_feirapaper.Helper;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.CheckBox;

/**
 *
 * @author tiagosantos
 */
public class Documentos {

    private int idDocumento;
    private String nrDoc;
    private Date data;
    private int idFornecedor;

    public static final String SQL_SELECT = "SELECT idDocumento, nrDoc, data, idFornecedor"
            + "FROM Documentos ";

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNrDoc() {
        return nrDoc;
    }

    public void setNrDoc(String nrDoc) {
        this.nrDoc = nrDoc;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public static Integer DocumentoInsert(String codDocumento,
            Integer idFornecedor,
            Integer dia,
            Integer mes,
            Integer ano,
            File ficheiro) {
        Integer retValue = null;

        ConnDB db = new ConnDB();
        Connection con = db.getConnection();

        String query = "EXEC spDocumentoInsert @codDocumento = ?, @idFornecedor=?, @dia=?, @mes=?, @ano=?, @ficheiro=?";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, codDocumento);
            preparedStatement.setInt(2, idFornecedor);
            preparedStatement.setInt(3, dia);
            preparedStatement.setInt(4, mes);
            preparedStatement.setInt(5, ano);
            
            FileInputStream fis = new FileInputStream(ficheiro);
            int len = (int)ficheiro.length();

            preparedStatement.setBinaryStream(6, fis, len); 
            
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                retValue = rs.getInt("idDocumento");
            }

            preparedStatement.close();
        } catch (SQLException e) {
            Logger.getLogger(Documentos.class.getName()).log(Level.SEVERE, null, e);
        }catch (Exception ex){
            Logger.getLogger(Documentos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }

        return retValue;
    }

    public static class DocumentoXML {

        public String OrderConfirmationReference;
        public Integer Date_Year;
        public Integer Date_Month;
        public Integer Date_Day;
        public Integer SupplierParty_PartyIdentifier;
        public String SupplierParty_Name;
        public String SupplierParty_Address1;
        public String SupplierParty_Address2;
        public String SupplierParty_City;
        public String SupplierParty_PostalCode;
        public String SupplierParty_Country;
        public String SupplierParty_Country_ISO;
        public ArrayList<DocumentosLinhas.DocumentosLinhasXML> OrderConfirmationLineItem;

    }

    public static class ListaDocumentos {

        private Documentos Documento;
        private ArrayList<DocumentosLinhas.ListaDocumentosLinhas> documentosLinhas;
        private String Fornecedor;
        private Date Data;
        private boolean selected;
        public static final int EN_LISTA_DOCUMENTOS = 1;
        public static final int EN_LISTA_DOCUMENTOS_PENDENTES = 2;
        public static final int EN_LISTA_DOCUMENTOS_CONTACORRENTE = 3;
        public static final int EN_LISTA_ENCOMENDAS_CONTACORRENTE = 4;

        public Documentos getDocumento() {
            return Documento;
        }

        public Integer getIdDocumento() {
            return Documento.getIdDocumento();
        }

        public String getNrDoc() {
            return String.valueOf(Documento.getNrDoc());
        }

        public String getIdFornecedor() {
            return String.valueOf(Documento.getIdFornecedor());
        }

        public ArrayList<DocumentosLinhas.ListaDocumentosLinhas> getDocumentosLinhas() {
            return documentosLinhas;
        }

        public String getFornecedor() {
            return Fornecedor;
        }

        public String getData() {
            return String.valueOf(Data);
        }

        public boolean getSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public void validar(boolean validar) {
            for (DocumentosLinhas.ListaDocumentosLinhas item : this.getDocumentosLinhas()) {
                item.validar(validar);
            }
        }

        public static ArrayList<ListaDocumentos> getListaDocumentos(int tipoLista) {
            String query;
            switch (tipoLista) {
                case (EN_LISTA_DOCUMENTOS_PENDENTES):
                    query = "spListarDocumentosPendentes";
                    break;
                case EN_LISTA_DOCUMENTOS_CONTACORRENTE:
                    query = "spListarDocumentosContaCorrente";
                    break;
                case  EN_LISTA_ENCOMENDAS_CONTACORRENTE:
                    query = "spListarEncomendasContaCorrente";
                    break;
                default:
                    query = "spListarDocumentos";
                    break;
            }
            
            ArrayList<ListaDocumentos> lst = new ArrayList();
            List newlst = Helper.con.executeQuery(query);
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    ListaDocumentos item = new ListaDocumentos();
                    item.Fornecedor = ((String) ((Map<String, Object>) newlst.get(i)).get("Fornecedor"));
                    item.Data = ((Date) ((Map<String, Object>) newlst.get(i)).get("Data"));
                    item.selected = false;

                    item.Documento = new Documentos();
                    item.Documento.setIdDocumento((Integer) ((Map<String, Object>) newlst.get(i)).get("idDocumento"));
                    item.Documento.setNrDoc((String) ((Map<String, Object>) newlst.get(i)).get("nrDoc"));
                    item.Documento.setIdFornecedor((Integer) ((Map<String, Object>) newlst.get(i)).get("idFornecedor"));

                    item.documentosLinhas = DocumentosLinhas.ListaDocumentosLinhas.getListaDocumentosLinhas(item.Documento.getIdDocumento(), tipoLista);
                    lst.add(item);
                }
            }
            return lst;

        }
    }
}
