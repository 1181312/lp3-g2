package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Gramagem {

    private int idGramagem;
    private String descricao;
    private boolean valid = false;
    public static final String SQL_SELECT = "SELECT idGramagem,descricao"
            + " FROM Gramagens ";

    public int getIdGramagem() {
        return this.idGramagem;
    }

    /**
     *
     * @param idGramagem
     */
    public void setIdGramagem(int idGramagem) {
        this.idGramagem = idGramagem;
    }

    public String getDescricao() {
        return this.descricao;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Gramagem() {
    }

    public Gramagem(int idGramagem) {
        this.idGramagem = idGramagem;
        getGramagem();
    }

    private void getGramagem() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idGramagem = " + this.idGramagem);
        if (lst != null && lst.size() > 0) {
            this.descricao = (String) ((Map<String, Object>) lst.get(0)).get("descricao");
            this.valid = true;
        }
    }
    
    public boolean isValid() {
        return this.valid;
    }

    // -------------------------
    // -- Métodos partilhados --
    // -------------------------
    public static ArrayList<Gramagem> getLista() {
        return getLst("");
    }

    public static ArrayList<Gramagem> getLista(String filter) {
        return getLst(filter);
    }

    /**
     * Retorna uma lista do tipo Gramagem com base na informação que é retornada
     * da base de dados
     *
     * @param filter
     * @return
     */
    private static ArrayList<Gramagem> getLst(String filter) {
        ArrayList<Gramagem> lst = new ArrayList();
        List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                Gramagem newItem = new Gramagem();
                newItem.setIdGramagem((int) ((Map<String, Object>) newlst.get(i)).get("idGramagem"));
                newItem.setDescricao((String) ((Map<String, Object>) newlst.get(i)).get("descricao"));
                lst.add(newItem);
            }
        }
        return lst;
    }

}
