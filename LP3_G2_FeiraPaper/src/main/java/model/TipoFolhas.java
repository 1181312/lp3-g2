package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TipoFolhas {

    private int idTipoFolha;
    private String descricao;
    private float largura;
    private float altura;
    private boolean valid = false;
    public static final String SQL_SELECT = "SELECT idTipoFolha,descricao,largura,altura"
            + " FROM TipoFolhas ";

    public int getIdTipoFolha() {
        return this.idTipoFolha;
    }

    /**
     *
     * @param idTipoFolha
     */
    public void setIdTipoFolha(int idTipoFolha) {
        this.idTipoFolha = idTipoFolha;
    }

    public String getDescricao() {
        return this.descricao;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getLargura() {
        return this.largura;
    }

    /**
     *
     * @param largura
     */
    public void setLargura(float largura) {
        this.largura = largura;
    }

    public float getAltura() {
        return this.altura;
    }

    /**
     *
     * @param altura
     */
    public void setAltura(float altura) {
        this.altura = altura;
    }

    public TipoFolhas() {
    }

    public TipoFolhas(int idTipoFolha) {
        this.idTipoFolha = idTipoFolha;
        getTipoFolhas();
    }
    
    public TipoFolhas(float largura, float altura) {
        this.largura = largura;
        this.altura = altura;
        getTipoFolhasBySize();
    }

    private void getTipoFolhas() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idTipoFolha = " + this.idTipoFolha);
        if (lst != null && lst.size() > 0) {
            this.descricao = (String) ((Map<String, Object>) lst.get(0)).get("descricao");
            this.largura = (float) ((Map<String, Object>) lst.get(0)).get("largura");
            this.altura = (float) ((Map<String, Object>) lst.get(0)).get("altura");
            this.valid = true;
        }
    }
    private void getTipoFolhasBySize() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE largura = " + this.largura + " AND altura = " + this.altura);
        if (lst != null && lst.size() > 0) {
            this.idTipoFolha = (int) ((Map<String, Object>) lst.get(0)).get("idTipoFolha");
            this.descricao = (String) ((Map<String, Object>) lst.get(0)).get("descricao");
            this.largura = (float) ((Map<String, Object>) lst.get(0)).get("largura");
            this.altura = (float) ((Map<String, Object>) lst.get(0)).get("altura");
            this.valid = true;
        }
    }
    
    
    public boolean isValid() {
        return this.valid;
    }

    // -------------------------
    // -- Métodos partilhados --
    // -------------------------
    public static ArrayList<TipoFolhas> getLista() {
        return getLst("");
    }

    public static ArrayList<TipoFolhas> getLista(String filter) {
        return getLst(filter);
    }

    /**
     * Retorna uma lista do tipo Tipo Folhas com base na informação que é
     * retornada da base de dados
     *
     * @param filter
     * @return
     */
    private static ArrayList<TipoFolhas> getLst(String filter) {
        ArrayList<TipoFolhas> lst = new ArrayList();
        List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                TipoFolhas newItem = new TipoFolhas();
                newItem.setIdTipoFolha((int) ((Map<String, Object>) newlst.get(i)).get("idTipoFolha"));
                newItem.setDescricao((String) ((Map<String, Object>) newlst.get(i)).get("descricao"));
                newItem.setLargura((Float) ((Map<String, Object>) newlst.get(i)).get("largura"));
                newItem.setAltura((Float) ((Map<String, Object>) newlst.get(i)).get("altura"));
                lst.add(newItem);
            }
        }
        return lst;
    }

}
