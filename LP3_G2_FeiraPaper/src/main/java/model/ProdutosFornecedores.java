package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;

public class ProdutosFornecedores {

    private int idProduto;
    private int idFornecedor;
    private int idProdutoFornecedor;
    public static final String SQL_SELECT = "SELECT idProduto, idFornecedor, idProdutoFornecedor "
            + "FROM ProdutosFornecedores ";

    public int getIdProduto() {
        return this.idProduto;
    }

    /**
     *
     * @param idProduto
     */
    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public int getIdFornecedor() {
        return this.idFornecedor;
    }

    /**
     *
     * @param idFornecedor
     */
    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public int getIdProdutoFornecedor() {
        return this.idProdutoFornecedor;
    }

    /**
     *
     * @param idProdutoFornecedor
     */
    public void setIdProdutoFornecedor(int idProdutoFornecedor) {
        this.idProdutoFornecedor = idProdutoFornecedor;
    }

    public ProdutosFornecedores() {
    }

    public ProdutosFornecedores(int idProduto) {
        this.idProduto = idProduto;
        getProdutosFornecedoresByProduto();
    }

    public ProdutosFornecedores(int idProduto, int idFornecedor) {
        this.idProduto = idProduto;
        this.idFornecedor = idFornecedor;
        getProdutosFornecedoresByProdutoAndFornecedor();
    }

    private void getProdutosFornecedoresByProduto() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idProduto = " + this.idProduto);
        if (lst != null && lst.size() > 0) {
            this.idProduto = (int) ((Map<String, Object>) lst.get(0)).get("idProduto");
            this.idFornecedor = (int) ((Map<String, Object>) lst.get(0)).get("idFornecedor");
            this.idProdutoFornecedor = (int) ((Map<String, Object>) lst.get(0)).get("idProdutoFornecedor");
        }
    }

    private void getProdutosFornecedoresByProdutoAndFornecedor() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idProduto = " + this.idProduto + " AND idFornecedor = " + this.idFornecedor);
        if (lst != null && lst.size() > 0) {
            this.idProduto = (int) ((Map<String, Object>) lst.get(0)).get("idProduto");
            this.idFornecedor = (int) ((Map<String, Object>) lst.get(0)).get("idFornecedor");
            this.idProdutoFornecedor = (int) ((Map<String, Object>) lst.get(0)).get("idProdutoFornecedor");
        }
    }
    
    /**
     * Verfica se o código já existe noutro produto do mesmo fornecedor
     * @return 
     */
    public boolean checkIfExists(){
        boolean retValue = true;
        
        try {
            String query = String.format("SELECT dbo.fCheckProdutoFornecedorExist(%d, %d, %d)", this.getIdProduto(), this.getIdFornecedor(), this.getIdProdutoFornecedor());
            List lst =  Helper.con.executeQuery(query);
            if (lst != null && lst.size() > 0) {
                retValue = (boolean) ((Map<String, Object>) lst.get(0)).get("");
            }
        } catch (Exception e) {
        }
        
        
        return retValue;
    }
    
    public boolean save() {
        boolean retValue = true;
        
        try {
            String query = String.format("spProdutoFornecedorGuardar @idProduto=%d, @idFornecedor=%d, @idProdutoFornecedor=%d", this.getIdProduto(), this.getIdFornecedor(), this.getIdProdutoFornecedor());
            Helper.con.executeQuery(query);
        } catch (Exception e) {
            Helper.showErrorAlert("Erro ao gravar código do produto de fornecedor.\nErro: " + e.getMessage());
            retValue = false;
        }
        
        return retValue;
    }

    // -------------------------
    // -- Métodos partilhados --
    // -------------------------
    public static ArrayList<ProdutosFornecedores> getLista() {
        return getLst("");
    }

    public static ArrayList<ProdutosFornecedores> getLista(String filter) {
        return getLst(filter);
    }

    /**
     * Retorna uma lista do tipo ProdutosFornecedores com base na informação que
     * é retornada da base de dados
     *
     * @param filter
     * @return
     */
    private static ArrayList<ProdutosFornecedores> getLst(String filter) {
        ArrayList<ProdutosFornecedores> lst = new ArrayList();
        List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                ProdutosFornecedores newItem = new ProdutosFornecedores();
                newItem.setIdProduto((int) ((Map<String, Object>) newlst.get(i)).get("idProduto"));
                newItem.setIdFornecedor((int) ((Map<String, Object>) newlst.get(i)).get("idFornecedor"));
                newItem.setIdProdutoFornecedor((int) ((Map<String, Object>) newlst.get(i)).get("idProdutoFornecedor"));
                lst.add(newItem);
            }
        }
        return lst;
    }

    public static class ListToProduct {

        private int idProduto;
        private int idUtilizador;
        private int idFornecedor;
        private String descFornecedor;
        private int idProdForn;
        public static final String SQL_SELECT = "";

        public int getIdProduto() {
            return idProduto;
        }

        public void setIdProduto(int idProduto) {
            this.idProduto = idProduto;
        }

        public int getIdUtilizador() {
            return idUtilizador;
        }

        public void setIdUtilizador(int idUtilizador) {
            this.idUtilizador = idUtilizador;
        }

        public int getIdFornecedor() {
            return idFornecedor;
        }

        public void setIdFornecedor(int idFornecedor) {
            this.idFornecedor = idFornecedor;
        }

        public String getDescFornecedor() {
            return descFornecedor;
        }

        public void setDescFornecedor(String descFornecedor) {
            this.descFornecedor = descFornecedor;
        }

        public int getIdProdForn() {
            return idProdForn;
        }

        public void setIdProdForn(int idProdForn) {
            this.idProdForn = idProdForn;
        }

        public static ArrayList<ListToProduct> getLista(int idProduto) {
            return getLst(idProduto);
        }

        private static ArrayList<ListToProduct> getLst(int idProduto) {
            ArrayList<ListToProduct> lst = new ArrayList();

            String query = "spProdutosFornecedoresByProduto @idProduto={0}";
            query = query.replace("{0}", String.valueOf(idProduto));

            List newlst = Helper.con.executeQuery(query);
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    ListToProduct item = new ListToProduct();

                    item.setIdProduto((int) ((Map<String, Object>) newlst.get(i)).get("idProduto"));
                    item.setIdUtilizador((int) ((Map<String, Object>) newlst.get(i)).get("idUtilizador"));
                    item.setIdFornecedor((int) ((Map<String, Object>) newlst.get(i)).get("idFornecedor"));
                    item.setDescFornecedor((String) ((Map<String, Object>) newlst.get(i)).get("descFornecedor"));
                    item.setIdProdForn((int) ((Map<String, Object>) newlst.get(i)).get("idProdutoFornecedor"));

                    lst.add(item);
                }
            }
            return lst;
        }

        public static void constructTblLinhas(TableView tblFornecedores, Button btn) {
            tblFornecedores.getColumns().clear();
            tblFornecedores.getItems().clear();
            tblFornecedores.setEditable(true);
            tblFornecedores.getSelectionModel().cellSelectionEnabledProperty().set(true);

            // idProduto --> HIDDEN
            TableColumn<ListToProduct, Integer> colIdProduto = new TableColumn<>("idProduto");
            colIdProduto.setMinWidth(150);
            colIdProduto.setCellValueFactory(new PropertyValueFactory<>("idProduto"));
            colIdProduto.setVisible(false);
            colIdProduto.setEditable(false);

            // idUtilizador --> HIDDEN
            TableColumn<ListToProduct, Integer> colIdUtilizador = new TableColumn<>("idUtilizador");
            colIdUtilizador.setMinWidth(150);
            colIdUtilizador.setCellValueFactory(new PropertyValueFactory<>("idUtilizador"));
            colIdUtilizador.setVisible(false);
            colIdUtilizador.setEditable(false);

            // idFornecedor 
            TableColumn<ListToProduct, Integer> colIdFornecedor = new TableColumn<>("Cód. Fornecedor");
            colIdFornecedor.setMinWidth(150);
            colIdFornecedor.setCellValueFactory(new PropertyValueFactory<>("idFornecedor"));
            colIdFornecedor.setEditable(false);

            // descFornecedor
            TableColumn<ListToProduct, String> colDescFornecedor = new TableColumn<>("Fornecedor");
            colDescFornecedor.setMinWidth(200);
            colDescFornecedor.setCellValueFactory(new PropertyValueFactory<>("descFornecedor"));
            colDescFornecedor.setEditable(false);

            // idProdutoFornecedor
            TableColumn<ListToProduct, Integer> colIdProdutoFornecedor = new TableColumn<>("Cód. Prod. Fornecedor");
            colIdProdutoFornecedor.setMinWidth(150);
            colIdProdutoFornecedor.setCellValueFactory(new PropertyValueFactory<>("idProdForn"));
            colIdProdutoFornecedor.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter(){                
                @Override
                public Integer fromString(String string) {
                    Integer retValue = null;
                    try {
                        retValue = Integer.parseInt(string);
                    } catch (Exception e) {
                        Helper.showErrorAlert("Apenas pode inserir números.");
                    }                       
                    return retValue;
                }
            }));

            colIdProdutoFornecedor.setOnEditStart((CellEditEvent<ListToProduct, Integer> event) -> {
                if (btn != null) {
                    btn.setDisable(true);
                }                
            });
            colIdProdutoFornecedor.setOnEditCancel((CellEditEvent<ListToProduct, Integer> event) -> {
                if (btn != null) {
                    btn.setDisable(true);
                }
            });
            colIdProdutoFornecedor.setOnEditCommit((CellEditEvent<ListToProduct, Integer> t) -> {
                ((ListToProduct) t.getTableView().getItems().get(t.getTablePosition().getRow())).setIdProdForn(t.getNewValue());
                if (btn != null) {
                    btn.setDisable(false);
                }
            });
            
            colIdProdutoFornecedor.setEditable(true);

            tblFornecedores.getColumns().addAll(colIdProduto, colIdUtilizador, colIdFornecedor, colDescFornecedor, colIdProdutoFornecedor);
        }

    }
}
