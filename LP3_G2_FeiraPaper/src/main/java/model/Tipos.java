package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Tipos {

        public static final int Admin = 1;
        public static final int Oper = 2;
        public static final int Forn = 3;
        public static final int Client = 4;
                
	private int idTipo;
	private String descricao;
        public static final String SQL_SELECT = "SELECT idTipoUtilizador, descricao"
            + " FROM TiposUtilizadores ";

	public int getIdTipo() {
		return this.idTipo;
	}

	/**
	 * 
	 * @param idTipo
	 */
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * 
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

        // -------------------------
        // -- Métodos partilhados --
        // -------------------------
        
        public static ArrayList<Tipos> getLista() {
            return getLst("");
        }
        public static ArrayList<Tipos> getLista(String filter) {
            return getLst(filter);
        }
        /**
         * Retorna uma lista do tipo Tipos com base na informação que é retornada da base de dados
         * @param filter
         * @return 
         */
        private static ArrayList<Tipos> getLst(String filter) {
            ArrayList<Tipos> lst = new ArrayList();
            List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    Tipos newItem = new Tipos();
                    newItem.setIdTipo((int) ((Map<String, Object>) newlst.get(i)).get("idTipoUtilizador"));
                    newItem.setDescricao((String) ((Map<String, Object>) newlst.get(i)).get("descricao"));
                    lst.add(newItem);
                }
            }
            return lst;
        }
        
    @Override
    public String toString() {
        return this.descricao;
    }
}