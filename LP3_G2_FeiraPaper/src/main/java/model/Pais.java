package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Pais {

	private int idPais;
	private String ISO;
	private String descricao;
        private String codPostalRegex;
        public static final String SQL_SELECT = "SELECT idPais, ISO, descricao, codPostalRegex "
            + "FROM Paises ";
        

	public int getIdPais() {
		return this.idPais;
	}

	/**
	 * 
	 * @param idPais
	 */
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}

	public String getISO() {
            return this.ISO;
	}

	/**
	 * 
	 * @param ISO
	 */
	public void setISO(String ISO) {
		
            this.ISO = ISO;
	}

	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * 
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
        
        public String getCodPostalRegex() {
		return this.codPostalRegex;
	}

	/**
	 * 
	 * @param codPostalRegex
	 */
	public void setCodPostalRegex(String codPostalRegex) {
		this.codPostalRegex = codPostalRegex;
	}

        
        // -------------------------
        // -- Métodos partilhados --
        // -------------------------
        public static ArrayList<Pais> getLista() {
            return getLst("");
        }
        public static ArrayList<Pais> getLista(String filter) {
            return getLst(filter);
        }
        /**
         * Retorna uma lista do tipo Pais com base na informação que é retornada da base de dados
         * @param filter
         * @return 
         */
        private static ArrayList<Pais> getLst(String filter) {
            ArrayList<Pais> lst = new ArrayList();
            List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    Pais newItem = new Pais();
                    newItem.setIdPais((int) ((Map<String, Object>) newlst.get(i)).get("idPais"));
                    newItem.setISO((String) ((Map<String, Object>) newlst.get(i)).get("ISO"));
                    newItem.setDescricao((String) ((Map<String, Object>) newlst.get(i)).get("descricao"));
                    newItem.setCodPostalRegex((String) ((Map<String, Object>) newlst.get(i)).get("codPostalRegex"));
                    lst.add(newItem);
                }
            }
            return lst;
        }
        
    @Override
    public String toString() {
        return this.descricao;
    }
}