/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author tiagosantos
 */
public class IVA {

    private int idIva;
    private double taxa;
    private int idPais;
    private boolean valid = false;
    public static final String SQL_SELECT = "SELECT idIva, taxa, idPais "
            + "FROM IVA ";

    public int getIdIva() {
        return this.idIva;
    }

    public void setIdIva(int idIva) {
        this.idIva = idIva;
    }

    public double getTaxa() {
        return this.taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    public int getIdPais() {
        return this.idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public IVA() {
    }

    public IVA(int idIva) {
        this.idIva = idIva;
        getIVA();
    }

    public IVA(double taxa, String ISO) {
        this.taxa = taxa;
        getIVAbyTaxa(ISO);
    }

    private void getIVA() {
        List lst = Helper.con.executeQuery(SQL_SELECT + " WHERE idIVA = " + this.idIva);
        if (lst != null && lst.size() > 0) {
            this.idIva = (int) ((Map<String, Object>) lst.get(0)).get("idIva");
            this.taxa =  Double.parseDouble(((Map<String, Object>) lst.get(0)).get("taxa").toString());
            this.idPais = (int) ((Map<String, Object>) lst.get(0)).get("idPais");
            this.valid = true;
        }
    }

    private void getIVAbyTaxa(String ISO) {
        List lst = Helper.con.executeQuery("SELECT Iva.idIva, Iva.taxa, Iva.idPais FROM Iva INNER JOIN Paises ON Iva.idpais = paises.idpais WHERE iva.taxa = " + this.taxa + " and paises.ISO='" + ISO + "'");
        if (lst != null && lst.size() > 0) {
            this.idIva = (int) ((Map<String, Object>) lst.get(0)).get("idIva");
            this.taxa = Double.parseDouble(((Map<String, Object>) lst.get(0)).get("taxa").toString());
            this.idPais = (int) ((Map<String, Object>) lst.get(0)).get("idPais");
            this.valid = true;
        }
    }

    public boolean isValid() {
        return this.valid;
    }

    // -------------------------
    // -- Métodos partilhados --
    // -------------------------
    public static ArrayList<IVA> getLista() {
        return getLst("");
    }

    public static ArrayList<IVA> getLista(String filter) {
        return getLst(filter);
    }

    /**
     * Retorna uma lista do tipo Iva com base na informação que é retornada da
     * base de dados
     *
     * @param filter
     * @return
     */
    private static ArrayList<IVA> getLst(String filter) {
        ArrayList<IVA> lst = new ArrayList();
        List newlst = Helper.con.executeQuery(SQL_SELECT + (!filter.isEmpty() ? " WHERE " + filter : ""));
        if (newlst != null) {
            for (int i = 0; i < newlst.size(); i++) {
                IVA newItem = new IVA();
                newItem.setIdIva((int) ((Map<String, Object>) newlst.get(i)).get("idIva"));
                newItem.setTaxa(((BigDecimal) ((Map<String, Object>) newlst.get(i)).get("taxa")).floatValue());
                newItem.setIdPais((int) ((Map<String, Object>) newlst.get(i)).get("idPais"));

                lst.add(newItem);
            }
        }
        return lst;
    }
}
