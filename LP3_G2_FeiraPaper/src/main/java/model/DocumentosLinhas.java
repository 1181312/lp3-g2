package model;

import com.mycompany.lp3_g2_feirapaper.ConnDB;
import com.mycompany.lp3_g2_feirapaper.Helper;
import com.mycompany.lp3_g2_feirapaper.UnitConvert;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DocumentosLinhas {

    private int idDocumentoLinha;
    private int idDocumento;
    private int idProduto;
    private Date data;
    private double pCusto;
    private int qt;
    private Boolean valido;
    private int idIva;

    public static final String SQL_SELECT = "SELECT idDocumentoLinha, idDocumento, idProduto, data, pCusto, qt, valido, idIva "
            + "FROM DocumentosLinhas ";

    public int getIdDocumentoLinha() {
        return idDocumentoLinha;
    }

    public void setIdDocumentoLinha(int idDocumentoLinha) {
        this.idDocumentoLinha = idDocumentoLinha;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getpCusto() {
        return pCusto;
    }

    public void setpCusto(double pCusto) {
        this.pCusto = pCusto;
    }

    public int getQt() {
        return qt;
    }

    public void setQt(int qt) {
        this.qt = qt;
    }

    public Boolean isValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

    public int getIdIVA() {
        return idIva;
    }

    public void setIdIVA(int idIva) {
        this.idIva = idIva;
    }

    public static void DocumentoLinhaInsert(int idDocumento, int idProduto, int qt, double pCusto, double taxa, String iso) {

        ConnDB db = new ConnDB();
        Connection con = db.getConnection();

        String query = "EXEC spDocumentoLinhaInsert @idDocumento=?, @idProduto=?, @qt=?, @pCusto=?, @taxa=?, @ISO=?";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, idDocumento);
            preparedStatement.setInt(2, idProduto);
            preparedStatement.setInt(3, qt);
            preparedStatement.setDouble(4, pCusto);
            preparedStatement.setDouble(5, taxa);
            preparedStatement.setString(6, iso);

            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (SQLException e) {
            Logger.getLogger(Produtos.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }
    }

    public static class DocumentosLinhasXML {

        public Integer OrderConfirmationLineItemNumber;
        public String ProductIdentifier_Buyer;
        public String ProductIdentifier_Supplier;
        public Integer PaperCharacteristics_BasisWeight;
        public Integer SheetSize_Length;
        public Integer SheetSize_Width;
        public Double PricePerUnit_CurrencyValue;
        public Double PricePerUnit_Value;
        public String PricePerUnit_Value_UOM;
        public Double MonetaryAdjustment_CurrencyValue;
        public Double TaxAdjustment_TaxPercent;
        public Double TaxAdjustment_TaxAmount;
        public String TaxAdjustment_TaxLocation;
        public Integer Quantity_Ream;
        public Integer Quantity_Sheet;
        public Double Quantity_Kilogram;
    }

    public static class ListaDocumentosLinhas {

        private String NumDoc;
        private DocumentosLinhas DocumentoLinha;
        private String CodProduto;
        private String CodProdutoFornecedor;
        private String Fornecedor;
        private Date Data;
        private Integer QuantidadeFolhas;
        private Double PesoProduto;
        private Double Preco;
        public static final int EN_LISTA_DOCUMENTOSLINHAS = 1;
        public static final int EN_LISTA_DOCUMENTOSLINHAS_PENDENTES = 2;
        public static final int EN_LISTA_DOCUMENTOSLINHAS_CONTACORRENTE = 3;
        public static final int EN_LISTA_ENCOMENDASLINHAS_CONTACORRENTE = 4;

        public String getNumDoc() {
            return NumDoc;
        }

        public String getFornecedor() {
            return Fornecedor;
        }

        public String getData() {
            return String.valueOf(Data);
        }

        public DocumentosLinhas getDocumentoLinha() {
            return DocumentoLinha;
        }

        public String getCodProduto() {
            return CodProduto;
        }

        public String getCodProdutoFornecedor() {
            return CodProdutoFornecedor;
        }

        public String getQuantidadeFolhas() {
            return Helper.formatSheetsToString(UnitConvert.fromSheetsToAny(QuantidadeFolhas, PesoProduto));
        }

        public String getQuantidadeResmas() {
            return Helper.formatReamsToString(UnitConvert.fromSheetsToAny(QuantidadeFolhas, PesoProduto));
        }

        public String getQuantidadeKg() {
            return Helper.formatGramsToString(UnitConvert.fromSheetsToAny(QuantidadeFolhas, PesoProduto));
        }

        public String getPreco() {
            return Helper.formatPriceToString(Preco);
        }

        public void validar(boolean validar) {

            ConnDB db = new ConnDB();
            Connection con = db.getConnection();
            String query = "spValidarDocumentosLinhasPendentes @idDocumentoLinha=?, @valido=?";

            try {
                PreparedStatement preparedStatement = con.prepareStatement(query);
                preparedStatement.setInt(1, this.getDocumentoLinha().getIdDocumentoLinha());
                preparedStatement.setBoolean(2, validar);

                preparedStatement.executeUpdate();

                preparedStatement.close();
            } catch (SQLException e) {

            } finally {
                try {
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                } catch (SQLException ex) {
                }
            }
        }

        public static ArrayList<ListaDocumentosLinhas> getListaDocumentosLinhas(Integer idDocumento, int tipoLista) {

            String query;
            switch (tipoLista) {
                case (EN_LISTA_DOCUMENTOSLINHAS_PENDENTES):
                    query = "spListarDocumentosLinhasPendentesByDocumento";
                    break;
                case EN_LISTA_DOCUMENTOSLINHAS_CONTACORRENTE:
                    query = "spListarDocumentosLinhasContaCorrenteByDocumento";
                    break;
                case EN_LISTA_ENCOMENDASLINHAS_CONTACORRENTE:
                    query = "spListarEncomendasLinhasContaCorrenteByEncomenda";
                    break;
                default:
                    query = "spListarDocumentosLinhasByDocumento";
                    break;
            }
            query += " @idDocumento="+idDocumento;

            ArrayList<ListaDocumentosLinhas> lst = new ArrayList();
            List newlst = Helper.con.executeQuery(query);
            if (newlst != null) {
                for (int i = 0; i < newlst.size(); i++) {
                    ListaDocumentosLinhas item = new ListaDocumentosLinhas();
                    item.CodProduto = ((String) ((Map<String, Object>) newlst.get(i)).get("CodProduto"));
                    item.CodProdutoFornecedor = String.valueOf((Integer) ((Map<String, Object>) newlst.get(i)).get("CodProdutoFornecedor"));
                    item.QuantidadeFolhas = ((Integer) ((Map<String, Object>) newlst.get(i)).get("QuantidadeFolhas"));
                    item.PesoProduto = ((Float) ((Map<String, Object>) newlst.get(i)).get("PesoProduto")).doubleValue();
                    item.Preco = ((BigDecimal) ((Map<String, Object>) newlst.get(i)).get("Preco")).doubleValue();

                    item.DocumentoLinha = new DocumentosLinhas();
                    item.DocumentoLinha.setIdDocumento(idDocumento);
                    item.DocumentoLinha.setIdDocumentoLinha((Integer) ((Map<String, Object>) newlst.get(i)).get("idDocumentoLinha"));
                    item.DocumentoLinha.setValido((Boolean) ((Map<String, Object>) newlst.get(i)).get("Valido"));
                    lst.add(item);
                }
            }
            return lst;

        }
    }
}
