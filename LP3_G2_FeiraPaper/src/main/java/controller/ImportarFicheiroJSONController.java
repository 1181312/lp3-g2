/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import com.mycompany.lp3_g2_feirapaper.JSONValidator;
import com.mycompany.lp3_g2_feirapaper.UnitConvert;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.AnchorPane;
import model.Documentos;
import model.DocumentosLinhas;
import model.Produtos;
import model.Utilizadores;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


        
/**
 * FXML Controller class
 *
 * @author helder.oliveira
 */
public class ImportarFicheiroJSONController implements Initializable {

    File jsonFile;
    Documentos.DocumentoXML doc;
    
    @FXML
    private TextField txtImportarFicheiro;
    @FXML
    private Button btnProcurar;
    @FXML
    private Button btnImportar;
    @FXML
    private AnchorPane pnlTable;
    @FXML
    private TableView tblLinhas;
    @FXML
    private TextField txtDocReferencia;
    @FXML
    private TextField txtDocDia;
    @FXML
    private TextField txtFornNome;
    @FXML
    private TextField txtFornMorada1;
    @FXML
    private TextField txtFornMorada2;
    @FXML
    private TextField txtFornCidade;
    @FXML
    private TextField txtFornCodPostal;
    @FXML
    private TextField txtFornPais;
    @FXML
    private Accordion accord;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pnlTable.setVisible(false);
        
        accord.expandedPaneProperty().addListener((ObservableValue<? extends TitledPane> ov, TitledPane old_val, TitledPane new_val) -> {
            if (new_val != null){
                tblLinhas.getSelectionModel().clearSelection();
                tblLinhas.refresh();
            }
        });
        
    }    

    @FXML
    private void Procurar(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON Files", "*.json"));

        jsonFile = fileChooser.showOpenDialog(null);

        if (jsonFile == null) {
            Helper.showErrorAlert("O ficheiro não é valido!");
            return;
        } else {
            txtImportarFicheiro.setText(jsonFile.getName());
        }
        
        JSONValidator validator = new JSONValidator();

        try {

        if (validator.JSONFileIsValid(jsonFile) == null) {
            Helper.showErrorAlert("O ficheiro JSON não está bem formatado.");
            cleanValues();
            return;
        } else {
            if (validator.validateStructeFromJSONFile(jsonFile)) {
                if (!validator.validateHeaderFromJSONFile(jsonFile, Helper.LoginUser.getIdentificador().toString())) {
                    Helper.showErrorAlert(validator.showWarningsMessage());
                    cleanValues();
                    return;
                }

                if (!validator.validateLinesFromJSONFile(jsonFile, Helper.LoginUser.getIdentificador().toString())) {
                    Helper.showErrorAlert(validator.showWarningsMessage());
                    cleanValues();
                    return;
                }
                
                SaveInMemory();
                previewData();
            }
        }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
    }

    @FXML
    private void Importar(ActionEvent event) {
        if(txtImportarFicheiro.getText().isEmpty()){
            Helper.showInfoAlert("Selecione um ficheiro JSON.");
            return;
        }
        
        if(Save()){
            Helper.showInfoAlert("Ficheiro JSON importado com sucesso.");
        }else{
            Helper.showInfoAlert("Ficheiro JSON não foi importado");
        }
        
        cleanValues();
        
    }
    
    
    public void SaveInMemory() {
        JSONValidator validator = new JSONValidator();
        JSONObject jsonDoc = validator.JSONFileIsValid(jsonFile);
        
        doc = new Documentos.DocumentoXML();
        doc.OrderConfirmationLineItem = new ArrayList<DocumentosLinhas.DocumentosLinhasXML>();
        
        //OrderConfirmation
        if(jsonDoc.get("OrderConfirmation") != null){
            JSONObject OrderConfirmation = (JSONObject)jsonDoc.get("OrderConfirmation");
            //OrderConfirmationHeader
            if(OrderConfirmation.get("OrderConfirmationHeader") != null){
                JSONObject OrderConfirmationHeader = (JSONObject)OrderConfirmation.get("OrderConfirmationHeader");
                //OrderConfirmationReference
                if (OrderConfirmationHeader.get("OrderConfirmationReference") != null){
                    doc.OrderConfirmationReference = OrderConfirmationHeader.get("OrderConfirmationReference").toString();
                }
                //OrderConfirmationIssuedDate
                if (OrderConfirmationHeader.get("OrderConfirmationIssuedDate") != null){
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    try{
                        Date date = formatter.parse(OrderConfirmationHeader.get("OrderConfirmationIssuedDate") .toString());
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        doc.Date_Day = calendar.get(Calendar.DAY_OF_MONTH);
                        doc.Date_Month = calendar.get(Calendar.MONTH);
                        doc.Date_Year = calendar.get(Calendar.YEAR);
                        
                    }catch (Exception ex){
                    
                    }
                }
                //SupplierParty
                if (OrderConfirmationHeader.get("SupplierParty") != null){
                    JSONObject SupplierParty = (JSONObject)OrderConfirmationHeader.get("SupplierParty");
                    //PartyIdentifier
                    if (SupplierParty.get("PartyIdentifier") != null){
                        JSONObject PartyIdentifier = (JSONObject)SupplierParty.get("PartyIdentifier");    
                        //Code
                        if (PartyIdentifier.get("Code") != null){
                            doc.SupplierParty_PartyIdentifier = Integer.parseInt(PartyIdentifier.get("Code").toString());
                        }
                    }
                    //NameAddress
                    if (SupplierParty.get("NameAddress") != null){
                        JSONObject NameAddress = (JSONObject)SupplierParty.get("NameAddress");
                        //Name
                        if (NameAddress.get("Name") != null){
                            doc.SupplierParty_Name = NameAddress.get("Name").toString();
                        }
                        //Address1
                        if (NameAddress.get("Address1") != null){
                            doc.SupplierParty_Address1 = NameAddress.get("Address1").toString();
                        }
                        //Address2
                        if (NameAddress.get("Address2") != null){
                            doc.SupplierParty_Address2 = NameAddress.get("Address2").toString();
                        }
                        //City
                        if (NameAddress.get("City") != null){
                            doc.SupplierParty_City = NameAddress.get("City").toString();
                        }
                        //PostalCode
                        if (NameAddress.get("PostalCode") != null){
                            doc.SupplierParty_PostalCode = NameAddress.get("PostalCode").toString();
                        }
                        //Country
                        if (NameAddress.get("Country") != null){
                            JSONObject Country = (JSONObject)NameAddress.get("Country");
                            //ISOCountryCode
                            if (Country.get("ISOCountryCode") != null){
                                doc.SupplierParty_Country_ISO = Country.get("ISOCountryCode").toString();
                            }
                            //Value
                            if (Country.get("Value") != null){
                                doc.SupplierParty_Country = Country.get("Value").toString();
                            }
                        }
                    }
                }
                
            }
            
            //OrderConfirmationLineItem
            if(OrderConfirmation.get("OrderConfirmationLineItem") != null){
                JSONArray OrderConfirmationLineItems = (JSONArray) OrderConfirmation.get("OrderConfirmationLineItem");
                for (int index = 0; index < OrderConfirmationLineItems.size(); index++) {
                
                    JSONObject OrderConfirmationLineItem = (JSONObject) OrderConfirmationLineItems.get(index);
                    DocumentosLinhas.DocumentosLinhasXML mov = new DocumentosLinhas.DocumentosLinhasXML();
                    
                    //OrderConfirmationLineItemNumber
                    if (OrderConfirmationLineItem.get("OrderConfirmationLineItemNumber") != null){
                        mov.OrderConfirmationLineItemNumber =  Integer.parseInt(OrderConfirmationLineItem.get("OrderConfirmationLineItemNumber").toString());
                    }
                    
                    //Product
                    if (OrderConfirmationLineItem.get("Product") != null){
                        JSONObject Product = (JSONObject) OrderConfirmationLineItem.get("Product");
                        //ProductIdentifier
                        if (Product.get("ProductIdentifier") != null){
                            JSONArray ProductIdentifiers = (JSONArray) Product.get("ProductIdentifier");
                            for (int indexProductIdentifier = 0; indexProductIdentifier < ProductIdentifiers.size(); indexProductIdentifier++) {
                                JSONObject ProductIdentifier = (JSONObject) ProductIdentifiers.get(indexProductIdentifier);
                                switch (ProductIdentifier.get("Agency").toString()){
                                    case "Buyer":
                                        if (ProductIdentifier.get("Code") != null){
                                            mov.ProductIdentifier_Buyer = ProductIdentifier.get("Code").toString();
                                        }
                                        break;
                                    case "Supplier":
                                        if (ProductIdentifier.get("Code") != null){
                                            mov.ProductIdentifier_Supplier = ProductIdentifier.get("Code").toString();
                                        }
                                        break;
                                }
                            }
                        
                        }
                        //Paper
                        if (Product.get("Paper") != null){
                            JSONObject Paper = (JSONObject) Product.get("Paper");
                            //PaperCharacteristics
                            if (Paper.get("PaperCharacteristics") != null){
                                JSONObject PaperCharacteristics = (JSONObject) Paper.get("PaperCharacteristics");
                                //BasisWeight
                                if (PaperCharacteristics.get("BasisWeight") != null){
                                    JSONObject BasisWeight = (JSONObject) PaperCharacteristics.get("BasisWeight");
                                    //DetailValue
                                    if (BasisWeight.get("DetailValue") != null){
                                        JSONObject DetailValue = (JSONObject) BasisWeight.get("DetailValue");
                                        //DetailValue
                                        if (DetailValue.get("Value") != null){
                                            mov.PaperCharacteristics_BasisWeight = Integer.parseInt(DetailValue.get("Value").toString());
                                        }
                                    }
                                }
                            }
                        
                            //Sheet
                            if (Paper.get("Sheet") != null){
                                JSONObject Sheet = (JSONObject) Paper.get("Sheet");
                                //SheetConversionCharacteristics
                                if(Sheet.get("SheetConversionCharacteristics") != null){
                                    JSONObject SheetConversionCharacteristics = (JSONObject) Sheet.get("SheetConversionCharacteristics");
                                    //SheetSize
                                    if(SheetConversionCharacteristics.get("SheetSize") != null){
                                        JSONObject SheetSize = (JSONObject) SheetConversionCharacteristics.get("SheetSize");
                                        //Length
                                        if(SheetSize.get("Length") != null){
                                            JSONObject Length = (JSONObject) SheetSize.get("Length");
                                            //Value
                                            if(Length.get("Value") != null){
                                                JSONObject Value = (JSONObject) Length.get("Value");
                                                if(Value.get("Value") != null){
                                                    mov.SheetSize_Length = Integer.parseInt(Value.get("Value").toString());
                                                }
                                            }
                                        }
                                        //Width
                                        if(SheetSize.get("Width") != null){
                                            JSONObject Width = (JSONObject) SheetSize.get("Width");
                                            //Value
                                            if(Width.get("Value") != null){
                                                JSONObject Value = (JSONObject) Width.get("Value");
                                                if(Value.get("Value") != null){
                                                    mov.SheetSize_Width = Integer.parseInt(Value.get("Value").toString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    //PriceDetails
                    if (OrderConfirmationLineItem.get("PriceDetails") != null){
                        JSONObject PriceDetails = (JSONObject) OrderConfirmationLineItem.get("PriceDetails");
                        //PricePerUnit
                        if (PriceDetails.get("PricePerUnit") != null){
                            JSONObject PricePerUnit = (JSONObject) PriceDetails.get("PricePerUnit");
                            
                            //CurrencyValue
                            if (PricePerUnit.get("CurrencyValue") != null){
                                JSONObject CurrencyValue = (JSONObject) PricePerUnit.get("CurrencyValue");
                                //Value
                                if (CurrencyValue.get("Value") != null){
                                    mov.PricePerUnit_CurrencyValue = Double.parseDouble(CurrencyValue.get("Value").toString());
                                }
                            }
                            
                            //Value
                            if (PricePerUnit.get("Value") != null){
                                JSONObject Value = (JSONObject) PricePerUnit.get("Value");
                                //Value
                                if (Value.get("Value") != null){
                                    mov.PricePerUnit_Value = Double.parseDouble(Value.get("Value").toString());
                                }
                                //UOM
                                if (Value.get("UOM") != null){
                                    mov.PricePerUnit_Value_UOM = Value.get("UOM").toString();
                                }
                            }
                        }
                    }
                    
                    //MonetaryAdjustment
                    if (OrderConfirmationLineItem.get("MonetaryAdjustment") != null){
                        JSONObject MonetaryAdjustment = (JSONObject) OrderConfirmationLineItem.get("MonetaryAdjustment");
                        //MonetaryAdjustmentStartAmount
                        if (MonetaryAdjustment.get("MonetaryAdjustmentStartAmount") != null){
                            JSONObject MonetaryAdjustmentStartAmount = (JSONObject) MonetaryAdjustment.get("MonetaryAdjustmentStartAmount");
                            //CurrencyValue
                            if (MonetaryAdjustmentStartAmount.get("CurrencyValue") != null){
                                JSONObject CurrencyValue = (JSONObject) MonetaryAdjustmentStartAmount.get("CurrencyValue");
                                //Value
                                if (CurrencyValue.get("Value") != null){
                                    mov.MonetaryAdjustment_CurrencyValue = Double.parseDouble(CurrencyValue.get("Value").toString());
                                }
                            }   
                        } 
                        
                        //TaxAdjustment
                        if (MonetaryAdjustment.get("TaxAdjustment") != null){
                            JSONObject TaxAdjustment = (JSONObject) MonetaryAdjustment.get("TaxAdjustment");
                            
                            //TaxPercent
                            if (TaxAdjustment.get("TaxPercent") != null){
                                mov.TaxAdjustment_TaxPercent = Double.parseDouble(TaxAdjustment.get("TaxPercent").toString());
                            }
                            
                            //TaxAmount
                            if (TaxAdjustment.get("TaxAmount") != null){
                                JSONObject TaxAmount = (JSONObject) TaxAdjustment.get("TaxAmount");
                                //CurrencyValue
                                if (TaxAmount.get("CurrencyValue") != null){
                                    JSONObject CurrencyValue = (JSONObject) TaxAmount.get("CurrencyValue");
                                    //Value
                                    if (CurrencyValue.get("Value") != null){
                                        mov.TaxAdjustment_TaxAmount = Double.parseDouble(CurrencyValue.get("Value").toString());
                                    }
                                }
                            }
                            
                            //TaxLocation
                            if (TaxAdjustment.get("TaxLocation") != null){
                                mov.TaxAdjustment_TaxLocation = TaxAdjustment.get("TaxLocation").toString();
                            }
                            
                        }
                    }
                    
                    //Quantity
                    if (OrderConfirmationLineItem.get("Quantity") != null){
                        JSONObject Quantity = (JSONObject) OrderConfirmationLineItem.get("Quantity");
                        //Value
                        if (Quantity.get("Value") != null){
                            JSONObject Value = (JSONObject) Quantity.get("Value");
                            if(Value.get("UOM") != null){
                                switch (Value.get("UOM").toString()){
                                    case "Ream":
                                        if(Value.get("Value") != null){ mov.Quantity_Ream = Integer.parseInt(Value.get("Value").toString()); }
                                        break;
                                    case "Kilogram":
                                        if(Value.get("Value") != null){ mov.Quantity_Kilogram = Double.parseDouble(Value.get("Value").toString()); }
                                        break;
                                    case "Sheet":
                                        if(Value.get("Value") != null){ mov.Quantity_Sheet = Integer.parseInt(Value.get("Value").toString()); }
                                        break;
                                }
                            }
                        }
                    }
                    
                    //InformationalQuantity
                    if (OrderConfirmationLineItem.get("InformationalQuantity") != null){
                        JSONArray InformationalsQuantity = (JSONArray) OrderConfirmationLineItem.get("InformationalQuantity");
                        
                        for (int indexQuantity = 0; indexQuantity < InformationalsQuantity.size(); indexQuantity++){
                            JSONObject InformationalQuantity = (JSONObject)InformationalsQuantity.get(indexQuantity);
                            
                            if (InformationalQuantity.get("Value") != null){
                                JSONObject Value = (JSONObject) InformationalQuantity.get("Value");
                                if(Value.get("UOM") != null){
                                    switch (Value.get("UOM").toString()){
                                        case "Ream":
                                            if(Value.get("Value") != null){ mov.Quantity_Ream = Integer.parseInt(Value.get("Value").toString()); }
                                            break;
                                        case "Kilogram":
                                            if(Value.get("Value") != null){ mov.Quantity_Kilogram = Double.parseDouble(Value.get("Value").toString()); }
                                            break;
                                        case "Sheet":
                                            if(Value.get("Value") != null){ mov.Quantity_Sheet = Integer.parseInt(Value.get("Value").toString()); }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    
                    doc.OrderConfirmationLineItem.add(mov);
                }
                
            }
        }
    }
    
    public boolean Save(){
        
        try{
            if (doc != null) {
                Integer identificador = Utilizadores.FornecedorUpdate(doc.SupplierParty_PartyIdentifier,
                        doc.SupplierParty_Name,
                        doc.SupplierParty_Address1,
                        doc.SupplierParty_Address2,
                        doc.SupplierParty_PostalCode,
                        doc.SupplierParty_City,
                        doc.SupplierParty_Country,
                        doc.SupplierParty_Country_ISO);

                if (identificador != null) {                    
                    Integer idDocumento = Documentos.DocumentoInsert(doc.OrderConfirmationReference,
                            identificador,
                            doc.Date_Day,
                            doc.Date_Month,
                            doc.Date_Year,
                            jsonFile);

                    if (idDocumento != null) {
                        for (DocumentosLinhas.DocumentosLinhasXML mov : doc.OrderConfirmationLineItem) {
                            Integer quantidade = null;
                            Double pCusto = null;
                            int sheets = 0;

                            Produtos prod = Produtos.getProduto(mov.PaperCharacteristics_BasisWeight, mov.SheetSize_Width, mov.SheetSize_Length);

                            if (mov.Quantity_Sheet != null) {
                                quantidade = mov.Quantity_Sheet;
                            } else if(mov.Quantity_Ream != null) {
                                quantidade = UnitConvert.fromReamsToSheets(mov.Quantity_Ream);
                            } else if(mov.Quantity_Kilogram != null) {
                                quantidade = UnitConvert.fromGramsToSheets("Kilogram", mov.Quantity_Kilogram, prod.getPeso());
                            }

                            switch (mov.PricePerUnit_Value_UOM) {
                                case "Ream":
                                    sheets = UnitConvert.fromReamsToSheets((int)(Math.round(mov.PricePerUnit_Value)));
                                    pCusto = (mov.PricePerUnit_CurrencyValue / sheets);
                                    break;
                                case "Kilogram":
                                    sheets = UnitConvert.fromGramsToSheets("Kilogram", (int)(Math.round(mov.PricePerUnit_Value)), prod.getPeso());
                                    pCusto = (mov.PricePerUnit_CurrencyValue / sheets);
                                    break;
                                case "Sheet":
                                    sheets = (int)(Math.round(mov.PricePerUnit_Value));
                                    pCusto = (mov.PricePerUnit_CurrencyValue / sheets);
                                    break;
                            }

                            DocumentosLinhas.DocumentoLinhaInsert(idDocumento, prod.getIdProduto(), quantidade, pCusto, mov.TaxAdjustment_TaxPercent, mov.TaxAdjustment_TaxLocation);
                        }
                    }
                }
            }
        }catch (Exception ex){
            return false;
        }
        return true;
    }
    
    public void cleanValues(){
        jsonFile = null;
        doc = null;
        txtImportarFicheiro.setText("");
        pnlTable.setVisible(false);
    }
    
    public void previewData() {
        pnlTable.setVisible(true);
        previewCabecalho();
        constructTblLinhas();
    }
    
    
    private void previewCabecalho() {
        dadosDocumento();
        dadosFornecedor();
    }
    private void dadosDocumento() {
        String nr = doc.OrderConfirmationReference;
        txtDocReferencia.setText(nr);
        
        if (doc.Date_Year != null && doc.Date_Month!= null && doc.Date_Day != null){
            Calendar c = Calendar.getInstance();
            c.set(doc.Date_Year, doc.Date_Month - 1, doc.Date_Day);
            Date dia = c.getTime();    
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy/MM/dd");

            txtDocDia.setText(ft.format(dia));
        }
    }
    private void dadosFornecedor() {
        String nome = doc.SupplierParty_Name;
        String morada1 = doc.SupplierParty_Address1;
        String morada2 = doc.SupplierParty_Address2;
        String cidade = doc.SupplierParty_City;
        String codPostal = doc.SupplierParty_PostalCode;
        String pais = doc.SupplierParty_Country;
        
        txtFornNome.setText(nome);
        txtFornMorada1.setText(morada1);
        txtFornMorada2.setText(morada2);
        txtFornCidade.setText(cidade);
        txtFornCodPostal.setText(codPostal);
        txtFornPais.setText(pais);        
    }    
    
    private void constructTblLinhas() {
        tblLinhas.getItems().clear();

        // Código Produto Fornecedor
        TableColumn<Lin, String> codProdForn = new TableColumn<>("Cód. Produto");
        codProdForn.setMinWidth(150);
        codProdForn.setCellValueFactory(new PropertyValueFactory<>("codProdForn"));
        // Código Produto Cliente
        TableColumn<Lin, String> codProdCliente = new TableColumn<>("Cód. Prod. Cliente");
        codProdCliente.setMinWidth(150);
        codProdCliente.setCellValueFactory(new PropertyValueFactory<>("codProd"));
        // IVA
        TableColumn<Lin, String> iva = new TableColumn<>("IVA");
        iva.setMinWidth(75);
        iva.setCellValueFactory(new PropertyValueFactory<>("iva"));
        // Preço Custo Unitário --> PricePerUnit_Value
        TableColumn<Lin, String> preco = new TableColumn<>("Preço Uni.");
        preco.setMinWidth(100);
        preco.setCellValueFactory(new PropertyValueFactory<>("precoUni"));
        // Total --> TaxAdjustment_TaxAmount
        TableColumn<Lin, String> total = new TableColumn<>("Total");
        total.setMinWidth(100);
        total.setCellValueFactory(new PropertyValueFactory<>("total"));

        tblLinhas.setItems(getDataLinhas());
        tblLinhas.getColumns().addAll(codProdForn, codProdCliente, iva, preco, total);

        tblLinhas.setRowFactory(tv -> new TableRow<Lin>() {
            Node infoPane;
            Node qtdPane;

            {
                this.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
                    if (isNowSelected) {
                        infoPane = tblInfo(getItem());
                        qtdPane = tblQuantidades(getItem());
                        this.getChildren().add(infoPane);
                        this.getChildren().add(qtdPane);
                    } else {
                        this.getChildren().remove(infoPane);
                        this.getChildren().remove(qtdPane);
                    }
                    this.requestLayout();
                });

            }

            @Override
            protected double computePrefHeight(double width) {
                if (isSelected()) {
                    return super.computePrefHeight(width) + infoPane.prefHeight(60) + qtdPane.prefHeight(60);
                } else {
                    return super.computePrefHeight(width);
                }
            }

            @Override
            protected void layoutChildren() {
                super.layoutChildren();
                if (isSelected()) {
                    double width = getWidth();
                    double paneHeight = infoPane.prefHeight(width);
                    double pane2Height = qtdPane.prefHeight(width);
                    infoPane.resizeRelocate(10, getHeight() - paneHeight - pane2Height, width - 20, paneHeight - 10);
                    qtdPane.resizeRelocate(10, getHeight() - pane2Height, width - 20, pane2Height - 10);
                }
            }
        });
    }
    private TableView<Lin> tblInfo(Lin linha) {
        TableView<Lin> subTable = new TableView<>();
        // ALTURA
        TableColumn<Lin, String> altura = new TableColumn<>("Altura");
        altura.setMinWidth(100);
        altura.setCellValueFactory(new PropertyValueFactory<>("altura"));
        // LARGURA
        TableColumn<Lin, String> largura = new TableColumn<>("Largura");
        largura.setMinWidth(100);
        largura.setCellValueFactory(new PropertyValueFactory<>("largura"));
        // GRAMAGEM
        TableColumn<Lin, String> gramagem = new TableColumn<>("Gramagem");
        gramagem.setMinWidth(100);
        gramagem.setCellValueFactory(new PropertyValueFactory<>("gramagem"));

        subTable.setItems(FXCollections.observableArrayList(linha));
        subTable.getColumns().addAll(altura, largura, gramagem);
        int prefHeight = 75;
        subTable.setPrefHeight(prefHeight);
        return subTable;
    }    
    private TableView<Lin> tblQuantidades(Lin linha) {
        TableView<Lin> subTable = new TableView<>();
        // FOLHAS
        TableColumn<Lin, String> folhas = new TableColumn<>("Folhas");
        folhas.setMinWidth(100);
        folhas.setCellValueFactory(new PropertyValueFactory<>("folhas"));
        // PESO
        TableColumn<Lin, String> peso = new TableColumn<>("Peso");
        peso.setMinWidth(100);
        peso.setCellValueFactory(new PropertyValueFactory<>("peso"));
        // RESMAS
        TableColumn<Lin, String> resmas = new TableColumn<>("Resmas");
        resmas.setMinWidth(100);
        resmas.setCellValueFactory(new PropertyValueFactory<>("resmas"));

        subTable.setItems(FXCollections.observableArrayList(linha));
        subTable.getColumns().addAll(folhas, peso, resmas);
        int prefHeight = 75;
        subTable.setPrefHeight(prefHeight);
        return subTable;
    }
    private ObservableList<Lin> getDataLinhas() {
        ArrayList<Lin> lst = new ArrayList<>();
        
        for(DocumentosLinhas.DocumentosLinhasXML item: doc.OrderConfirmationLineItem) {            
            Lin newItem = new Lin();
            newItem.setCodProdForn(item.ProductIdentifier_Buyer);
            newItem.setCodProd(item.ProductIdentifier_Supplier);
            newItem.setIVA(item.TaxAdjustment_TaxPercent);
            newItem.setPrecoUni(item.PricePerUnit_CurrencyValue);
            newItem.setTotal(item.MonetaryAdjustment_CurrencyValue);
            
            newItem.setLargura(item.SheetSize_Length);
            newItem.setAltura(item.SheetSize_Width);
            newItem.setGramagem(item.PaperCharacteristics_BasisWeight);    
            
            newItem.setFolhas(item.Quantity_Sheet);
            newItem.setPeso(item.Quantity_Kilogram);
            newItem.setResmas(item.Quantity_Ream);
            
            lst.add(newItem);            
        }
        
        return FXCollections.observableArrayList(lst);
    }
    public static class Lin {

        private static DecimalFormat df0 = new DecimalFormat("#.#");
        private static DecimalFormat df2 = new DecimalFormat("#.00");
        private static DecimalFormat df4 = new DecimalFormat("#.0000");
        
        private final SimpleStringProperty codProdForn;
        private final SimpleStringProperty codProd;
        private final SimpleStringProperty iva;
        private final SimpleStringProperty precoUni;
        private final SimpleStringProperty total;
        
        // INFO        
        private final SimpleStringProperty altura;
        private final SimpleStringProperty largura;
        private final SimpleStringProperty gramagem;
        
        // QUANTIDADES
        private final SimpleStringProperty folhas;
        private final SimpleStringProperty peso;
        private final SimpleStringProperty resmas;
        
        
        Lin(){
            this.codProdForn = new SimpleStringProperty("");
            this.codProd = new SimpleStringProperty("");
            this.iva = new SimpleStringProperty("");
            this.precoUni = new SimpleStringProperty("");
            this.total = new SimpleStringProperty("");
            this.altura = new SimpleStringProperty("");
            this.largura = new SimpleStringProperty("");
            this.gramagem = new SimpleStringProperty("");
            this.folhas = new SimpleStringProperty("-");
            this.peso = new SimpleStringProperty("-");
            this.resmas = new SimpleStringProperty("-");
        }

        public String getCodProdForn() {
            return codProdForn.get();
        }
        public void setCodProdForn(String codProdutoForn) {
            this.codProdForn.set(codProdutoForn);
        }

        public String getCodProd() {
            return codProd.get();
        }
        public void setCodProd(String codProduto) {
            this.codProd.set(codProduto);
        }

        public String getIva() {
            return iva.get();
        }
        public void setIVA(double taxaIVA) {
            this.iva.set(df0.format(taxaIVA));
        }
        
        public String getPrecoUni() {
            return precoUni.get();
        }
        public void setPrecoUni(double precoUni) {
            this.precoUni.set(df2.format(precoUni) + " €");
        }

        public String getTotal() {
            return total.get();
        }
        public void setTotal(double total) {
            this.total.set(df2.format(total) + " €");
        }

        // INFO
        public String getAltura() {
            return altura.get();
        }
        public void setAltura(int altura) {
            this.altura.set(String.valueOf(altura));
        }

        public String getLargura() {
            return largura.get();
        }
        public void setLargura(int largura) {
            this.largura.set(String.valueOf(largura));
        }

        public String getGramagem() {
            return gramagem.get();
        }
        public void setGramagem(int gramagem) {
            this.gramagem.set(String.valueOf(gramagem) + " gsm");
        }

        // QUANTIDADES
        public String getFolhas() {
            return folhas.get();
        }
        public void setFolhas(Integer folhas) {
            String txtFolhas = "-";
            if (folhas != null){
                if (folhas != 0) {
                    txtFolhas = String.valueOf(folhas);
                }
            }
            this.folhas.set(txtFolhas);
        }
        public String getPeso() {
            return peso.get();
        }
        public void setPeso(Double peso) {
            String txtPeso = "-";
            if (peso != null){
                if (peso != 0) {
                    txtPeso = df4.format(peso);
                }            
            }
            this.peso.set(txtPeso);
        }        
        public String getResmas() {
            return resmas.get();
        }
        public void setResmas(Integer resmas) {
            String txtResmas = "-";
            if(resmas != null){
                if (resmas != 0) {
                    txtResmas = String.valueOf(resmas);
                }
            }
            this.resmas.set(txtResmas);
        }


    }
    
}
