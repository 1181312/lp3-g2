/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Tipos;

/**
 * FXML Controller class
 *
 * @author tiagosantos
 */
public class MasterController implements Initializable {

    @FXML
    private Label lblUser;
    @FXML
    private Button btLogOff;
    @FXML
    private Button btnImportXML;
    @FXML
    private Button btnImportJSON;
    @FXML
    private Button btnManageStock;
    @FXML
    private Button btnManageUsers;
    @FXML
    private AnchorPane AnchorContent;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblUser.setText(Helper.LoginUser.getNome());
        
        switch (Helper.LoginUser.getIdTIpo()){
            case Tipos.Admin:
                btnImportXML.setVisible(false);
                btnImportJSON.setVisible(false);
                Helper.showPage("/fxml/ListaUtilizadores.fxml", AnchorContent);
                break;
            case Tipos.Oper:
                btnImportXML.setVisible(false);
                btnImportJSON.setVisible(false);
                Helper.showPage("/fxml/ListaUtilizadores.fxml", AnchorContent);
                break;
            case Tipos.Forn:
                btnManageUsers.setVisible(false);
                btnManageStock.setVisible(false);
                Helper.showPage("/fxml/ImportarFicheiroXML.fxml", AnchorContent);
                break;
        }
    }    

    @FXML
    private void btnImportXML_OnAction(ActionEvent event) {
        Helper.showPage("/fxml/ImportarFicheiroXML.fxml", AnchorContent);
    }
    
    @FXML
    private void btnImportJSON_OnAction(ActionEvent event) {
        Helper.showPage("/fxml/ImportarFicheiroJSON.fxml", AnchorContent);
    }

    @FXML
    private void btnManageStock_OnAction(ActionEvent event) {
        Helper.showPage("/fxml/ListagemStock.fxml", AnchorContent);
    }

    @FXML
    private void btnManageUsers_OnAction(ActionEvent event) {
        Helper.showPage("/fxml/ListaUtilizadores.fxml", AnchorContent);
    }
    
    @FXML
    private void btLogOff_OnAction(ActionEvent event) throws IOException {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/fxml/Login.fxml"));
                Parent root = loader.load();
                
                Scene scn = new Scene(root);
                scn.getStylesheets().add("/styles/Styles.css");
                
                Stage stg = (Stage) btLogOff.getScene().getWindow();
                stg.setTitle("Feira&Paper");
                stg.setScene(scn);
                stg.show();
    }
}
