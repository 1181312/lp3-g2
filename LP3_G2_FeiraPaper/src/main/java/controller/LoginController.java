/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.Utilizadores;

/**
 * FXML Controller class
 *
 *
 */
public class LoginController implements Initializable {

    @FXML
    private TextField txtUser;
    @FXML
    private TextField txtPassword;
    @FXML
    private Button btnLogin;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bindForm();
    }

    @FXML
    private void btnLogin_click(ActionEvent event) throws IOException {
        login();
    }

    @FXML
    private void txtPassword_OnKeyReleased(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            login();
        }
    }

    /**
     * Método para preparar o formulário de login.
     */
    private void bindForm() {
        txtUser.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtUser.getText().length() > 32) {
                txtUser.deleteNextChar();
                Helper.showInfoAlert("O campo Username não pode exceder 32 caracteres.");
            }

            if (!newValue.matches("^[a-zA-Z0-9]+$")) {
                txtUser.deleteNextChar();
            }
        });

        txtPassword.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtPassword.getText().length() > 512) {
                txtPassword.deleteNextChar();
                Helper.showInfoAlert("O campo Password não pode exceder os 512 caracteres.");
            }
        });
    }

    /**
     * Método para efectuar o login na aplicação.
     *
     * @throws java.io.IOException
     */
    public void login() throws IOException {
        if (validarCampos()) {
            if (validarLogin()) {
                
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/fxml/Master.fxml"));
                Parent root = loader.load();
                
                Scene scn = new Scene(root);
                scn.getStylesheets().add("/styles/Styles.css");
                
                Stage stg = (Stage) btnLogin.getScene().getWindow();
                stg.setTitle("Feira&Paper");
                stg.setScene(scn);
                stg.show();
            }
        }
    }

    /**
     * Método para validar os campos do formulário de login.
     *
     * @return
     */
    public boolean validarCampos() {
        String user = txtUser.getText();
        if ("".equals(user) || !Helper.validarUsername(user)) {
            Helper.showErrorAlert("Nome de Utilizador é de preenchimento obrigatório!");
            return false;
        }

        String pass = txtPassword.getText();
        if ("".equals(pass)) {
            Helper.showErrorAlert("Password é de preenchimento obrigatório!");
            return false;
        }

        return true;
    }

    /**
     * Método para validar o login na BD
     *
     * @return
     */
    public boolean validarLogin() {
        String user = txtUser.getText();
        String pass = txtPassword.getText();

        Utilizadores loginUser = new Utilizadores(user, pass);
        if (loginUser.getIdUtilizador() == 0 || !loginUser.isAtivo()) {
            Helper.showErrorAlert("Nome de Utilizador ou Password inválido(a).");
            return false;
        }

        Helper.LoginUser = loginUser;
        return true;
    }
}
