package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import model.Documentos.ListaDocumentos;
import model.DocumentosLinhas.ListaDocumentosLinhas;
import model.Tipos;

/**
 * FXML Controller class
 *
 * @author Bruno
 */
public class ListagemDocumentosLinhasController implements Initializable {
    
    @FXML
    private AnchorPane anchor;

    String Ordem;
    @FXML
    private TableView<ListaDocumentos> tabDocumentosLinhas;
    
    @FXML
    private TableView<ListaDocumentos> tabEncomendasLinhas;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        switch (Helper.LoginUser.getIdTIpo()){
            case Tipos.Forn:
                tabDocumentosLinhas.setVisible(false);
                tabEncomendasLinhas.setVisible(false);
                break;
            default:
                constructTable();
                constructTableEnc();
                break;
    }
        
}    

    /**
     * Metodo para contruir a tabela principal
     */
    public void constructTable() {
        TableColumn<ListaDocumentos, String> nrDocCol = new TableColumn<>("Documento");
        nrDocCol.setMinWidth(150);
        nrDocCol.setEditable(false);
        nrDocCol.setCellValueFactory(new PropertyValueFactory<>("nrDoc"));

        TableColumn<ListaDocumentos, String> FornecedorCol = new TableColumn<>("Fornecedor");
        FornecedorCol.setMinWidth(350);
        FornecedorCol.setEditable(false);
        FornecedorCol.setCellValueFactory(new PropertyValueFactory<>("Fornecedor"));

        TableColumn<ListaDocumentos, String> dataCol = new TableColumn<>("Data");
        dataCol.setMinWidth(100);
        dataCol.setEditable(false);
        dataCol.setCellValueFactory(new PropertyValueFactory<>("Data"));

        tabDocumentosLinhas.getItems().clear();
        tabDocumentosLinhas.setItems(FXCollections.observableArrayList(ListaDocumentos.getListaDocumentos(ListaDocumentos.EN_LISTA_DOCUMENTOS_CONTACORRENTE)));

        tabDocumentosLinhas.getColumns().clear();
        tabDocumentosLinhas.getColumns().addAll(nrDocCol, FornecedorCol, dataCol);

        tabDocumentosLinhas.setEditable(true);
        tabDocumentosLinhas.setRowFactory(tv -> new TableRow<ListaDocumentos>() {
            Node detailsPane;
            {
                this.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
                    if (isNowSelected) {
                        detailsPane = constructSubTable(getItem());
                        this.getChildren().add(detailsPane);
                    } else {
                        this.getChildren().remove(detailsPane);
                    }
                    this.requestLayout();
                });

            }

            @Override
            protected double computePrefHeight(double width) {
                if (isSelected()) {
                    return super.computePrefHeight(width) + detailsPane.prefHeight(60);
                } else {
                    return super.computePrefHeight(width);
                }
            }

            @Override
            protected void layoutChildren() {
                super.layoutChildren();
                if (isSelected()) {
                    double width = getWidth();
                    double paneHeight = detailsPane.prefHeight(width);
                    detailsPane.resizeRelocate(10, getHeight() - paneHeight, width - 20, paneHeight);
                }
            }
        });

    }

    private TableView<ListaDocumentosLinhas> constructSubTable(ListaDocumentos documentoPendente) {

        List<ListaDocumentosLinhas> DocumentosLinhasPendentes = documentoPendente.getDocumentosLinhas();

        TableView<ListaDocumentosLinhas> subTable = new TableView<>();

        TableColumn<ListaDocumentosLinhas, String> CodProdutoCol = new TableColumn<>("Produto");
        CodProdutoCol.setMinWidth(100);
        CodProdutoCol.setCellValueFactory(new PropertyValueFactory<>("CodProduto"));

        TableColumn<ListaDocumentosLinhas, String> CodProdutoFornecedorCol = new TableColumn<>("Prod. Forn.");
        CodProdutoFornecedorCol.setMinWidth(140);
        CodProdutoFornecedorCol.setCellValueFactory(new PropertyValueFactory<>("CodProdutoFornecedor"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeFolhasCol = new TableColumn<>("Qtd. (Folhas)");
        QuantidadeFolhasCol.setMinWidth(100);
        QuantidadeFolhasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeFolhas"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeResmasCol = new TableColumn<>("Qtd. (Resmas)");
        QuantidadeResmasCol.setMinWidth(100);
        QuantidadeResmasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeResmas"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeKgCol = new TableColumn<>("Qtd. (Kg)");
        QuantidadeKgCol.setMinWidth(100);
        QuantidadeKgCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeKg"));

        TableColumn<ListaDocumentosLinhas, String> PrecoCol = new TableColumn<>("P. Custo (€)");
        PrecoCol.setMinWidth(100);
        PrecoCol.setCellValueFactory(new PropertyValueFactory<>("Preco"));

        subTable.setItems(FXCollections.observableArrayList(DocumentosLinhasPendentes));
        subTable.getColumns().addAll(CodProdutoCol, CodProdutoFornecedorCol, QuantidadeFolhasCol, QuantidadeResmasCol, QuantidadeKgCol, PrecoCol);
        subTable.setPrefHeight(50 + (DocumentosLinhasPendentes.size() * 25));
        subTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        return subTable;
    }
    
    /**
     * Metodo para contruir a tabela principal
     */
    public void constructTableEnc() {
        TableColumn<ListaDocumentos, String> nrDocCol = new TableColumn<>("Encomenda");
        nrDocCol.setMinWidth(150);
        nrDocCol.setEditable(false);
        nrDocCol.setCellValueFactory(new PropertyValueFactory<>("nrDoc"));

        TableColumn<ListaDocumentos, String> FornecedorCol = new TableColumn<>("Cliente");
        FornecedorCol.setMinWidth(350);
        FornecedorCol.setEditable(false);
        FornecedorCol.setCellValueFactory(new PropertyValueFactory<>("Fornecedor"));

        TableColumn<ListaDocumentos, String> dataCol = new TableColumn<>("Data");
        dataCol.setMinWidth(100);
        dataCol.setEditable(false);
        dataCol.setCellValueFactory(new PropertyValueFactory<>("Data"));

        tabEncomendasLinhas.getItems().clear();
        tabEncomendasLinhas.setItems(FXCollections.observableArrayList(ListaDocumentos.getListaDocumentos(ListaDocumentos.EN_LISTA_ENCOMENDAS_CONTACORRENTE)));

        tabEncomendasLinhas.getColumns().clear();
        tabEncomendasLinhas.getColumns().addAll(nrDocCol, FornecedorCol, dataCol);

        tabEncomendasLinhas.setEditable(true);
        tabEncomendasLinhas.setRowFactory(tv -> new TableRow<ListaDocumentos>() {
            Node detailsPane;
            {
                this.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
                    if (isNowSelected) {
                        detailsPane = constructSubTableEnc(getItem());
                        this.getChildren().add(detailsPane);
                    } else {
                        this.getChildren().remove(detailsPane);
                    }
                    this.requestLayout();
                });

            }

            @Override
            protected double computePrefHeight(double width) {
                if (isSelected()) {
                    return super.computePrefHeight(width) + detailsPane.prefHeight(60);
                } else {
                    return super.computePrefHeight(width);
                }
            }

            @Override
            protected void layoutChildren() {
                super.layoutChildren();
                if (isSelected()) {
                    double width = getWidth();
                    double paneHeight = detailsPane.prefHeight(width);
                    detailsPane.resizeRelocate(10, getHeight() - paneHeight, width - 20, paneHeight);
                }
            }
        });
    }
    
    private TableView<ListaDocumentosLinhas> constructSubTableEnc(ListaDocumentos documentoPendente) {

        List<ListaDocumentosLinhas> DocumentosLinhasPendentes = documentoPendente.getDocumentosLinhas();

        TableView<ListaDocumentosLinhas> subTable = new TableView<>();

        TableColumn<ListaDocumentosLinhas, String> CodProdutoCol = new TableColumn<>("Produto");
        CodProdutoCol.setMinWidth(100);
        CodProdutoCol.setCellValueFactory(new PropertyValueFactory<>("CodProduto"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeFolhasCol = new TableColumn<>("Qtd. (Folhas)");
        QuantidadeFolhasCol.setMinWidth(100);
        QuantidadeFolhasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeFolhas"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeResmasCol = new TableColumn<>("Qtd. (Resmas)");
        QuantidadeResmasCol.setMinWidth(100);
        QuantidadeResmasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeResmas"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeKgCol = new TableColumn<>("Qtd. (Kg)");
        QuantidadeKgCol.setMinWidth(100);
        QuantidadeKgCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeKg"));

        TableColumn<ListaDocumentosLinhas, String> PrecoCol = new TableColumn<>("P. Custo (€)");
        PrecoCol.setMinWidth(100);
        PrecoCol.setCellValueFactory(new PropertyValueFactory<>("Preco"));

        subTable.setItems(FXCollections.observableArrayList(DocumentosLinhasPendentes));
        subTable.getColumns().addAll(CodProdutoCol, QuantidadeFolhasCol, QuantidadeResmasCol, QuantidadeKgCol, PrecoCol);
        subTable.setPrefHeight(50 + (DocumentosLinhasPendentes.size() * 25));
        subTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        return subTable;
    }
}
