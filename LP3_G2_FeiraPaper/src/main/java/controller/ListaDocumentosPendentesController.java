/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import model.Documentos.ListaDocumentos;
import model.DocumentosLinhas.ListaDocumentosLinhas;
import model.Tipos;

/**
 * FXML Controller class
 *
 * @author ricardopinho
 */
public class ListaDocumentosPendentesController implements Initializable {

    @FXML
    private AnchorPane anchor;
    @FXML
    private Button btAprovar;
    @FXML
    private Button btReprovar;
    @FXML
    private Pane paneTable;
    @FXML
    private TableView<ListaDocumentos> tblDocumentos;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        switch (Helper.LoginUser.getIdTIpo()) {
            case Tipos.Forn:
                btAprovar.setVisible(false);
                btReprovar.setVisible(false);
                break;
            default:
                constructTable();
                break;
        }
    }

    @FXML
    private void btAprovar_OnAction(ActionEvent event) {
        //Iterate through all items in ObservableList
        for (ListaDocumentos documento : tblDocumentos.getItems()) {
            if (documento.getSelected()) {
                documento.validar(true);
            }
        }
        constructTable();
    }

    @FXML
    private void btReprovar_OnAction(ActionEvent event) {
        //Iterate through all items in ObservableList
        for (ListaDocumentos documento : tblDocumentos.getItems()) {
            if (documento.getSelected()) {
                documento.validar(false);
            }
        }
        constructTable();
    }

    /**
     * Metodo para selecionar todas as checkBoxes da tabela principal
     * @param e
     */
    private void selectAllBoxes(ActionEvent e) {
        //Iterate through all items in ObservableList
        for (ListaDocumentos documento : tblDocumentos.getItems()) {
            documento.setSelected(((CheckBox) e.getSource()).isSelected());
        }
        tblDocumentos.getSelectionModel().clearSelection();
        tblDocumentos.refresh();

    }

    /**
     * Metodo para contruir a tabela principal
     */
    public void constructTable() {
        TableColumn<ListaDocumentos, String> nrDocCol = new TableColumn<>("Documento");
        nrDocCol.setMinWidth(150);
        nrDocCol.setEditable(false);
        nrDocCol.setCellValueFactory(new PropertyValueFactory<>("nrDoc"));

        TableColumn<ListaDocumentos, String> FornecedorCol = new TableColumn<>("Fornecedor");
        FornecedorCol.setMinWidth(350);
        FornecedorCol.setEditable(false);
        FornecedorCol.setCellValueFactory(new PropertyValueFactory<>("Fornecedor"));

        TableColumn<ListaDocumentos, String> dataCol = new TableColumn<>("Data");
        dataCol.setMinWidth(100);
        dataCol.setEditable(false);
        dataCol.setCellValueFactory(new PropertyValueFactory<>("Data"));

        TableColumn<ListaDocumentos, Boolean> validoCol = new TableColumn<>();
        validoCol.setMinWidth(70);
        CheckBox chkValidoHeader = new CheckBox();
        chkValidoHeader.setOnAction(e -> selectAllBoxes(e));
        validoCol.setGraphic(chkValidoHeader);

        Callback<TableColumn<ListaDocumentos, Boolean>, TableCell<ListaDocumentos, Boolean>> cellFactory = (param) -> {
            final TableCell<ListaDocumentos, Boolean> cell = new TableCell<ListaDocumentos, Boolean>() {
                @Override
                public void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setAlignment(Pos.TOP_CENTER);
                    if (!empty) {
                        CheckBox chkValido = new CheckBox();
                        ListaDocumentos modelTabela = getTableView().getItems().get(getIndex());
                        chkValido.setSelected(modelTabela.getSelected());
                        chkValido.selectedProperty().addListener(new ChangeListener<Boolean>() {
                            public void changed(ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) {
                                modelTabela.setSelected(new_val);
                            }
                        });

                        setGraphic(chkValido);
                    } else {
                        setGraphic(null);
                    }
                }
            };
            return cell;
        };

        validoCol.setCellFactory(cellFactory);
        tblDocumentos.getItems().clear();
        tblDocumentos.setItems(FXCollections.observableArrayList(ListaDocumentos.getListaDocumentos(ListaDocumentos.EN_LISTA_DOCUMENTOS_PENDENTES)));

        tblDocumentos.getColumns().clear();
        tblDocumentos.getColumns().addAll(nrDocCol, FornecedorCol, dataCol, validoCol);

        tblDocumentos.setEditable(true);
        tblDocumentos.setRowFactory(tv -> new TableRow<ListaDocumentos>() {
            Node detailsPane;
            {
                this.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
                    if (isNowSelected) {
                        detailsPane = constructSubTable(getItem());
                        this.getChildren().add(detailsPane);
                    } else {
                        this.getChildren().remove(detailsPane);
                    }
                    this.requestLayout();
                });

            }

            @Override
            protected double computePrefHeight(double width) {
                if (isSelected()) {
                    return super.computePrefHeight(width) + detailsPane.prefHeight(60);
                } else {
                    return super.computePrefHeight(width);
                }
            }

            @Override
            protected void layoutChildren() {
                super.layoutChildren();
                if (isSelected()) {
                    double width = getWidth();
                    double paneHeight = detailsPane.prefHeight(width);
                    detailsPane.resizeRelocate(10, getHeight() - paneHeight, width - 20, paneHeight);
                }
            }
        });

    }

    private TableView<ListaDocumentosLinhas> constructSubTable(ListaDocumentos documentoPendente) {

        List<ListaDocumentosLinhas> DocumentosLinhasPendentes = documentoPendente.getDocumentosLinhas();

        TableView<ListaDocumentosLinhas> subTable = new TableView<>();

        TableColumn<ListaDocumentosLinhas, String> CodProdutoCol = new TableColumn<>("Produto");
        CodProdutoCol.setMinWidth(100);
        CodProdutoCol.setCellValueFactory(new PropertyValueFactory<>("CodProduto"));

        TableColumn<ListaDocumentosLinhas, String> CodProdutoFornecedorCol = new TableColumn<>("Prod. Forn.");
        CodProdutoFornecedorCol.setMinWidth(140);
        CodProdutoFornecedorCol.setCellValueFactory(new PropertyValueFactory<>("CodProdutoFornecedor"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeFolhasCol = new TableColumn<>("Qtd. (Folhas)");
        QuantidadeFolhasCol.setMinWidth(100);
        QuantidadeFolhasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeFolhas"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeResmasCol = new TableColumn<>("Qtd. (Resmas)");
        QuantidadeResmasCol.setMinWidth(100);
        QuantidadeResmasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeResmas"));

        TableColumn<ListaDocumentosLinhas, String> QuantidadeKgCol = new TableColumn<>("Qtd. (Kg)");
        QuantidadeKgCol.setMinWidth(100);
        QuantidadeKgCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeKg"));

        TableColumn<ListaDocumentosLinhas, String> PrecoCol = new TableColumn<>("P. Custo (€)");
        PrecoCol.setMinWidth(100);
        PrecoCol.setCellValueFactory(new PropertyValueFactory<>("Preco"));

        subTable.setItems(FXCollections.observableArrayList(DocumentosLinhasPendentes));
        subTable.getColumns().addAll(CodProdutoCol, CodProdutoFornecedorCol, QuantidadeFolhasCol, QuantidadeResmasCol, QuantidadeKgCol, PrecoCol);
        subTable.setPrefHeight(50 + (DocumentosLinhasPendentes.size() * 25));
        subTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        return subTable;
    }
}
