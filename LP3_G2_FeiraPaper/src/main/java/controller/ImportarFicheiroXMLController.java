/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import com.mycompany.lp3_g2_feirapaper.UnitConvert;
import com.mycompany.lp3_g2_feirapaper.XMLHelper;
import com.mycompany.lp3_g2_feirapaper.XMLValidator;
import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import model.Documentos;
import model.DocumentosLinhas;
import model.Produtos;
import model.Utilizadores;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * FXML Controller class
 *
 * @author Bruno
 */
public class ImportarFicheiroXMLController implements Initializable {

    @FXML
    private TextField txtImportarFicheiro;
    @FXML
    private Button btnProcurar;
    @FXML
    private Button btnImportar;

    File xmlFile;
    Documentos.DocumentoXML doc;

    @FXML
    private AnchorPane pnlTable;
    @FXML
    private TableView tblLinhas;
    @FXML
    private TextField txtDocReferencia;
    @FXML
    private TextField txtDocDia;
    @FXML
    private TextField txtFornNome;
    @FXML
    private TextField txtFornMorada1;
    @FXML
    private TextField txtFornMorada2;
    @FXML
    private TextField txtFornCidade;
    @FXML
    private TextField txtFornCodPostal;
    @FXML
    private TextField txtFornPais;
    @FXML
    private Accordion accord;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pnlTable.setVisible(false);
    
           accord.expandedPaneProperty().addListener((ObservableValue<? extends TitledPane> ov, TitledPane old_val, TitledPane new_val) -> {
            if (new_val != null){
                tblLinhas.getSelectionModel().clearSelection();
                tblLinhas.refresh();
            }
        });
        
    }

    /**
     *
     * Ação para selecionar o ficheiro, apenas do tipo xml, para a importação
     */
    @FXML
    public void Procurar(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("XML Files", "*.xml"));

        xmlFile = fileChooser.showOpenDialog(null);

        if (xmlFile == null) {
            Helper.showErrorAlert("O ficheiro não é valido!");
            return;
        } else {
            txtImportarFicheiro.setText(xmlFile.getName());
        }

        XMLValidator validator = new XMLValidator();

        if (validator.XMLFileIsValid(xmlFile) == null) {
            Helper.showErrorAlert("O ficheiro XML não está bem formatado.");
            cleanValues();
            return;
        } else {
            if (validator.validateStructeFromXMLFile(xmlFile)) {
                if (!validator.validateHeaderFromXMLFile(xmlFile, Helper.LoginUser.getIdentificador().toString())) {
                    Helper.showErrorAlert(validator.showWarningsMessage());
                    cleanValues();
                    return;
                }

                if (!validator.validateLinesFromXMLFile(xmlFile,  Helper.LoginUser.getIdentificador().toString())) {
                    Helper.showErrorAlert(validator.showWarningsMessage());
                    cleanValues();
                    return;
                }

                SaveInMemory();
                previewData();

            }
        }

    }

    /**
     * Função para importar o documento XML.
     *
     * @param event
     */
    @FXML
    public void Importar(ActionEvent event) {
        if(txtImportarFicheiro.getText().isEmpty()){
            Helper.showInfoAlert("Selecione um ficheiro XML.");
            return;
        }
        
        if(Save()){
            Helper.showInfoAlert("Ficheiro XML importado com sucesso.");
        }else{
            Helper.showInfoAlert("Ficheiro XML não foi importado");
        }
        
        cleanValues();
    }

    public void SaveInMemory() {

        XMLValidator validator = new XMLValidator();
        Document xml = validator.XMLFileIsValid(xmlFile);

        doc = new Documentos.DocumentoXML();
        doc.OrderConfirmationLineItem = new ArrayList<DocumentosLinhas.DocumentosLinhasXML>();

        //OrderConfirmation
        if (xml.getElementsByTagName("OrderConfirmation").getLength() > 0) {
            Element order = XMLHelper.getChildElement(xml.getElementsByTagName("OrderConfirmation"));

            //OrderConfirmation - OrderConfirmationHeader
            if (order.getElementsByTagName("OrderConfirmationHeader").getLength() > 0) {
                Element header = XMLHelper.getChildElement(order.getElementsByTagName("OrderConfirmationHeader"));
                //OrderConfirmation - OrderConfirmationHeader - OrderConfirmationReference
                if (header.getElementsByTagName("OrderConfirmationReference").getLength() > 0) {
                    doc.OrderConfirmationReference = XMLHelper.getValueByTagName(header, "OrderConfirmationReference");
                }

                //OrderConfirmation - OrderConfirmationHeader - OrderConfirmationIssuedDate
                if (header.getElementsByTagName("OrderConfirmationIssuedDate").getLength() > 0) {
                    Element orderConfirmationIssuedDate = XMLHelper.getChildElement(header.getElementsByTagName("OrderConfirmationIssuedDate"));
                    //OrderConfirmation - OrderConfirmationHeader - OrderConfirmationIssuedDate - Date
                    if (orderConfirmationIssuedDate.getElementsByTagName("Date").getLength() > 0) {
                        Element date = XMLHelper.getChildElement(orderConfirmationIssuedDate.getElementsByTagName("Date"));
                        //OrderConfirmation - OrderConfirmationHeader - OrderConfirmationIssuedDate - Date - Year
                        if (date.getElementsByTagName("Year").getLength() > 0) {
                            doc.Date_Year = Integer.parseInt(XMLHelper.getValueByTagName(date, "Year"));
                        }
                        //OrderConfirmation - OrderConfirmationHeader - OrderConfirmationIssuedDate - Date - Month
                        if (date.getElementsByTagName("Month").getLength() > 0) {
                            doc.Date_Month = Integer.parseInt(XMLHelper.getValueByTagName(date, "Month"));
                        }
                        //OrderConfirmation - OrderConfirmationHeader - OrderConfirmationIssuedDate - Date - Day
                        if (date.getElementsByTagName("Day").getLength() > 0) {
                            doc.Date_Day = Integer.parseInt(XMLHelper.getValueByTagName(date, "Day"));
                        }
                    }
                }

                //OrderConfirmation - OrderConfirmationHeader - SupplierParty
                if (header.getElementsByTagName("SupplierParty").getLength() > 0) {
                    Element supplier = XMLHelper.getChildElement(header.getElementsByTagName("SupplierParty"));
                    //OrderConfirmation - OrderConfirmationHeader - SupplierParty - PartyIdentifier
                    if (supplier.getElementsByTagName("PartyIdentifier").getLength() > 0) {
                        doc.SupplierParty_PartyIdentifier = Integer.parseInt(XMLHelper.getValueByTagName(supplier, "PartyIdentifier"));
                    }
                    //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress
                    if (supplier.getElementsByTagName("NameAddress").getLength() > 0) {
                        Element nameAddress = XMLHelper.getChildElement(supplier.getElementsByTagName("NameAddress"));

                        //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress - Name
                        if (nameAddress.getElementsByTagName("Name").getLength() > 0) {
                            doc.SupplierParty_Name = XMLHelper.getValueByTagName(nameAddress, "Name");
                        }
                        //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress - Address1
                        if (nameAddress.getElementsByTagName("Address1").getLength() > 0) {
                            doc.SupplierParty_Address1 = XMLHelper.getValueByTagName(nameAddress, "Address1");
                        }
                        //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress - Address2
                        if (nameAddress.getElementsByTagName("Address2").getLength() > 0) {
                            doc.SupplierParty_Address2 = XMLHelper.getValueByTagName(nameAddress, "Address2");
                        }
                        //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress - City
                        if (nameAddress.getElementsByTagName("City").getLength() > 0) {
                            doc.SupplierParty_City = XMLHelper.getValueByTagName(nameAddress, "City");
                        }
                        //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress - PostalCode
                        if (nameAddress.getElementsByTagName("PostalCode").getLength() > 0) {
                            doc.SupplierParty_PostalCode = XMLHelper.getValueByTagName(nameAddress, "PostalCode");
                        }
                        //OrderConfirmation - OrderConfirmationHeader - SupplierParty - NameAddress - Country
                        if (nameAddress.getElementsByTagName("Country").getLength() > 0) {
                            doc.SupplierParty_Country = XMLHelper.getValueByTagName(nameAddress, "Country");
                            doc.SupplierParty_Country_ISO = XMLHelper.getAttributeValueByTagName(nameAddress, "Country", "ISOCountryCode");
                        }

                    }
                }
            }

            //OrderConfirmationLineItem
            if (order.getElementsByTagName("OrderConfirmationLineItem").getLength() > 0) {
                for (int index = 0; index < order.getElementsByTagName("OrderConfirmationLineItem").getLength(); index++) {
                    Element line = XMLHelper.getChildElement(order.getElementsByTagName("OrderConfirmationLineItem"), index);

                    DocumentosLinhas.DocumentosLinhasXML mov = new DocumentosLinhas.DocumentosLinhasXML();

                    //OrderConfirmationLineItemNumber
                    if (line.getElementsByTagName("OrderConfirmationLineItemNumber").getLength() > 0) {
                        mov.OrderConfirmationLineItemNumber = Integer.parseInt(XMLHelper.getValueByTagName(line, "OrderConfirmationLineItemNumber"));
                    }

                    //Product
                    if (line.getElementsByTagName("Product").getLength() > 0) {
                        Element product = XMLHelper.getChildElement(line.getElementsByTagName("Product"));

                        //Product - ProductIdentifier
                        if (product.getElementsByTagName("ProductIdentifier").getLength() > 0) {
                            for (int indexProductIdentifier = 0; indexProductIdentifier < product.getElementsByTagName("ProductIdentifier").getLength(); indexProductIdentifier++) {
                                Element productIdentifier = XMLHelper.getChildElement(product.getElementsByTagName("ProductIdentifier"), indexProductIdentifier);
                                if (productIdentifier.getAttribute("Agency") != null) {
                                    switch (productIdentifier.getAttribute("Agency")) {
                                        case "Buyer":
                                            mov.ProductIdentifier_Buyer = XMLHelper.getValueByTagName(product, "ProductIdentifier", indexProductIdentifier);
                                            break;
                                        case "Supplier":
                                            mov.ProductIdentifier_Supplier = XMLHelper.getValueByTagName(product, "ProductIdentifier", indexProductIdentifier);
                                            break;
                                    }
                                }
                            }
                        }

                        //Product - Paper
                        if (product.getElementsByTagName("Paper").getLength() > 0) {
                            Element paper = XMLHelper.getChildElement(product.getElementsByTagName("Paper"));
                            // Product - Paper - PaperCharacteristics
                            if (paper.getElementsByTagName("PaperCharacteristics").getLength() > 0) {
                                Element paperCharacteristics = XMLHelper.getChildElement(paper.getElementsByTagName("PaperCharacteristics"));
                                // Product - Paper - PaperCharacteristics - BasisWeight
                                if (paperCharacteristics.getElementsByTagName("BasisWeight").getLength() > 0) {
                                    Element basisWeight = XMLHelper.getChildElement(paperCharacteristics.getElementsByTagName("BasisWeight"));
                                    // Product - Paper - PaperCharacteristics - BasisWeight - DetailValue
                                    if (basisWeight.getElementsByTagName("DetailValue").getLength() > 0) {
                                        mov.PaperCharacteristics_BasisWeight = Integer.parseInt(XMLHelper.getValueByTagName(basisWeight, "DetailValue"));
                                    }
                                }
                            }

                            // Product - Paper - Sheet
                            if (paper.getElementsByTagName("Sheet").getLength() > 0) {
                                Element sheet = XMLHelper.getChildElement(paper.getElementsByTagName("Sheet"));
                                // Product - Paper - Sheet - SheetConversionCharacteristics
                                if (sheet.getElementsByTagName("SheetConversionCharacteristics").getLength() > 0) {
                                    Element sheetConversionCharacteristics = XMLHelper.getChildElement(sheet.getElementsByTagName("SheetConversionCharacteristics"));
                                    // Product - Paper - Sheet - SheetConversionCharacteristics - SheetSize
                                    if (sheetConversionCharacteristics.getElementsByTagName("SheetSize").getLength() > 0) {
                                        Element sheetSize = XMLHelper.getChildElement(sheetConversionCharacteristics.getElementsByTagName("SheetSize"));
                                        // Product - Paper - Sheet - SheetConversionCharacteristics - SheetSize - Length
                                        if (sheetSize.getElementsByTagName("Length").getLength() > 0) {
                                            Element length = XMLHelper.getChildElement(sheetSize.getElementsByTagName("Length"));
                                            // Product - Paper - Sheet - SheetConversionCharacteristics - SheetSize - Width - Value
                                            if (length.getElementsByTagName("Value").getLength() > 0) {
                                                mov.SheetSize_Length = Integer.parseInt(XMLHelper.getValueByTagName(length, "Value"));
                                            }
                                        }
                                        // Product - Paper - Sheet - SheetConversionCharacteristics - SheetSize - Width
                                        if (sheetSize.getElementsByTagName("Width").getLength() > 0) {
                                            Element width = XMLHelper.getChildElement(sheetSize.getElementsByTagName("Width"));
                                            // Product - Paper - Sheet - SheetConversionCharacteristics - SheetSize - Width - Value
                                            if (width.getElementsByTagName("Value").getLength() > 0) {
                                                mov.SheetSize_Width = Integer.parseInt(XMLHelper.getValueByTagName(width, "Value"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //PriceDetails
                    if (line.getElementsByTagName("PriceDetails").getLength() > 0) {
                        Element priceDetails = XMLHelper.getChildElement(line.getElementsByTagName("PriceDetails"));
                        //PriceDetails - PricePerUnit
                        if (priceDetails.getElementsByTagName("PricePerUnit").getLength() > 0) {
                            Element pricePerUnit = XMLHelper.getChildElement(priceDetails.getElementsByTagName("PricePerUnit"));
                            //PriceDetails - PricePerUnit - CurrencyValue
                            if (pricePerUnit.getElementsByTagName("CurrencyValue").getLength() > 0) {
                                mov.PricePerUnit_CurrencyValue = Double.parseDouble(XMLHelper.getValueByTagName(pricePerUnit, "CurrencyValue"));
                            }
                            //PriceDetails - PricePerUnit - Value
                            if (pricePerUnit.getElementsByTagName("Value").getLength() > 0) {
                                mov.PricePerUnit_Value = Double.parseDouble(XMLHelper.getValueByTagName(pricePerUnit, "Value"));
                                mov.PricePerUnit_Value_UOM = XMLHelper.getAttributeValueByTagName(pricePerUnit, "Value", "UOM");
                            }
                        }
                    }

                    //MonetaryAdjustment
                    if (line.getElementsByTagName("MonetaryAdjustment").getLength() > 0) {
                        Element monetaryAdjustment = XMLHelper.getChildElement(line.getElementsByTagName("MonetaryAdjustment"));
                        //MonetaryAdjustment - MonetaryAdjustmentStartAmount
                        if (monetaryAdjustment.getElementsByTagName("MonetaryAdjustmentStartAmount").getLength() > 0) {
                            Element monetaryAdjustmentStartAmount = XMLHelper.getChildElement(monetaryAdjustment.getElementsByTagName("MonetaryAdjustmentStartAmount"));
                            //MonetaryAdjustment - MonetaryAdjustmentStartAmount - CurrencyValue
                            if (monetaryAdjustmentStartAmount.getElementsByTagName("CurrencyValue").getLength() > 0) {
                                mov.MonetaryAdjustment_CurrencyValue = Double.parseDouble(XMLHelper.getValueByTagName(monetaryAdjustmentStartAmount, "CurrencyValue"));
                            }
                        }

                        //MonetaryAdjustment - TaxAdjustment
                        if (monetaryAdjustment.getElementsByTagName("TaxAdjustment").getLength() > 0) {
                            Element taxAdjustment = XMLHelper.getChildElement(monetaryAdjustment.getElementsByTagName("TaxAdjustment"));
                            //MonetaryAdjustment - TaxAdjustment - TaxPercent
                            if (taxAdjustment.getElementsByTagName("TaxPercent").getLength() > 0) {
                                mov.TaxAdjustment_TaxPercent = Double.parseDouble(XMLHelper.getValueByTagName(taxAdjustment, "TaxPercent"));
                            }

                            //MonetaryAdjustment - TaxAdjustment - TaxAmount
                            if (taxAdjustment.getElementsByTagName("TaxAmount").getLength() > 0) {
                                Element taxAmount = XMLHelper.getChildElement(taxAdjustment.getElementsByTagName("TaxAmount"));
                                //MonetaryAdjustment - TaxAdjustment - TaxAmount - CurrencyValue
                                if (taxAmount.getElementsByTagName("CurrencyValue").getLength() > 0) {
                                    mov.TaxAdjustment_TaxAmount = Double.parseDouble(XMLHelper.getValueByTagName(taxAmount, "CurrencyValue"));
                                }
                            }

                            //MonetaryAdjustment - TaxAdjustment - TaxLocation
                            if (taxAdjustment.getElementsByTagName("TaxLocation").getLength() > 0) {
                                mov.TaxAdjustment_TaxLocation = XMLHelper.getValueByTagName(taxAdjustment, "TaxLocation");
                            }
                        }
                    }

                    //Quantity
                    if (line.getElementsByTagName("Quantity").getLength() > 0) {
                        Element quantity = XMLHelper.getChildElement(line.getElementsByTagName("Quantity"));
                        if (quantity.getElementsByTagName("Value").getLength() > 0) {
                            switch (XMLHelper.getAttributeValueByTagName(quantity, "Value", "UOM")) {
                                case "Ream":
                                    mov.Quantity_Ream = Integer.parseInt(XMLHelper.getValueByTagName(quantity, "Value"));
                                    break;
                                case "Kilogram":
                                    mov.Quantity_Kilogram = Double.parseDouble(XMLHelper.getValueByTagName(quantity, "Value"));
                                    break;
                                case "Sheet":
                                    mov.Quantity_Sheet = Integer.parseInt(XMLHelper.getValueByTagName(quantity, "Value"));
                                    break;
                            }
                        }
                    }
                    //InformationalQuantity
                    if (line.getElementsByTagName("InformationalQuantity").getLength() > 0) {
                        for (int indexInformationalQuantity = 0; indexInformationalQuantity < line.getElementsByTagName("InformationalQuantity").getLength(); indexInformationalQuantity++) {
                            Element informationalQuantity = XMLHelper.getChildElement(line.getElementsByTagName("InformationalQuantity"), indexInformationalQuantity);
                            if (informationalQuantity.getElementsByTagName("Value").getLength() > 0) {
                                switch (XMLHelper.getAttributeValueByTagName(informationalQuantity, "Value", "UOM")) {
                                    case "Ream":
                                        mov.Quantity_Ream = Integer.parseInt(XMLHelper.getValueByTagName(informationalQuantity, "Value"));
                                        break;
                                    case "Kilogram":
                                        mov.Quantity_Kilogram = Double.parseDouble(XMLHelper.getValueByTagName(informationalQuantity, "Value"));
                                        break;
                                    case "Sheet":
                                        mov.Quantity_Sheet = Integer.parseInt(XMLHelper.getValueByTagName(informationalQuantity, "Value"));
                                        break;
                                }
                            }
                        }
                    }
                    doc.OrderConfirmationLineItem.add(mov);
                }

            }
        }
    }

    public boolean Save() {

        try{
            if (doc != null) {
                Integer identificador = Utilizadores.FornecedorUpdate(doc.SupplierParty_PartyIdentifier,
                        doc.SupplierParty_Name,
                        doc.SupplierParty_Address1,
                        doc.SupplierParty_Address2,
                        doc.SupplierParty_PostalCode,
                        doc.SupplierParty_City,
                        doc.SupplierParty_Country,
                        doc.SupplierParty_Country_ISO);

                if (identificador != null) {
                    Integer idDocumento = Documentos.DocumentoInsert(doc.OrderConfirmationReference,
                            identificador,
                            doc.Date_Day,
                            doc.Date_Month,
                            doc.Date_Year,
                            xmlFile);

                    if (idDocumento != null) {
                        for (DocumentosLinhas.DocumentosLinhasXML mov : doc.OrderConfirmationLineItem) {
                            Integer quantidade = null;
                            Double pCusto = null;
                            int sheets = 0;

                            Produtos prod = Produtos.getProduto(mov.PaperCharacteristics_BasisWeight, mov.SheetSize_Width, mov.SheetSize_Length);

                            if (mov.Quantity_Sheet != null) {
                                quantidade = mov.Quantity_Sheet;
                            } else if(mov.Quantity_Ream != null) {
                                quantidade = UnitConvert.fromReamsToSheets(mov.Quantity_Ream);
                            } else if(mov.Quantity_Kilogram != null) {
                                quantidade = UnitConvert.fromGramsToSheets("Kilogram", mov.Quantity_Kilogram, prod.getPeso());
                            }

                            switch (mov.PricePerUnit_Value_UOM) {
                                case "Ream":
                                    sheets = UnitConvert.fromReamsToSheets((int)(Math.round(mov.PricePerUnit_Value)));
                                    pCusto = (mov.PricePerUnit_CurrencyValue / sheets);
                                    break;
                                case "Kilogram":
                                    sheets = UnitConvert.fromGramsToSheets("Kilogram", (int)(Math.round(mov.PricePerUnit_Value)), prod.getPeso());
                                    pCusto = (mov.PricePerUnit_CurrencyValue / sheets);
                                    break;
                                case "Sheet":
                                    sheets = (int)(Math.round(mov.PricePerUnit_Value));
                                    pCusto = (mov.PricePerUnit_CurrencyValue / sheets);
                                    break;
                            }

                            DocumentosLinhas.DocumentoLinhaInsert(idDocumento, prod.getIdProduto(), quantidade, pCusto, mov.TaxAdjustment_TaxPercent, mov.TaxAdjustment_TaxLocation);
                        }
                    }
                }
            }
        }catch (Exception ex){
            return false;
        }
        return true;
    }

    public void cleanValues(){
        xmlFile = null;
        doc = null;
        txtImportarFicheiro.setText("");
        pnlTable.setVisible(false);
    }
    
    public void previewData() {
        pnlTable.setVisible(true);
        previewCabecalho();
        constructTblLinhas();
    }
    
    
    private void previewCabecalho() {
        dadosDocumento();
        dadosFornecedor();
    }
    private void dadosDocumento() {
        String nr = doc.OrderConfirmationReference;
        Calendar c = Calendar.getInstance();
        c.set(doc.Date_Year, doc.Date_Month - 1, doc.Date_Day);
        Date dia = c.getTime();    
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy/MM/dd");
        
        txtDocReferencia.setText(nr);
        txtDocDia.setText(ft.format(dia));
    }
    private void dadosFornecedor() {
        String nome = doc.SupplierParty_Name;
        String morada1 = doc.SupplierParty_Address1;
        String morada2 = doc.SupplierParty_Address2;
        String cidade = doc.SupplierParty_City;
        String codPostal = doc.SupplierParty_PostalCode;
        String pais = doc.SupplierParty_Country;
        
        txtFornNome.setText(nome);
        txtFornMorada1.setText(morada1);
        txtFornMorada2.setText(morada2);
        txtFornCidade.setText(cidade);
        txtFornCodPostal.setText(codPostal);
        txtFornPais.setText(pais);        
    }    
    
    private void constructTblLinhas() {
        tblLinhas.getItems().clear();

        // Código Produto Fornecedor
        TableColumn<Lin, String> codProdForn = new TableColumn<>("Cód. Produto");
        codProdForn.setMinWidth(150);
        codProdForn.setCellValueFactory(new PropertyValueFactory<>("codProdForn"));
        // Código Produto Cliente
        TableColumn<Lin, String> codProdCliente = new TableColumn<>("Cód. Prod. Cliente");
        codProdCliente.setMinWidth(150);
        codProdCliente.setCellValueFactory(new PropertyValueFactory<>("codProd"));
        // IVA
        TableColumn<Lin, String> iva = new TableColumn<>("IVA");
        iva.setMinWidth(75);
        iva.setCellValueFactory(new PropertyValueFactory<>("iva"));
        // Preço Custo Unitário --> PricePerUnit_Value
        TableColumn<Lin, String> preco = new TableColumn<>("Preço Uni.");
        preco.setMinWidth(100);
        preco.setCellValueFactory(new PropertyValueFactory<>("precoUni"));
        // Total --> TaxAdjustment_TaxAmount
        TableColumn<Lin, String> total = new TableColumn<>("Total");
        total.setMinWidth(100);
        total.setCellValueFactory(new PropertyValueFactory<>("total"));

        tblLinhas.setItems(getDataLinhas());
        tblLinhas.getColumns().addAll(codProdForn, codProdCliente, iva, preco, total);

        tblLinhas.setRowFactory(tv -> new TableRow<Lin>() {
            Node infoPane;
            Node qtdPane;

            {
                this.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
                    if (isNowSelected) {
                        infoPane = tblInfo(getItem());
                        qtdPane = tblQuantidades(getItem());
                        this.getChildren().add(infoPane);
                        this.getChildren().add(qtdPane);
                    } else {
                        this.getChildren().remove(infoPane);
                        this.getChildren().remove(qtdPane);
                    }
                    this.requestLayout();
                });

            }

            @Override
            protected double computePrefHeight(double width) {
                if (isSelected()) {
                    return super.computePrefHeight(width) + infoPane.prefHeight(60) + qtdPane.prefHeight(60);
                } else {
                    return super.computePrefHeight(width);
                }
            }

            @Override
            protected void layoutChildren() {
                super.layoutChildren();
                if (isSelected()) {
                    double width = getWidth();
                    double paneHeight = infoPane.prefHeight(width);
                    double pane2Height = qtdPane.prefHeight(width);
                    infoPane.resizeRelocate(10, getHeight() - paneHeight - pane2Height, width - 20, paneHeight - 10);
                    qtdPane.resizeRelocate(10, getHeight() - pane2Height, width - 20, pane2Height - 10);
                }
            }
        });
    }
    private TableView<Lin> tblInfo(Lin linha) {
        TableView<Lin> subTable = new TableView<>();
        // ALTURA
        TableColumn<Lin, String> altura = new TableColumn<>("Altura");
        altura.setMinWidth(100);
        altura.setCellValueFactory(new PropertyValueFactory<>("altura"));
        // LARGURA
        TableColumn<Lin, String> largura = new TableColumn<>("Largura");
        largura.setMinWidth(100);
        largura.setCellValueFactory(new PropertyValueFactory<>("largura"));
        // GRAMAGEM
        TableColumn<Lin, String> gramagem = new TableColumn<>("Gramagem");
        gramagem.setMinWidth(100);
        gramagem.setCellValueFactory(new PropertyValueFactory<>("gramagem"));

        subTable.setItems(FXCollections.observableArrayList(linha));
        subTable.getColumns().addAll(altura, largura, gramagem);
        int prefHeight = 75;
        subTable.setPrefHeight(prefHeight);
        return subTable;
    }    
    private TableView<Lin> tblQuantidades(Lin linha) {
        TableView<Lin> subTable = new TableView<>();
        // FOLHAS
        TableColumn<Lin, String> folhas = new TableColumn<>("Folhas");
        folhas.setMinWidth(100);
        folhas.setCellValueFactory(new PropertyValueFactory<>("folhas"));
        // PESO
        TableColumn<Lin, String> peso = new TableColumn<>("Peso");
        peso.setMinWidth(100);
        peso.setCellValueFactory(new PropertyValueFactory<>("peso"));
        // RESMAS
        TableColumn<Lin, String> resmas = new TableColumn<>("Resmas");
        resmas.setMinWidth(100);
        resmas.setCellValueFactory(new PropertyValueFactory<>("resmas"));

        subTable.setItems(FXCollections.observableArrayList(linha));
        subTable.getColumns().addAll(folhas, peso, resmas);
        int prefHeight = 75;
        subTable.setPrefHeight(prefHeight);
        return subTable;
    }
    private ObservableList<Lin> getDataLinhas() {
        ArrayList<Lin> lst = new ArrayList<>();
        
        for(DocumentosLinhas.DocumentosLinhasXML item: doc.OrderConfirmationLineItem) {            
            Lin newItem = new Lin();
            newItem.setCodProdForn(item.ProductIdentifier_Buyer);
            newItem.setCodProd(item.ProductIdentifier_Supplier);
            newItem.setIVA(item.TaxAdjustment_TaxPercent);
            newItem.setPrecoUni(item.PricePerUnit_CurrencyValue);
            newItem.setTotal(item.MonetaryAdjustment_CurrencyValue);
            
            newItem.setLargura(item.SheetSize_Length);
            newItem.setAltura(item.SheetSize_Width);
            newItem.setGramagem(item.PaperCharacteristics_BasisWeight);    
            
            newItem.setFolhas(item.Quantity_Sheet);
            newItem.setPeso(item.Quantity_Kilogram);
            newItem.setResmas(item.Quantity_Ream);
            
            lst.add(newItem);            
        }
        
        return FXCollections.observableArrayList(lst);
    }
    public static class Lin {

        private static DecimalFormat df0 = new DecimalFormat("#.#");
        private static DecimalFormat df2 = new DecimalFormat("#.00");
        private static DecimalFormat df4 = new DecimalFormat("#.0000");
        
        private final SimpleStringProperty codProdForn;
        private final SimpleStringProperty codProd;
        private final SimpleStringProperty iva;
        private final SimpleStringProperty precoUni;
        private final SimpleStringProperty total;
        
        // INFO        
        private final SimpleStringProperty altura;
        private final SimpleStringProperty largura;
        private final SimpleStringProperty gramagem;
        
        // QUANTIDADES
        private final SimpleStringProperty folhas;
        private final SimpleStringProperty peso;
        private final SimpleStringProperty resmas;
        
        
        Lin(){
            this.codProdForn = new SimpleStringProperty("");
            this.codProd = new SimpleStringProperty("");
            this.iva = new SimpleStringProperty("");
            this.precoUni = new SimpleStringProperty("");
            this.total = new SimpleStringProperty("");
            this.altura = new SimpleStringProperty("");
            this.largura = new SimpleStringProperty("");
            this.gramagem = new SimpleStringProperty("");
            this.folhas = new SimpleStringProperty("-");
            this.peso = new SimpleStringProperty("-");
            this.resmas = new SimpleStringProperty("-");
        }

        public String getCodProdForn() {
            return codProdForn.get();
        }
        public void setCodProdForn(String codProdutoForn) {
            this.codProdForn.set(codProdutoForn);
        }

        public String getCodProd() {
            return codProd.get();
        }
        public void setCodProd(String codProduto) {
            this.codProd.set(codProduto);
        }

        public String getIva() {
            return iva.get();
        }
        public void setIVA(double taxaIVA) {
            this.iva.set(df0.format(taxaIVA));
        }
        
        public String getPrecoUni() {
            return precoUni.get();
        }
        public void setPrecoUni(double precoUni) {
            this.precoUni.set(df2.format(precoUni) + " €");
        }

        public String getTotal() {
            return total.get();
        }
        public void setTotal(double total) {
            this.total.set(df2.format(total) + " €");
        }

        // INFO
        public String getAltura() {
            return altura.get();
        }
        public void setAltura(int altura) {
            this.altura.set(String.valueOf(altura));
        }

        public String getLargura() {
            return largura.get();
        }
        public void setLargura(int largura) {
            this.largura.set(String.valueOf(largura));
        }

        public String getGramagem() {
            return gramagem.get();
        }
        public void setGramagem(int gramagem) {
            this.gramagem.set(String.valueOf(gramagem) + " gsm");
        }

        // QUANTIDADES
        public String getFolhas() {
            return folhas.get();
        }
        public void setFolhas(Integer folhas) {
            String txtFolhas = "-";
            if (folhas != null){
                if (folhas != 0) {
                    txtFolhas = String.valueOf(folhas);
                }
            }
            this.folhas.set(txtFolhas);
        }
        public String getPeso() {
            return peso.get();
        }
        public void setPeso(Double peso) {
            String txtPeso = "-";
            if(peso != null){
                if (peso != 0) {
                    txtPeso = df4.format(peso);
                }            
            }
            this.peso.set(txtPeso);
        }        
        public String getResmas() {
            return resmas.get();
        }
        public void setResmas(Integer resmas) {
            String txtResmas = "-";
            if (resmas != null){
                if (resmas != 0) {
                    txtResmas = String.valueOf(resmas);
                }
            }
            this.resmas.set(txtResmas);
        }


    }

}
