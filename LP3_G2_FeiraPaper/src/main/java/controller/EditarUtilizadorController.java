/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Cidades;
import model.Pais;
import model.Tipos;
import model.Utilizadores;

/**
 * FXML Controller class
 *
 * @author tiagosantos
 */
public class EditarUtilizadorController implements Initializable {

    private int tipoUtilizador;
    private Utilizadores utilizador;

    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private PasswordField txtConfPassword;
    @FXML
    private TextField txtNome;
    @FXML
    private ComboBox<Pais> cbPais;
    @FXML
    private ComboBox<Cidades> cbCidade;
    @FXML
    private TextField txtCodPostal;
    @FXML
    private ComboBox<Tipos> cbTipoUtilizador;
    @FXML
    private Button btnGravar;
    @FXML
    private Button btnSair;
    @FXML
    private TextField txtMorada1;
    @FXML
    private TextField txtMorada2;
    @FXML
    private Label lblIdentificador;
    @FXML
    private TextField txtIdentificador;
    @FXML
    private TextField txtEmail;
    @FXML
    private Label lblEmail;

    /**
     * Retorna o tipo de utilizador que está acerder ao formulário
     *
     * @return tipoUtilizador
     */
    public int getTipoUtilizador() {
        return this.tipoUtilizador;
    }

    /**
     * Insere o tipo de utilizador que está aceder ao formulário
     *
     * @param tipoUtilizador
     */
    public void setTipoUtilizador(int tipoUtilizador) {
        this.tipoUtilizador = tipoUtilizador;
    }

    /**
     * Retorna o utilizador que está no formulário
     *
     * @return utilizador
     */
    public Utilizadores getUtilizador() {
        return this.utilizador;
    }

    /**
     * Insere o utilizador que está neste formulário
     *
     * @param utilizador
     */
    public void setUtilizador(Utilizadores utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        carregarObejctos();
    }

    /**
     * Método para preparar o formulário de registo.
     */
    private void bindForm() {
        txtUsername.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtUsername.getText().length() > 32) {
                txtUsername.deleteNextChar();
                Helper.showInfoAlert("O campo Nome de Utilizador não pode exceder 32 caracteres.");
            }
            if (!Helper.validarChangeUsername(newValue)) {
                txtUsername.deleteNextChar();
            }
        });

        txtPassword.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtPassword.getText().length() > 512) {
                txtPassword.deleteNextChar();
                Helper.showInfoAlert("O campo Palavra-Passe não pode exceder os 512 caracteres.");
            }
        });

        txtConfPassword.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtConfPassword.getText().length() > 512) {
                txtConfPassword.deleteNextChar();
                Helper.showInfoAlert("O campo Palavra-Passe não pode exceder os 512 caracteres.");
            }
        });

        txtNome.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtNome.getText().length() > 255) {
                txtNome.deleteNextChar();
                Helper.showInfoAlert("O campo nome não pode exceder os 255 caracteres.");
            }
            if (!Helper.validarNome(newValue)) {
                txtNome.deleteNextChar();
            }
        });

        txtMorada1.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtMorada1.getText().length() > 255) {
                txtMorada1.deleteNextChar();
                Helper.showInfoAlert("O campo Morada 1 não pode exceder os 255 caracteres.");
            }
        });
        txtMorada2.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtMorada2.getText().length() > 255) {
                txtMorada2.deleteNextChar();
                Helper.showInfoAlert("O campo Morada 2 não pode exceder os 255 caracteres.");
            }
        });

        txtIdentificador.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtIdentificador.getText().length() > 7) {
                txtIdentificador.deleteNextChar();
                Helper.showInfoAlert("O campo Identificador não pode exceder os 7 caracteres.");
            }
            if (!Helper.validarIdentificador(newValue)) {
                txtIdentificador.deleteNextChar();
            }
        });
        
        
        txtEmail.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtEmail.getText().length() > 255) {
                txtEmail.deleteNextChar();
                Helper.showInfoAlert("O campo Email não pode exceder os 255 caracteres.");
            }
        });
    }

    /**
     * Preencher valores nos campos do tipo combobox
     */
    public void carregarObejctos() {
        boolean permiteModificar;

        permiteModificar = !((utilizador != null && getTipoUtilizador() != Tipos.Admin) || (utilizador != null && utilizador.getIdTIpo() == Tipos.Admin) || (utilizador == null && getTipoUtilizador() == Tipos.Forn));

        if (utilizador != null) {
            txtUsername.setDisable(true);
            txtPassword.setDisable(true);
            txtConfPassword.setDisable(true);

            txtUsername.setText(utilizador.getUsername());
            txtNome.setText(utilizador.getNome());
            txtMorada1.setText(utilizador.getMorada1());
            txtMorada2.setText(utilizador.getMorada2());
        }

        txtNome.setDisable(!permiteModificar);
        txtMorada1.setDisable(!permiteModificar);
        txtMorada2.setDisable(!permiteModificar);

        cbPais.setItems(FXCollections.observableArrayList(Pais.getLista()));

        cbPais.setDisable(!permiteModificar);

        if (utilizador != null) {
            if (utilizador.getIdCidade() != null) {
                cbPais.getSelectionModel().select((Pais.getLista("idPais=" + String.valueOf(((Cidades.getLista("idCidade=" + String.valueOf(utilizador.getIdCidade()))).get(0).getIdPais())))).get(0));
            }
        }

        LoadCidadeCodigoPostal();

        if (utilizador != null) {
            if (utilizador.getIdCidade() != null) {
                cbCidade.getSelectionModel().select(Cidades.getLista("idCidade=" + String.valueOf(utilizador.getIdCidade())).get(0));
            }
            txtCodPostal.setText(utilizador.getCodPostal());
        }
        //Apenas inativar senao manter estado que pode ser inativo
        if (!permiteModificar) {
            cbCidade.setDisable(!permiteModificar);
            txtCodPostal.setDisable(!permiteModificar);
        }

        if (tipoUtilizador == Tipos.Admin) {
            cbTipoUtilizador.setItems(FXCollections.observableArrayList(Tipos.getLista("idTipoUtilizador <> 1")));
        } else {
            cbTipoUtilizador.setItems(FXCollections.observableArrayList(Tipos.getLista("idTipoUtilizador = 3")));
        }

        if (utilizador != null) {
            if (utilizador.getIdTIpo() != null) {
                cbTipoUtilizador.getSelectionModel().select(Tipos.getLista("idTipoUtilizador=" + String.valueOf(utilizador.getIdTIpo())).get(0));
            }

            cbTipoUtilizador.setDisable(true);
        }

        ShowHideTxtIdentificador();

        if (utilizador != null) {
            if (utilizador.getIdentificador() != null) {
                txtIdentificador.setText(String.valueOf(utilizador.getIdentificador()));
            } else {
                txtIdentificador.setText((""));
            }
        }

        txtIdentificador.setDisable(!permiteModificar);
        
        
        ShowHideTxtEmail();
        
        if (utilizador != null) {
            if (utilizador.getEmail()!= null) {
                txtEmail.setText(String.valueOf(utilizador.getEmail()));
            } else {
                txtEmail.setText((""));
            }
        }
        
        txtEmail.setDisable(!permiteModificar);

        btnGravar.setVisible(permiteModificar);
    }

    /**
     * Gravar formulário
     *
     * @return boolean
     */
    public boolean Gravar() {
        if (!Validar()) {
            return false;
        }

        if (utilizador == null) {

            String password, salt, hash;
            password = txtPassword.getText();

            //CIFRAR PASSWORD
            try {
                salt = Helper.gerarSalt();
                hash = Helper.cifrar(salt, password);
            } catch (NoSuchAlgorithmException e) {
                return false;
            }

            utilizador = new Utilizadores();

            utilizador.setUsername(txtUsername.getText());
            utilizador.setHashKey(hash);
            utilizador.setSalt(salt);

            utilizador.setIdTIpo(cbTipoUtilizador.getValue().getIdTipo());
        }

        utilizador.setNome(txtNome.getText());
        utilizador.setMorada1(txtMorada1.getText());
        utilizador.setMorada2(txtMorada2.getText());
        if (cbCidade.getValue() != null) {
            utilizador.setIdCidade(cbCidade.getValue().getIdCidade());
            utilizador.setCodPostal(txtCodPostal.getText());
        }

        if (txtIdentificador.getText().isEmpty()) {
            utilizador.setIdentificador(null);
        } else {
            utilizador.setIdentificador(Integer.parseInt(txtIdentificador.getText()));
        }
        
        if (txtEmail.getText().isEmpty()) {
            utilizador.setEmail(null);
        } else {
            utilizador.setEmail(txtEmail.getText());
        }
        
        utilizador.Gravar();
        carregarObejctos();
        return true;
    }

    /**
     * Validar formulário (Campos obrigatórios, requisitos minimos de
     * preenchimentos, etc...)
     *
     * @return boolean
     */
    public Boolean Validar() {

        String username, password, nome, morada1, morada2, codPostal;
        Integer idPais, idCidade, idTipoUtilizador, identificador;
        Boolean validaLogin = true;

        if ((cbTipoUtilizador.getValue() != null) && (cbTipoUtilizador.getValue().getIdTipo() == Tipos.Client)) {
            validaLogin = false;
        }
        // VALIDAR USERNAME 
        if (utilizador == null) {
            if (txtUsername.getText().isEmpty()) {
                if (validaLogin) {
                    Helper.showInfoAlert("Obrigatório preencher o campo Nome de Utilizador!");
                    return false;
                } else {
                    username = "";
                }
            } else {
                username = txtUsername.getText();
                if (!Helper.validarUsername(username)) {
                    Helper.showInfoAlert("Nome de Utilizador invalido!");
                    return false;
                }
                if (username.length() > 32) {
                    Helper.showInfoAlert("Nome de Utilizador invalido!");
                    return false;
                }
                if (Utilizadores.getLista("username = '" + username + "'").size() > 0) {
                    Helper.showInfoAlert("Nome de Utilizador já existe!");
                    return false;
                }
            }
        }

        //VALIDAR PASSWORD
        if (utilizador == null) {
            if ((txtPassword.getText().isEmpty() && txtConfPassword.getText().isEmpty())) {
                if (validaLogin) {
                    Helper.showInfoAlert("Obrigatório preencher o campo Palavra-Passe!");
                    return false;
                } else {
                    password = "";
                }
            } else if ((txtPassword.getText().isEmpty() || txtConfPassword.getText().isEmpty())) {
                if (txtPassword.getText().isEmpty()) {
                    Helper.showInfoAlert("Obrigatório preencher o campo Palavra-Passe!");
                    return false;
                } else if (txtConfPassword.getText().isEmpty()) {
                    Helper.showInfoAlert("Obrigatório preencher o campo Confirmar Palavra-Passe!");
                    return false;
                }
            } else {
                password = txtPassword.getText();
                String confPassword = txtConfPassword.getText();
                if (!Helper.validarPassword(password)) {
                    Helper.showInfoAlert("Palavra-Passe invalida!");
                    return false;
                }
                if (!password.equals(confPassword)) {
                    Helper.showInfoAlert("Palavra-Passe invalida!");
                    return false;
                }
                if (password.length() > 512) {
                    Helper.showInfoAlert("Palavra-Passe invalida!");
                    return false;
                }
            }
        }

        //VALIDAR NOME 
        if (txtNome.getText().isEmpty()) {
            Helper.showInfoAlert("Obrigatório preencher o campo Nome!");
            return false;
        } else {
            nome = txtNome.getText();
            if (!Helper.validarNome(nome)) {
                Helper.showInfoAlert("Nome invalido!");
                return false;
            }
            if (nome.length() > 255) {
                Helper.showInfoAlert("Nome invalido!");
                return false;
            }
            if (nome.trim().isEmpty()) {
                Helper.showInfoAlert("Nome invalido!");
                return false;
            }
        }

        //VALIDAR MORADA
        if (!txtMorada1.getText().isEmpty()) {
            morada1 = txtMorada1.getText();
            if (morada1.length() > 255) {
                Helper.showInfoAlert("Morada inválida!");
                return false;
            }
        }
        if ((txtMorada2.getText()!= null) && (!txtMorada2.getText().isEmpty())) {
            morada2 = txtMorada2.getText();
            if (morada2.length() > 255) {
                Helper.showInfoAlert("Morada inválida!");
                return false;
            }
        }

        //VALIDAR PAIS E CIDADE
        if (cbPais.getValue() != null) {
            if (cbCidade.getValue() != null) {
                idPais = cbPais.getValue().getIdPais();
                idCidade = cbCidade.getValue().getIdCidade();

                //VALIDAR CODIGO POSTAL
                if (!txtCodPostal.getText().isEmpty()) {
                    codPostal = txtCodPostal.getText();
                    String regex = cbPais.getValue().getCodPostalRegex();
                    if (!Helper.validarTextoComRegex(codPostal, regex)) {
                        Helper.showInfoAlert("Codigo Postal inválido!");
                        return false;
                    }
                }
            }
        }

        //VALIDAR TIPO DE UTILIZADOR
        if (cbTipoUtilizador.getValue() == null) {
            Helper.showInfoAlert("Obrigatório selecionar o tipo de utilizador!");
            return false;
        }

        //VALIDAR Identificador
        if (cbTipoUtilizador.getValue().getIdTipo() == Tipos.Forn || cbTipoUtilizador.getValue().getIdTipo() == Tipos.Client) {
            if (txtIdentificador.getText().isEmpty()) {
                Helper.showInfoAlert("Obrigatório preencher o campo Identificador!");
                return false;
            } else {
                if (!Helper.validarIdentificador(txtIdentificador.getText())) {
                    Helper.showInfoAlert("Identificador inválido!");
                    return false;
                }
                if (utilizador == null) {
                    if (Utilizadores.getLista("ativo = 1 AND idTipoUtilizador = " + String.valueOf(cbTipoUtilizador.getValue().getIdTipo()) + " AND  identificador = " + txtIdentificador.getText() + "").size() > 0) {
                        Helper.showInfoAlert("Identificador indicado já se encontra associado!");
                        return false;
                    }
                } else {
                    if (Utilizadores.getLista("ativo = 1 AND idTipoUtilizador = " + String.valueOf(cbTipoUtilizador.getValue().getIdTipo()) + " AND identificador = " + txtIdentificador.getText() + " AND idUtilizador != " + String.valueOf(utilizador.getIdUtilizador())).size() > 0) {
                        Helper.showInfoAlert("Identificador indicado já se encontra associado!");
                        return false;
                    }
                }
            }
        }
        
        //VALIDAR EMAIL CASO SEJA CLIENT
        if (cbTipoUtilizador.getValue().getIdTipo() == Tipos.Client) {
            if (txtEmail.getText().isEmpty()) {
                Helper.showInfoAlert("Obrigatório preencher o campo Email.");
                return false;
            }
        }

        return true;
    }

    /**
     * Mostrar ou Oculta a txtIdentificador pode variar consoante o tipo de
     * utilizador.
     */
    private void ShowHideTxtIdentificador() {
        if ((cbTipoUtilizador.getValue() != null) && (cbTipoUtilizador.getValue().getIdTipo() == Tipos.Forn || cbTipoUtilizador.getValue().getIdTipo() == Tipos.Client)) {
            lblIdentificador.setVisible(true);
            txtIdentificador.setVisible(true);
        } else {
            lblIdentificador.setVisible(false);
            txtIdentificador.setVisible(false);
            txtIdentificador.setText((""));
        }
    }

    private void LoadCidadeCodigoPostal() {
        if (cbPais.getValue() != null) {
            cbCidade.setDisable(false);
            cbCidade.setItems(FXCollections.observableArrayList(Cidades.getLista("idPais=" + cbPais.getValue().getIdPais())));
            txtCodPostal.setDisable(false);
        } else {
            cbCidade.setDisable(true);
            cbCidade.getItems().clear();
            txtCodPostal.setDisable(true);
        }
    }
    
    private void ShowHideTxtEmail(){
        if ((cbTipoUtilizador.getValue() != null) && (cbTipoUtilizador.getValue().getIdTipo() == Tipos.Client)) {
            lblEmail.setVisible(true);
            txtEmail.setVisible(true);
        } else {
            lblEmail.setVisible(false);
            txtEmail.setVisible(false);
            txtEmail.setText((""));
        }
    }

    @FXML
    private void cbPais_OnAction(ActionEvent event) {
        LoadCidadeCodigoPostal();
    }

    @FXML
    private void btnGravar_OnAction(ActionEvent event) {
        if (!Gravar()) {
            return;
        }

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void btnSair_OnAction(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void cbUtilizador_OnAction(ActionEvent event) {
        ShowHideTxtIdentificador();
        ShowHideTxtEmail();
    }

}
