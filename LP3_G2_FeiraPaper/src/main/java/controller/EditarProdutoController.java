/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import static com.mycompany.lp3_g2_feirapaper.Helper.validarInteger;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Produtos;
import model.ProdutosClientes;
import model.ProdutosFornecedores;

/**
 * FXML Controller class
 *
 * @author Hugo Costa (1181312)
 */
public class EditarProdutoController implements Initializable {

    private Produtos produto = new Produtos();

    @FXML
    private TableView tblFornecedores;
    @FXML
    private Button btnGravar;
    @FXML
    private Button btnSair;
    @FXML
    private Label lblPrecoVenda;
    @FXML
    private TextField txtPrecoVenda;
    @FXML
    private TableView tblClientes;

    public Produtos getProduto() {
        return produto;
    }

    public void setProduto(Produtos produto) {
        this.produto = produto;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void carregarObjetos() {
        txtPrecoVenda.setText(String.valueOf(produto.getPrecoVenda()));
        
        constructTblLinhasFornecedores();
        tblFornecedores.setItems(getDataLinhasFornecedores());
        
        constructTblLinhasClientes();
        tblClientes.setItems(getDataLinhasClientes());
    }

    @FXML
    private void btnGravar_OnAction(ActionEvent event) {
        if (!Gravar()) {
            return;
        }

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void btnSair_OnAction(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    private void constructTblLinhasFornecedores() {
        ProdutosFornecedores.ListToProduct.constructTblLinhas(tblFornecedores, btnGravar);
    }
    
    private void constructTblLinhasClientes() {
        ProdutosClientes.ListToClient.constructTblLinhas(tblClientes, btnGravar);
    }
    
    private ObservableList<ProdutosFornecedores.ListToProduct> getDataLinhasFornecedores() {
        ArrayList<ProdutosFornecedores.ListToProduct> lst = ProdutosFornecedores.ListToProduct.getLista(this.produto.getIdProduto());
        return FXCollections.observableArrayList(lst);
    }
    
    private ObservableList<ProdutosClientes.ListToClient> getDataLinhasClientes() {
        ArrayList<ProdutosClientes.ListToClient> lst = ProdutosClientes.ListToClient.getLista(this.produto.getIdProduto());
        return FXCollections.observableArrayList(lst);
    }

    private boolean Gravar() {
        boolean retValue = true;
        
        //Atualizar preço de venda
        if (txtPrecoVenda.getText().isEmpty()){
            Helper.showErrorAlert("Obrigatório preencher o preço de venda.");
            return false;
        }else{
            if (!Helper.validarDouble(txtPrecoVenda.getText())){
                Helper.showErrorAlert("Permitir apenas valores numéricos.");
                return false;
            }
            
            BigDecimal preco = Helper.StringToBigInteger(txtPrecoVenda.getText());
            BigDecimal min = new BigDecimal("0.0");
            BigDecimal max = new BigDecimal("99999999999999.9999");
            if (preco.compareTo(min) < 0){
                Helper.showErrorAlert("Preço de venda inferior à zero.");
                return false;
            }
            if (preco.compareTo(max) > 0){
                Helper.showErrorAlert("Preço de venda superior à 99999999999999.9999.");
                return false;
            }
            produto.setPrecoVenda(preco);
        }

        // Preparar código produto fornecedor a ser enviados da tabela para a bd
        ArrayList<ProdutosFornecedores> lstTblFornecedores = new ArrayList<>();
        for (int i = 0; i < tblFornecedores.getItems().size(); i++) {
            ProdutosFornecedores.ListToProduct lstItem = (ProdutosFornecedores.ListToProduct) tblFornecedores.getItems().get(i);
            if (lstItem.getIdProdForn() != 0) {
                ProdutosFornecedores newItem = new ProdutosFornecedores();
                newItem.setIdFornecedor(lstItem.getIdUtilizador());
                newItem.setIdProduto(produto.getIdProduto());
                newItem.setIdProdutoFornecedor(lstItem.getIdProdForn());

                if (newItem.checkIfExists()) {
                    Helper.showErrorAlert("O código \"" + newItem.getIdProdutoFornecedor() + "\" já se encontra atribuido a outro produto do fornecedor \""+ lstItem.getDescFornecedor() +"\".");
                    return false;
                }
                lstTblFornecedores.add(newItem);
            }
        }

        // Adicionar ao produto os códigos preenchidos
        for (ProdutosFornecedores item : lstTblFornecedores) {
            boolean found = false;
            for (ProdutosFornecedores p : produto.getProdutosFornecedores()) {
                if (p.getIdFornecedor() == item.getIdFornecedor()) {
                    found = true;
                    p.setIdProdutoFornecedor(item.getIdProdutoFornecedor());
                }
            }
            if (!found) {
                ProdutosFornecedores newP = new ProdutosFornecedores();
                newP.setIdProduto(produto.getIdProduto());
                newP.setIdFornecedor(item.getIdFornecedor());
                newP.setIdProdutoFornecedor(item.getIdProdutoFornecedor());
                produto.getProdutosFornecedores().add(newP);
            }
        }

        ArrayList<ProdutosClientes> lstTblClientes = new ArrayList<>();
        for (int i = 0; i < tblClientes.getItems().size(); i++) {
            ProdutosClientes.ListToClient lstItem = (ProdutosClientes.ListToClient) tblClientes.getItems().get(i);
            if (!lstItem.getPrecoVenda().equals(0) ) {
                ProdutosClientes newItem = new ProdutosClientes();
                newItem.setIdCliente(lstItem.getIdCliente());
                newItem.setIdProduto(produto.getIdProduto());
                newItem.setPrecoVenda(lstItem.getPrecoVenda());
                
                lstTblClientes.add(newItem);
            }
        }

        for (ProdutosClientes item : lstTblClientes) {
            boolean found = false;
            for (ProdutosClientes p : produto.getProdutosClientes()) {
                if (p.getIdCliente() == item.getIdCliente()) {
                    found = true;
                    p.setIdProduto(item.getIdCliente());
                }
            }
            if (!found) {
                ProdutosClientes newP = new ProdutosClientes();
                newP.setIdProduto(produto.getIdProduto());
                newP.setIdCliente(item.getIdCliente());
                newP.setPrecoVenda(item.getPrecoVenda());
                if (item.getPrecoVenda().compareTo(BigDecimal.ZERO) < 0) {
                    Helper.showErrorAlert("Preço de venda no separador de cliente inferior a 0.");
                    return false;
                }
                produto.getProdutosClientes().add(newP);
            }
        }
        
        retValue = produto.save();
        return retValue;

    }

}
