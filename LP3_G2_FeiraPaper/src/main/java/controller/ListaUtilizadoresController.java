package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mycompany.lp3_g2_feirapaper.ConnDB;
import com.mycompany.lp3_g2_feirapaper.Helper;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Tipos;
import model.Utilizadores;
import model.Utilizadores.UtilizadoresLista;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ListaUtilizadoresController implements Initializable {
    @FXML
    private TableView<UtilizadoresLista> tabListagemUtilizadores;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabNome;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabUserName;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabTipoUtilizador;
    @FXML
    private AnchorPane anchor;
    @FXML
    private Button btCriarUser;
    @FXML
    private Button btEliminarUser;
    @FXML
    private Button btVoltar;
    @FXML
    private Button btModificarUser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        switch (Helper.LoginUser.getIdTIpo()){
            case Tipos.Oper:
                btModificarUser.setVisible(false);
                btEliminarUser.setVisible(false);
                break;
            case Tipos.Forn:
                btCriarUser.setVisible(false);
                btModificarUser.setVisible(false);
                btEliminarUser.setVisible(false);
                break;
        }
        
        bindGrid();
    }
    
    private void bindGrid() {
        ArrayList<UtilizadoresLista> lst = UtilizadoresLista.getListaUtilizadores(Helper.LoginUser.getIdTIpo());
        ObservableList<UtilizadoresLista> obslst = FXCollections.observableArrayList(lst);
        tabListagemUtilizadores.setItems(obslst);
        tabNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tabUserName.setCellValueFactory(new PropertyValueFactory("Username"));
        tabTipoUtilizador.setCellValueFactory(new PropertyValueFactory("TipoUtilizador"));
    }

    @FXML
    private void btCriarUser_OnAction(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditarUtilizador.fxml"));
            Parent root = loader.load();
            EditarUtilizadorController controller = loader.<EditarUtilizadorController>getController();
            controller.setTipoUtilizador(Helper.LoginUser.getIdTIpo());
            controller.carregarObejctos();
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
            
        } catch (IOException e) {
            Logger.getLogger(ListaUtilizadoresController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bindGrid();
        }
    }

    @FXML
    private void btEliminarUser_OnAction(ActionEvent event) {
        try {
            if (tabListagemUtilizadores.getSelectionModel().getSelectedItem() == null) {
                Helper.showErrorAlert("Necessário selecionar o utilizador que quer eliminar.");
                return;
            }
            
            Optional<ButtonType> confirm = Helper.showConfirmAlert("Eliminar", "Tem a certeza que quer eliminar o utilizador selecionado?");
            if (confirm.get() == ButtonType.OK) { 
                int idUtilizador = tabListagemUtilizadores.getSelectionModel().getSelectedItem().getIdUtilizador();
                Utilizadores.Eliminar(idUtilizador);
            } else {
                return;
            }
        } catch (Exception e) {
            Logger.getLogger(ListaUtilizadoresController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bindGrid();
        }
    }

    @FXML
    private void btModificarUser_OnAction(ActionEvent event) {
        try {
            if (tabListagemUtilizadores.getSelectionModel().getSelectedItem() == null) {
                Helper.showErrorAlert("Necessário selecionar o utilizador que quer editar.");
                return;
            }
            
            if (tabListagemUtilizadores.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditarUtilizador.fxml"));
                Parent root = loader.load();
                EditarUtilizadorController controller = loader.<EditarUtilizadorController>getController();
                controller.setTipoUtilizador(Helper.LoginUser.getIdTIpo());
                controller.setUtilizador(((Utilizadores.getLista("idUtilizador=" + String.valueOf(tabListagemUtilizadores.getSelectionModel().getSelectedItem().getIdUtilizador()))).get(0)));
                controller.carregarObejctos();
                Scene scene = new Scene(root);
                scene.getStylesheets().add("/styles/Styles.css");

                Stage stage = new Stage();
                stage.setScene(scene);
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
            }
        } catch (IOException e) {
            Logger.getLogger(ListaUtilizadoresController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bindGrid();
        }
    }

}
