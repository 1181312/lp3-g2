/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.lp3_g2_feirapaper.Helper;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Produtos;
import model.Produtos.ListaStock;
import model.Tipos;
import model.Utilizadores;
import model.ProdutosClientes;

/**
 * FXML Controller class
 *
 * @author 1181838
 */
public class ListagemStockController implements Initializable {

    @FXML
    private AnchorPane anchor;
    @FXML
    private TableView<ListaStock> tabListagemStock;

    String Ordem;
    @FXML
    private Button btDocumentosPendentes;
    @FXML
    private Button btContaCorrente;
    @FXML
    private Button btFornecedores;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        switch (Helper.LoginUser.getIdTIpo()) {
            case Tipos.Forn:
                tabListagemStock.setVisible(false);
                btDocumentosPendentes.setVisible(false);
                break;
            default:
                Ordem = "idTipoFolha";
                bindGrid();
                break;
        }
    }

    private void bindGrid() {
        ArrayList<ListaStock> lst = ListaStock.getListaStock();
        ObservableList<ListaStock> obslst = FXCollections.observableArrayList(lst);

        
        TableColumn<ListaStock, String> CodProdutoCol = new TableColumn<>("Produto");
        CodProdutoCol.setMinWidth(100);
        CodProdutoCol.setEditable(false);
        CodProdutoCol.setCellValueFactory(new PropertyValueFactory<>("CodProduto"));

        TableColumn<ListaStock, String> TipoPapelCol = new TableColumn<>("Tipo de Papel");
        TipoPapelCol.setMinWidth(100);
        TipoPapelCol.setEditable(false);
        TipoPapelCol.setCellValueFactory(new PropertyValueFactory<>("TipoPapel"));

        TableColumn<ListaStock, String> QuantidadeFolhasCol = new TableColumn<>("Qtd. (Folhas)");
        QuantidadeFolhasCol.setMinWidth(100);
        QuantidadeFolhasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeFolhas"));

        TableColumn<ListaStock, String> QuantidadeResmasCol = new TableColumn<>("Qtd. (Resmas)");
        QuantidadeResmasCol.setMinWidth(125);
        QuantidadeResmasCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeResmas"));

        TableColumn<ListaStock, String> QuantidadeKgCol = new TableColumn<>("Qtd. (Kg)");
        QuantidadeKgCol.setMinWidth(100);
        QuantidadeKgCol.setCellValueFactory(new PropertyValueFactory<>("QuantidadeKg"));

        TableColumn<ListaStock, String> pVendaCol = new TableColumn<>("P. Venda (€)");
        pVendaCol.setMinWidth(125);
        pVendaCol.setCellValueFactory(new PropertyValueFactory<>("pVenda"));
        
        
        tabListagemStock.getItems().clear();
        tabListagemStock.setItems(obslst);
        tabListagemStock.getColumns().clear();
        tabListagemStock.getColumns().addAll(CodProdutoCol, TipoPapelCol, QuantidadeFolhasCol, QuantidadeResmasCol, QuantidadeKgCol, pVendaCol);
    }

    @FXML
    private void btContaCorrente_OnAction(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ListagemDocumentosLinhas.fxml"));
            Parent root = loader.load();
            ListagemDocumentosLinhasController controller = loader.<ListagemDocumentosLinhasController>getController();
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

        } catch (IOException e) {
            Logger.getLogger(ListaUtilizadoresController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bindGrid();
        }
    }

    @FXML
    private void btDocumentosPendentes_OnAction(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ListaDocumentosPendentes.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");
            Stage stage = new Stage();
            stage.setTitle("Feira&Paper");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            Logger.getLogger(ListagemStockController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bindGrid();
        }
    }

    @FXML
    private void btFornecedores_OnAction(ActionEvent event) throws IOException {

        if (tabListagemStock.getSelectionModel().getSelectedItems().size() == 0) {
            Helper.showInfoAlert("Selecione o produto.");
            return;
        }
        try {
            ListaStock item = tabListagemStock.getSelectionModel().getSelectedItem();
            Produtos p = new Produtos(item.getIdProduto());
            if (p.isValid()) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditarProduto.fxml"));
                Parent root = loader.load();
                EditarProdutoController controller = loader.<EditarProdutoController>getController();

                controller.setProduto(p);
                controller.carregarObjetos();

                Scene scene = new Scene(root);
                scene.getStylesheets().add("/styles/Styles.css");

                Stage stage = new Stage();
                stage.setScene(scene);
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
            }
        } catch (IOException e) {
            Logger.getLogger(ListagemStockController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bindGrid();
        }

    }
}

