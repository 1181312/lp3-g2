/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.util.ArrayList;

/**
 *
 * @author ricardopinho
 */
public class Logger {

    public enum Level {
        ERROR,
        WARNING,
        INFO,
        CONFIG,
        FINE;
    }

    private String LevelToString(Logger.Level levelLog) {
        switch (levelLog) {
            case ERROR:
                return "ERROR";
            case WARNING:
                return "WARNING";
            case INFO:
                return "INFO";
            case CONFIG:
                return "CONFIG";
            case FINE:
                return "FINE";
        }
        return "";
    }

    public enum Action {

        TEST,
        UPLOAD,
        VALIDATOR,
        CONVERT,
        INSERT,
        UPDATE,
        DELETE,
        SELECT;
    } 
    private String ActionToString(Logger.Action actionLog) {
        switch (actionLog) {
            case TEST:
                return "TEST";
            case UPLOAD:
                return "UPLOAD";
            case VALIDATOR:
                return "VALIDATOR";
            case CONVERT:
                return "CONVERT";
            case INSERT:
                return "INSERT";
            case UPDATE:
                return "UPDATE";
            case DELETE:
                return "DELETE";
            case SELECT:
                return "SELECT";
        }
        return "";
    }

    public enum Type {

        JSON,
        XML,
        LOGIN,
        UTILIZADOR,
        PRODUTOS,
        DOCUMENTOS;
    }
    private String TypeToString(Logger.Type typeLog) {
        switch (typeLog) {
            case JSON:
                return "JSON";
            case XML:
                return "XML";
            case LOGIN:
                return "LOGIN";
            case UTILIZADOR:
                return "UTILIZADOR";
            case PRODUTOS:
                return "PRODUTOS";
            case DOCUMENTOS:
                return "DOCUMENTOS";
        }
        return "";
    }
    /**
     *
     * @param levelLog
     * @param actionLog
     * @param typeLog
     * @param message
     * @param idUtilizador
     */
    public Logger(Logger.Level levelLog, Logger.Action actionLog, Logger.Type typeLog, String message, Integer idUtilizador) {

        boolean success = false;

        ArrayList<String> parameters = new ArrayList();
        parameters.add(LevelToString(levelLog));
        parameters.add(ActionToString(actionLog));
        parameters.add(TypeToString(typeLog));
        parameters.add(message);

        String SQL_SP = "EXEC spLogInsert @levelLog=?, @actionLog=?, @typeLog=?, @message=?";

        if (idUtilizador != null) {
            parameters.add(idUtilizador.toString());
            SQL_SP += ", @idUtilizador=?";

        }
        int newIdlog = Helper.con.executeInsert(SQL_SP, parameters);
        if (newIdlog != 0) {
            success = true;
        }
    }

}
