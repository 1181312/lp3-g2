/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

/**
 *
 * @author helder.oliveira
 */
public class UnitConvert {
    private static final int FOLHAS_POR_RESMA = 500;

    /**
     * Função para converter resmas para folhas.
     * @param reamsQuantity
     * @return 
     */
    public static int fromReamsToSheets(int reamsQuantity) {
        return (reamsQuantity * FOLHAS_POR_RESMA);
    }
    
    /**
     * Função para converter resmas para gramas.
     * @param reamsQuantity
     * @param sheetWeight
     * @return 
     */
    public static double fromReamsToGrams(int reamsQuantity, double sheetWeight) {
        return (fromReamsToSheets(reamsQuantity) * sheetWeight);
    }
   
    /**
     * Função para converter folhas para resmas.
     * @param sheetsQuantity
     * @return 
     */
    public static int fromSheetsToReams(int sheetsQuantity) {
        return (int) Math.floor(sheetsQuantity / FOLHAS_POR_RESMA);
    }
    
    /**
     * Função para converter folhas para gramas.
     * @param sheetsQuantity
     * @param sheetWeight
     * @return 
     */
    public static double fromSheetsToGrams(int sheetsQuantity, double sheetWeight) {
        return (sheetsQuantity * sheetWeight);
    }
    
    /**
     * Função para converter gramas para resmas.
     * @param totalWeightUnit
     * @param totalWeight
     * @param sheetWeight
     * @return 
     */
    public static int fromGramsToReams(String totalWeightUnit , double totalWeight, double sheetWeight) {
        return (int) Math.floor(fromGramsToSheets(totalWeightUnit, totalWeight, sheetWeight) / FOLHAS_POR_RESMA);
    }
    
    /**
     * Função para converter gramas para folhas.
     * @param totalWeightUnit
     * @param totalWeight
     * @param sheetWeight
     * @return 
     */
    public static int fromGramsToSheets(String totalWeightUnit , double totalWeight, double sheetWeight) {
        switch(totalWeightUnit.toUpperCase()) {
            case "KILOGRAM":
              return (int)((totalWeight * 1000) / sheetWeight);
            case "DECIGRAM":
              return (int)((totalWeight * 0.1) / sheetWeight);
            default:
              return (int)((totalWeight * 1) / sheetWeight);
        }
    }
    
    /**
     * Função para converter resmas para:
     *  - Folhas
     *  - Gramas
     * @param reamsQuantity
     * @param sheetWeight
     * @return 
     */
    public static UnitConvertResult fromReamsToAny(int reamsQuantity, double sheetWeight) {
        int reams = reamsQuantity;
        int reamsRestInSheets = 0;
        int sheets = fromReamsToSheets(reamsQuantity);
        double grams = fromSheetsToGrams(sheets, sheetWeight);
        
        UnitConvertResult result = new UnitConvertResult(reams, reamsRestInSheets, sheets, grams);
        return result;
    }
    
    /**
     * Função para converter folhas para:
     *  - Resmas
     *  - Gramas
     * 
     * Devolve também o número de folhas que fica a sobrar para completar uma resma.
     * @param sheetsQuantity
     * @param sheetWeight
     * @return 
     */
    public static UnitConvertResult fromSheetsToAny(int sheetsQuantity, double sheetWeight) {
        int reams = fromSheetsToReams(sheetsQuantity);
        int reamsRestInSheets = (sheetsQuantity % FOLHAS_POR_RESMA);
        int sheets = sheetsQuantity;
        double grams = fromSheetsToGrams(sheetsQuantity, sheetWeight);
        
        UnitConvertResult result = new UnitConvertResult(reams, reamsRestInSheets, sheets, grams);
        return result;
    }
    
    /**
     * Função para converter gramas para:
     *  - Resmas
     *  - Folhas
     * 
     * Devolve também o número de folhas que fica a sobrar para completar uma resma.
     * @param totalWeightUnit
     * @param totalWeight
     * @param sheetWeight
     * @return 
     */
    public static UnitConvertResult fromGramsToAny(String totalWeightUnit , double totalWeight, double sheetWeight) {
        int reams = fromGramsToReams(totalWeightUnit, totalWeight, sheetWeight);
        int sheets = fromGramsToSheets(totalWeightUnit, totalWeight, sheetWeight);
        int reamsRestInSheets = (sheets % FOLHAS_POR_RESMA);
        double grams;
        switch(totalWeightUnit.toUpperCase()) {
            case "KILOGRAM":
              grams = (totalWeight * 1000);
              break;
            case "DECIGRAM":
              grams = (totalWeight * 0.1);
              break;
            default:
              grams = (totalWeight * 1);
              break;
        }
        
        UnitConvertResult result = new UnitConvertResult(reams, reamsRestInSheets, sheets, grams);
        return result;
    }
}
