/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.util.ArrayList;

/**
 *
 * @author helder.oliveira
 */
public class UnitConvertResult {
    private int _reams;
    private int _reamsRestInSheets;
    private int _sheets;
    private double _grams;
    
    public UnitConvertResult(int reams, int reamsRestInSheets, int sheets, double grams) {
        _reams = reams;
        _sheets = sheets;
        _grams = grams;
        _reamsRestInSheets = reamsRestInSheets;
    }
    
    public int getReams() {
        return _reams;
    } 
    
    public int getReamsRestInSheets() {
        return _reamsRestInSheets;
    } 
    
    public int getSheets() {
        return _sheets;
    }  
    
    public double getGrams() {
        return _grams;
    } 
    
    
    // APENAS PARA O XML/JSON
    public static class infoQuantity {

        private double qtd = 0.0;
        private String unidade = "";

        public double getQtd() {
            return qtd;
        }

        public String getUnidade() {
            return unidade;
        }
       
        public infoQuantity(double qtd, String unidade) {
            this.qtd = qtd;
            this.unidade = unidade;
        }

    }

    public static boolean validaQuantidades(ArrayList<infoQuantity> lstQtds, double peso) {
        boolean isValid = true;

        int h1 = 0, h2 = 0;

        switch (lstQtds.get(0).getUnidade()) {
            case "Ream":
                h1 = UnitConvert.fromReamsToSheets((int) (lstQtds.get(0).getQtd()));
                break;
            case "Kilogram":
                h1 = UnitConvert.fromGramsToSheets("Kilogram", lstQtds.get(0).getQtd(), peso);
                break;
            case "Sheet":
                h1 = (int) (lstQtds.get(0).getQtd());
                break;
        }

        for (int i = 1; i < lstQtds.size(); i++) {
            
            switch (lstQtds.get(i).getUnidade()) {
                case "Ream":
                    h2 = UnitConvert.fromReamsToSheets((int) (lstQtds.get(i).getQtd()));
                    break;
                case "Kilogram":
                    h2 = UnitConvert.fromGramsToSheets("Kilogram", lstQtds.get(i).getQtd(), peso);
                    break;
                case "Sheet":
                    h2 = (int) (lstQtds.get(i).getQtd());
                    break;
            }

            if (h1 != h2) {
                isValid = false;
            }
        }

        return isValid;
    }
}
