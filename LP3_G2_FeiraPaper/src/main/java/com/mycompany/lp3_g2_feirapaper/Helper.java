/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.Produtos;
import model.Utilizadores;

/**
 *
 * @author Hugo Costa (1181312)
 */
public class Helper {

    public static final ConnDB con = new ConnDB();
    public static Utilizadores LoginUser = null;

    /**
     * Valida a password enviada por parametros Apenas aceita passwords com 1
     * Maiscula, 1 Digito e 1 Carater Especial Minimo de 6 carateres
     *
     * @param strPassword
     * @return
     */
    public static boolean validarPassword(String strPassword) {
        String regexValidator = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*\\W).{6,}$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strPassword);
        boolean result = m.find();
        return result;
    }

    /**
     * Mensagem de alerta
     *
     * @param message
     */
    public static void showInfoAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Info");
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Mensage de erro
     *
     * @param message
     */
    public static void showErrorAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Error");
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Valida o username enviado por parametro Apenas aceita letras e numeros
     *
     * @param strUsername
     * @return
     */
    public static boolean validarUsername(String strUsername) {
        String regexValidator = "^([a-zA-Z0-9]+){3,32}$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }

    /**
     * Valida alteração no campo username enviado por parametro Apenas aceita
     * letras e numeros
     *
     * @param strUsername
     * @return
     */
    public static boolean validarChangeUsername(String strUsername) {
        String regexValidator = "^([a-zA-Z0-9]+){0,}$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }

    /**
     * Valida o nome do utilizador Aceita nomes"
     *
     * @param strUsername
     * @return
     */
    public static boolean validarNome(String strUsername) {
        String regexValidator = "(?=^.{2,255}$)^[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+(?:[ ](?:das?|dos?|de|e|[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+))*$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }

    /**
     * Valida o identificador
     *
     * @param strIdentificador
     * @return
     */
    public static boolean validarIdentificador(String strIdentificador) {
        boolean result = true;
        if (!validarInteger(strIdentificador)) {
            result = false;
        }
        if (strIdentificador.length() > 7) {
            result= false;
        }
        return result;
    }

    /**
     * Valida qualquer texto com base no regex enviado por parametro
     *
     * @param strUsername
     * @param regexValidator
     * @return
     */
    public static boolean validarTextoComRegex(String strUsername, String regexValidator) {
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }

    /**
     * Valida qualquer numero inteiro com base no texto enviado por parametro
     *
     * @param strNum
     * @return boolean
     */
    public static boolean validarInteger(String strNum) {
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    
    /**
     * Valida qualquer numero double com base no texto enviado por parametro
     *
     * @param strNum
     * @return boolean
     */
    public static boolean validarDouble(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    

    public static String cifrar(String salt, String plainText)
            throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");

        if (salt != null) {
            Integer intMiddle = plainText.length() / 2;
            plainText = insertString(plainText, salt, intMiddle);
        }
        md.update(plainText.getBytes());

        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return sb.toString();
    }

    /**
     * Gera um salt aleatorio
     *
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String gerarSalt() throws NoSuchAlgorithmException {
        int n = 10;
        byte[] array = new byte[256];
        new Random().nextBytes(array);

        String randomString = new String(array, Charset.forName("UTF-8"));

        StringBuffer r = new StringBuffer();

        String AlphaNumericString = randomString.replaceAll("[^A-Za-z0-9]", "");

        for (int k = 0; k < AlphaNumericString.length(); k++) {
            if (Character.isLetter(AlphaNumericString.charAt(k))
                    && (n > 0)
                    || Character.isDigit(AlphaNumericString.charAt(k))
                    && (n > 0)) {
                r.append(AlphaNumericString.charAt(k));
                n--;
            }
        }

        return r.toString();
    }

    /**
     * Insere um texto dentro de outro texto com base na posição enviada por
     * parametro
     *
     * @param originalString
     * @param stringToBeInserted
     * @param index
     * @return
     */
    public static String insertString(String originalString, String stringToBeInserted, Integer index) {

        String newString = new String();

        for (int i = 0; i < originalString.length(); i++) {
            newString += originalString.charAt(i);
            if (i == index) {
                newString += stringToBeInserted;
            }
        }

        return newString;
    }

    public static void showPage(String uri, AnchorPane anchor) {
        Parent root;
        try {
            anchor.getChildren().clear();
            root = FXMLLoader.load(Helper.class.getResource(uri));
            anchor.getChildren().add(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static Optional<ButtonType> showConfirmAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(title);
        alert.setContentText(message);
        
        Optional<ButtonType> result = alert.showAndWait();
        return result;
    }
    
    public static boolean ReferenceExists(String reference) {
        List result = Helper.con.executeQuery("SELECT nrDoc FROM Documentos WHERE nrDoc = '" + reference + "'");
        
        return (result != null && result.size() > 0);
    }
    
    public static boolean SupplierExists(String supplier) {
        List result = Helper.con.executeQuery("SELECT idUtilizador FROM Utilizadores WHERE identificador = '" + supplier + "'");
        
        return result != null && result.size() > 0;
    }
    
    public static double Round2DecimalPlaces(Double p){
       return (Math.round((p) * 100.0) / 100.0);
    }
    public static double Round4DecimalPlaces(Double p){
       return (Math.round((p) * 10000.0) / 10000.0);
    }
    public static String formatSheetsToString (UnitConvertResult p){
       return String.valueOf(p.getSheets());
    }
    public static String formatReamsToString (UnitConvertResult p){
       String result = String.valueOf(p.getReams());
        if (p.getReamsRestInSheets()!= 0) {
            result += " e " + p.getReamsRestInSheets() + " folhas";
        }
        return result;
    }
    public static String formatGramsToString (UnitConvertResult p){
        //Validar tipo de unidade? Atual de gramas para kilograma
        return String.valueOf(Round4DecimalPlaces((p.getGrams()) / 1000)) + "Kg";
    }
    public static String formatPriceToString (double price){
        //Faz sentido o preco ser com 4 casas? senao alterar para 2;
        return String.valueOf(Round4DecimalPlaces(price)) + "€";
    }
    public static String formatPriceToString (BigDecimal price){
        //Faz sentido o preco ser com 4 casas? senao alterar para 2;
        return String.valueOf(price) + "€";
    }
    
    public static BigDecimal StringToBigInteger (String price){
        BigDecimal bigDecimalCurrency = new BigDecimal(price);
        return bigDecimalCurrency;
    }
    
}
