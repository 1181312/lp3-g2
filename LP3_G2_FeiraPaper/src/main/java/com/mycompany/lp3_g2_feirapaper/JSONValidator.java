/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import model.Gramagem;
import model.IVA;
import model.Produtos;
import model.TipoFolhas;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author tiagosantos
 */
public class JSONValidator {
    
    private ArrayList<WarningLog> warnings = new ArrayList();

    public ArrayList<WarningLog> getWarnings() {
        return warnings;
    }

    private void addWarning(WarningLog wLog) {
        warnings.add(wLog);
        new Logger(Logger.Level.WARNING, Logger.Action.VALIDATOR, Logger.Type.JSON, wLog.message, null);
    }

    public String showWarningsMessage() {
        String msg = "";

        if (warnings.size() > 0) {
            for (WarningLog log : warnings) {
                if ("".equals(msg)) {
                    msg = log.toString();
                } else {
                    msg += "\n" + log.toString();
                }
            }
        }

        return msg;
    }

    public class WarningLog {

        private int lin = 0;
        private String message = "";

        public WarningLog(int linha, String mensagem) {
            this.lin = linha;
            this.message = mensagem;
        }

        @Override
        public String toString() {
            if (lin == 0) {
                return  message;                
            } else {
                return "Erro na linha " + lin + ". " + message;                
            }  
        }
    }
    
    public JSONObject JSONFileIsValid(File jsonFile) {
        JSONObject obj = null;
        try {
            JSONParser parser = new JSONParser();
            obj = (JSONObject) parser.parse(new FileReader(jsonFile));
        } catch (Exception ex) {
            obj =  null;
        }
        return obj;
    }

    
    public boolean validateStructeFromJSONFile(File jsonFile) {
        JSONObject jsonDoc = JSONFileIsValid(jsonFile);

        if (jsonDoc.size() == 0 ){
            return false;
        }
        
        if (((JSONObject) jsonDoc.get("OrderConfirmation")).size() == 0 ){
            return false;
        }
        
        return true;
    }
    
    public boolean validateHeaderFromJSONFile(File jsonFile, String idfornecedor) {
        JSONObject jsonDoc = JSONFileIsValid(jsonFile);
        
        try{
            if (jsonDoc.size() == 0 || jsonDoc.get("OrderConfirmation") == null){
                addWarning(new WarningLog(0, "A referência não é válida."));
                return false;
            }
            
            JSONObject OrderConfirmation = (JSONObject) jsonDoc.get("OrderConfirmation");
            if (OrderConfirmation.size() > 0 || OrderConfirmation.get("OrderConfirmationHeader") != null){
                JSONObject OrderConfirmationHeader = (JSONObject) OrderConfirmation.get("OrderConfirmationHeader");
                if (OrderConfirmationHeader.size() == 0){
                    addWarning(new WarningLog(0, "A referência não é válida."));
                    return false; 
                }
                
                // VALIDATE REFERENCE 
                if (OrderConfirmationHeader.get("OrderConfirmationReference") == null){
                    addWarning(new WarningLog(0, "A referência não é válida."));
                    return false;
                }else if (OrderConfirmationHeader.get("OrderConfirmationReference").toString().isEmpty()){
                    addWarning(new WarningLog(0, "A referência não é válida."));
                    return false;
                }else if (Helper.ReferenceExists(OrderConfirmationHeader.get("OrderConfirmationReference").toString())){
                    addWarning(new WarningLog(0, "A referência não é válida."));
                    return false;
                }
                
                //VALIDATE SUPPLIER
                if (OrderConfirmationHeader.get("SupplierParty") == null){
                    addWarning(new WarningLog(0, "O código fornecedor não é válido."));
                    return false;
                }else{
                    JSONObject SupplierParty = (JSONObject) OrderConfirmationHeader.get("SupplierParty");
                    if(SupplierParty.size() == 0){
                        addWarning(new WarningLog(0, "O código fornecedor não é válido."));
                        return false; 
                    }
                    
                    if(SupplierParty.get("PartyIdentifier") == null){
                        addWarning(new WarningLog(0, "O código fornecedor não é válido."));
                        return false;
                    }else{
                        JSONObject PartyIdentifier = (JSONObject) SupplierParty.get("PartyIdentifier");
                        if(PartyIdentifier.size() == 0){
                            addWarning(new WarningLog(0, "O código fornecedor não é válido."));
                            return false;
                        }
                        
                        
                        if (PartyIdentifier.get("Code") == null){
                            addWarning(new WarningLog(0, "O código fornecedor não é válido."));
                            return false;
                        }else if (!idfornecedor.equals(PartyIdentifier.get("Code").toString())){
                            addWarning(new WarningLog(0, "O utilizador autenticado não corresponde ao código fornecedor."));
                            return false;
                        }else if (!Helper.SupplierExists(PartyIdentifier.get("Code").toString())){
                            addWarning(new WarningLog(0, "O fornecedor não é válido."));
                            return false;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            return false;
        }
        
        return true;
    }
    
    
    private Produtos linProd;
    
    public boolean validateLinesFromJSONFile(File jsonFile, String idfornecedor) {
        JSONObject jsonDoc = JSONFileIsValid(jsonFile);
        int sucessLin = 0;        
        int count =0;
        try{
            if (jsonDoc.size() == 0 || jsonDoc.get("OrderConfirmation") == null){
                return false;
            }
            

            JSONObject OrderConfirmation = (JSONObject) jsonDoc.get("OrderConfirmation");
            if (OrderConfirmation.size() > 0 || OrderConfirmation.get("OrderConfirmationLineItem") != null){
                JSONArray OrderConfirmationLineItems = (JSONArray) OrderConfirmation.get("OrderConfirmationLineItem");
                if (OrderConfirmationLineItems.size() == 0){ return false; }   
                count = OrderConfirmationLineItems.size();
                double totalLin = 0, unitaryPrice = 0, quantity = 0;
                items:
                for (int index = 0; index < count; index++) {
                    
                    JSONObject OrderConfirmationLineItem = (JSONObject) OrderConfirmationLineItems.get(index);
                    
                    linProd = new Produtos();
                    ArrayList<UnitConvertResult.infoQuantity> lstQtds = new ArrayList();
                    totalLin = 0;
                    unitaryPrice = 0;
                    quantity = 0;
                    
                    //OrderConfirmationLineItemNumber
                    if (OrderConfirmationLineItem.get("OrderConfirmationLineItemNumber") == null){ return false; }
                    
                    //Product
                    if (OrderConfirmationLineItem.get("Product") == null){ return false; }
                    if (!validaProduto((JSONObject)OrderConfirmationLineItem.get("Product"), idfornecedor)){ return false; }
                    
                    //PriceDetails
                    if (OrderConfirmationLineItem.get("PriceDetails") == null){ return false; }
                    if (!validaPrecoUnitario((JSONObject)OrderConfirmationLineItem.get("PriceDetails"), unitaryPrice)){ return false; }
                    
                    //MonetaryAdjustment
                    if (OrderConfirmationLineItem.get("MonetaryAdjustment") == null){ return false; }
                    if (!validaIVA((JSONObject)OrderConfirmationLineItem.get("MonetaryAdjustment"), totalLin)){ return false; }
                    
                    //Quantity
                    if (OrderConfirmationLineItem.get("Quantity") == null){ return false; }
                    if (!validaQuantidade((JSONObject)OrderConfirmationLineItem.get("Quantity"), quantity, lstQtds)){ return false; }
                    
                    //InformationalQuantity
                    if (OrderConfirmationLineItem.get("InformationalQuantity") == null){ return false; }
                    if (!validaInfoQuantidade((JSONArray)OrderConfirmationLineItem.get("InformationalQuantity"), lstQtds)){ return false; }
    
                    
                    //Validar se o preço total corresponde ao preço unitário x quantidade, 
                    // caso não correspondam deve passar para o próximo produto e 
                    // guardar o produto inválido para posteriormente alertar o fornecedor.
                    if (totalLin != unitaryPrice * quantity) {
                        addWarning(new WarningLog(index + 1, "Preço total não coerente com o preço unitário."));
                        break;
                    }

                    if (lstQtds.size() > 0) {
                        // Validar as quantidades necessário verificar os métodos de conversão
                        if (!UnitConvertResult.validaQuantidades(lstQtds, linProd.getPeso())) {
                            addWarning(new WarningLog(index + 1, "Quantidades incoerentes."));
                            break;
                        }

                    }

                    sucessLin += 1;

                }                
            }
            
        } catch (Exception ex) {
            return false;
        }
        
        
        return count != 0 && count == sucessLin;
    }
    
    private boolean validaProduto(JSONObject obj, String idFornecedor) {
        
        String idProductBuyer = "", idProductSupp = "";
        int gramagem = 0, length = 0, width = 0;

        try{
            
            //ProductIdentifier
            if (obj.get("ProductIdentifier") !=  null){
                
                JSONArray ProductIdentifiers = (JSONArray)obj.get("ProductIdentifier");
                for (int index = 0; index < ProductIdentifiers.size(); index++){
                
                    JSONObject ProductIdentifier = (JSONObject)ProductIdentifiers.get(index);
                    switch (ProductIdentifier.get("Agency").toString()){
                        case "Buyer":
                                idProductBuyer = ProductIdentifier.get("Code").toString();
                            break;
                        case "Supplier":
                                idProductSupp = ProductIdentifier.get("Code").toString();
                            break;
                                
                    }
                }
            }
            
            //Paper
            if (obj.get("Paper") !=  null){
                JSONObject Paper = (JSONObject)obj.get("Paper");
                //PaperCharacteristics
                if (Paper.get("PaperCharacteristics") !=  null){
                    JSONObject PaperCharacteristics = (JSONObject)Paper.get("PaperCharacteristics");
                    //BasisWeight
                    if (PaperCharacteristics.get("BasisWeight") !=  null){
                        JSONObject BasisWeight = (JSONObject)PaperCharacteristics.get("BasisWeight");
                        //DetailValue
                        if (BasisWeight.get("DetailValue") !=  null){
                            JSONObject DetailValue = (JSONObject)BasisWeight.get("DetailValue");
                            //Value
                            if (DetailValue.get("Value") !=  null){
                                gramagem = Integer.parseInt(DetailValue.get("Value").toString());
                            }
                        }
                    }
                }
                //Sheet
                if (Paper.get("Sheet") !=  null){
                    JSONObject Sheet = (JSONObject)Paper.get("Sheet");
                    //SheetConversionCharacteristics
                    if (Sheet.get("SheetConversionCharacteristics") !=  null){
                        JSONObject SheetConversionCharacteristics = (JSONObject)Sheet.get("SheetConversionCharacteristics");
                        //SheetSize
                        if (SheetConversionCharacteristics.get("SheetSize") !=  null){
                            JSONObject SheetSize = (JSONObject)SheetConversionCharacteristics.get("SheetSize");
                            //Length
                            if (SheetSize.get("Length") !=  null){
                                JSONObject Length = (JSONObject)SheetSize.get("Length");
                                //Value
                                if (Length.get("Value") !=  null){
                                    JSONObject Value = (JSONObject)Length.get("Value");
                                    length = Integer.parseInt(Value.get("Value").toString());
                                }
                            }
                            //Width
                            if (SheetSize.get("Width") !=  null){
                                JSONObject Width = (JSONObject)SheetSize.get("Width");
                                //Value
                                if (Width.get("Value") !=  null){
                                    JSONObject Value = (JSONObject)Width.get("Value");
                                    width = Integer.parseInt(Value.get("Value").toString());
                                }
                            }
                        }
                    }
                }
            }
            
            if (idProductBuyer.isEmpty() && idProductSupp.isEmpty()){
                return false;
            }
            
            if (gramagem == 0) {
                addWarning(new WarningLog(0, "Produto não tem gramagem definida."));
                return false;
            }

            if (length == 0 || width == 0) {
                addWarning(new WarningLog(0, "Produto não tem tipo de folha definido."));
                return false;
            }
            
            Gramagem myGramagem = new Gramagem(gramagem);

            if (!myGramagem.isValid()) {
                addWarning(new WarningLog(0, "Gramagem não é válida."));
                return false;
            }
            TipoFolhas myTipoFolha = new TipoFolhas(length, width);

            if (!myTipoFolha.isValid()) {
                addWarning(new WarningLog(0, "Tipo de folha não é válida."));
                return false;
            }
            
            linProd = new Produtos(myGramagem.getIdGramagem(), myTipoFolha.getIdTipoFolha(), idFornecedor);
            if (!linProd.isValid()) {
                addWarning(new WarningLog(0, "Produto não é válido."));
                return false;
            }

            // Validar se o produto exista na BD
            // deve validar se o idProductBuyer é igual ao codProduto
            if (!linProd.getCodProduto().equals(idProductBuyer)) {
                addWarning(new WarningLog(0, "Produto não é válido.\nCódigo de produto \"Buyer\" não corresponde ao código de produto registado."));
                return false;
            }

        
        } catch (Exception ex) {
            return false;
        }
        
        return true;
    }
    
    private boolean validaPrecoUnitario(JSONObject obj, double precoUnitario) {
        double qtdUnitaria = 0;
        String tipoMoeda = "", UOM = "";
        
        try{
            //PricePerUnit
            if (obj.get("PricePerUnit") != null){
                JSONObject PricePerUnit = (JSONObject)obj.get("PricePerUnit");
                //CurrencyValue
                if (PricePerUnit.get("CurrencyValue") != null){
                    JSONObject CurrencyValue = (JSONObject)PricePerUnit.get("CurrencyValue");
                    //Value
                    if (CurrencyValue.get("Value") != null){
                        precoUnitario = Double.parseDouble(CurrencyValue.get("Value").toString());    
                    }
                    //CurrencyType
                    if (CurrencyValue.get("CurrencyType") != null){
                        tipoMoeda = CurrencyValue.get("CurrencyType").toString();
                    }
                }
                //Value
                if (PricePerUnit.get("Value") != null){
                    JSONObject Value = (JSONObject)PricePerUnit.get("Value");
                    //UOM
                    if (Value.get("UOM") != null){
                        UOM=Value.get("UOM").toString();
                    }
                    //Value
                    if (Value.get("Value") != null){
                        qtdUnitaria = Double.parseDouble(Value.get("Value").toString());
                    }
                }

                if (tipoMoeda.isEmpty()) {
                    addWarning(new WarningLog(0, "Preço unitário não tem tipo de moeda definido."));
                    return false;
                }
                if (UOM.isEmpty()) {
                    addWarning(new WarningLog(0, "Preço unitário não tem unidade de medida definida."));
                    return false;
                }

                if (qtdUnitaria <= 0) {
                    addWarning(new WarningLog(0, "Preço unitário não tem quantidade unitária definida."));
                    return false;
                }
                // Valida preço unitário são negativos, 
                // caso sejam negativos deve passar para o próximo produto e guardar o produto inválido para 
                // posteriormente alertar o fornecedor.
                if (precoUnitario < 0) {
                    addWarning(new WarningLog(0, "Preço unitário inferior a 0."));
                    return false;
                }    
            }
            
        } catch (Exception ex) {
            return false;
        }
        
        return true;
    }
    
    private boolean validaIVA(JSONObject obj, double total) {
        String tipoMoeda = "", localISO = "";
        double taxa = 0.0;

        try{
            //TaxAdjustment
            if (obj.get("TaxAdjustment") != null){
                JSONObject TaxAdjustment = (JSONObject) obj.get("TaxAdjustment");
                //TaxPercent
                if (TaxAdjustment.get("TaxPercent") != null){
                    taxa = Double.parseDouble(TaxAdjustment.get("TaxPercent").toString());
                }
                
                //TaxLocation
                if (TaxAdjustment.get("TaxLocation") != null){
                    localISO = TaxAdjustment.get("TaxLocation").toString();
                }
            }
           
            if (taxa < 0) {
                addWarning(new WarningLog(0, "IVA não definido."));
                return false;
            }

            IVA myIVA = new IVA(taxa, localISO);
            if (!myIVA.isValid()) {
                addWarning(new WarningLog(0, "IVA não é válida."));
                return false;
            }       
            
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    
    private boolean validaQuantidade(JSONObject obj, double quantidade, ArrayList<UnitConvertResult.infoQuantity> lst) {
        String UOM = "";
        
        try {
            if (obj.get("Value") != null){
                JSONObject Value = (JSONObject) obj.get("Value");
                //Value
                if (Value.get("Value") != null){
                    quantidade = Double.parseDouble(Value.get("Value").toString());
                }
                //UOM
                if (Value.get("UOM") != null){
                    UOM = Value.get("UOM").toString();
                }
            }
            
            if (UOM.isEmpty()) {
                addWarning(new WarningLog(0, "Unidade da quantidade inválida."));
                return false;
            }

            if (quantidade <= 0.0) {
                addWarning(new WarningLog(0, "Quantidade inválida."));
                return false;
            }

            lst.add(new UnitConvertResult.infoQuantity(quantidade, UOM));
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    
    private boolean validaInfoQuantidade(JSONArray objs, ArrayList<UnitConvertResult.infoQuantity> lst) {
        double quantidade;
        
        for (int index=0; index < objs.size(); index++){
            quantidade = 0.0;
            if(!validaQuantidade(((JSONObject) objs.get(index)), quantidade, lst)){
                return false;
            }
        }
        
        return true;
    }
}
