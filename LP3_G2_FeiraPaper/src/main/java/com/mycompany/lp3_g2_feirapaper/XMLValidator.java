/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.Gramagem;
import model.IVA;
import model.Produtos;
import model.ProdutosFornecedores;
import model.TipoFolhas;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author helder.oliveira
 */
public class XMLValidator {

    private ArrayList<WarningLog> warnings = new ArrayList();

    public ArrayList<WarningLog> getWarnings() {
        return warnings;
    }

    private void addWarning(WarningLog wLog) {
        warnings.add(wLog);
        new Logger(Logger.Level.WARNING, Logger.Action.VALIDATOR, Logger.Type.XML, wLog.message, null);
    }

    public String showWarningsMessage() {
        String msg = "";

        if (warnings.size() > 0) {
            for (WarningLog log : warnings) {
                if ("".equals(msg)) {
                    msg = log.toString();
                } else {
                    msg += "\n" + log.toString();
                }
            }
        }

        return msg;
    }

    public class WarningLog {

        private int lin = 0;
        private String message = "";

        public WarningLog(int linha, String mensagem) {
            this.lin = linha;
            this.message = mensagem;
        }

        @Override
        public String toString() {
            if (lin == 0) {
                return  message;                
            } else {
                return "Erro na linha " + lin + ". " + message;                
            }            
        }
    }

    /**
     * Função para converter o ficheiro XML para Document se estiver bem
     * formatado.
     *
     * @param xmlFile
     * @return
     */
    public Document XMLFileIsValid(File xmlFile) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document doc;

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(xmlFile);
        } catch (ParserConfigurationException ex) {
            return null;
        } catch (SAXException | IOException ex) {
            return null;
        }

        return doc;
    }

    public boolean validateStructeFromXMLFile(File xmlFile) {
        Document xmlDoc = XMLFileIsValid(xmlFile);

        if (xmlDoc.getElementsByTagName("OrderConfirmation").getLength() == 0) {
            //Não tem a tag "OrderConfirmation" --> Obrigatório.
            return false;
        }

        Element order = XMLHelper.getChildElement(xmlDoc.getElementsByTagName("OrderConfirmation"));
        return "Accepted".equals(XMLHelper.getAttributeValue(order, "OrderConfirmationStatusType"));
    }

    public boolean validateHeaderFromXMLFile(File xmlFile, String idfornecedor) {
        Document xmlDoc = XMLFileIsValid(xmlFile);

        // Dados da Encomenda.
        if (xmlDoc.getElementsByTagName("OrderConfirmationHeader").getLength() == 0
                || !"Accepted".equals(xmlDoc.getElementsByTagName("OrderConfirmationHeader").item(0).getAttributes().getNamedItem("OrderConfirmationHeaderStatusType").getNodeValue())) {
            //Não tem a tag "OrderConfirmationHeader" --> Obrigatório.
            //OU a encomenda não está no estado "Accepted".
            addWarning(new WarningLog(0, "A tag OrderConfirmationHeader não existe ou a encomenda ainda não foi aceite."));
            return false;
        }

        // Dados da referência da encomenda
        if (xmlDoc.getElementsByTagName("OrderConfirmationReference").getLength() == 0
                || "".equals(xmlDoc.getElementsByTagName("OrderConfirmationReference").item(0).getTextContent())) {
            //Não tem a tag da Referência da encomenda ou está inválida.
            addWarning(new WarningLog(0, "A referência não é válida."));
            return false;
        }

        // Validar se a referência da encomenda já existe na BD
        String reference = xmlDoc.getElementsByTagName("OrderConfirmationReference").item(0).getTextContent();
        if (Helper.ReferenceExists(reference)) {
            // Se existir retorna false
            addWarning(new WarningLog(0, "A referência da encomenda já existe."));
            return false;
        }

        // Dados do Fornecedor
        if (xmlDoc.getElementsByTagName("SupplierParty").getLength() == 0) {
            //Não tem a tag inicial dos dados do Fornecedor.
            addWarning(new WarningLog(0, "O fornecedor não é válido."));
            return false;

        }

        // Dados da referência do fornecador
        if (xmlDoc.getElementsByTagName("PartyIdentifier").getLength() == 0
                || !"AssignedByBuyer".equals(xmlDoc.getElementsByTagName("PartyIdentifier").item(0).getAttributes().getNamedItem("PartyIdentifierType").getNodeValue())
                || "".equals(xmlDoc.getElementsByTagName("PartyIdentifier").item(0).getAttributes().getNamedItem("PartyIdentifierType").getTextContent())) {
            //Referência do Fornecedor inválida.
            addWarning(new WarningLog(0, "A referência do fornecedor não é válida."));
            return false;
        }

        // Validar se o fornecedor da encomenda já existe na BD
        String supplier = xmlDoc.getElementsByTagName("PartyIdentifier").item(0).getTextContent();
        if (!idfornecedor.equals(supplier)){
            addWarning(new WarningLog(0, "O utilizador autenticado não corresponde ao código fornecedor."));
            return false;
        }
        if (!Helper.SupplierExists(supplier)) {
            // Se não existir retorna false
            addWarning(new WarningLog(0, "O fornecedor não é válido."));
            return false;
        }

        return true;
    }

    private Produtos linProd = new Produtos();

    /**
     * Função para validar as linhas do ficheiro XML
     *
     * @param xmlFile
     * @return
     */
    public boolean validateLinesFromXMLFile(File xmlFile, String idfornecedor) {
        Document xmlDoc = XMLFileIsValid(xmlFile);

        int sucessLin = 0;
        Element order = XMLHelper.getChildElement(xmlDoc.getElementsByTagName("OrderConfirmation"));
        int count = order.getElementsByTagName("OrderConfirmationLineItem").getLength();
        if (count > 0) {
            double totalLin = 0, unitaryPrice = 0, quantity = 0;

            items:
            for (int index = 0; index < count; index++) {
                Element lin = XMLHelper.getChildElement(order.getElementsByTagName("OrderConfirmationLineItem"), index);
                linProd = new Produtos();
                ArrayList<UnitConvertResult.infoQuantity> lstQtds = new ArrayList();
                totalLin = 0;
                unitaryPrice = 0;
                quantity = 0;

                for (int indexLine = 0; indexLine < lin.getChildNodes().getLength(); indexLine++) {
                    Element linItem = XMLHelper.getChildElement(lin.getChildNodes(), indexLine);
                    if (linItem != null) {

                        switch (linItem.getNodeName()) {
                            case "Product":
                                if (!validaProduto(linItem, index + 1,idfornecedor)) {
                                    break items;
                                }
                                break;
                            case "PriceDetails":
                                if (!validaPrecoUnitario(linItem, index + 1, unitaryPrice)) {
                                    break items;
                                }
                                break;
                            case "MonetaryAdjustment":
                                if (!validaIVA(linItem, index + 1, totalLin)) {
                                    break items;
                                }
                                break;
                            case "Quantity":
                                if (!validaQuantidade(linItem, index + 1, quantity, lstQtds)) {
                                    break items;
                                }
                                break;
                            case "InformationalQuantity":
                                if (!validaInfoQuantidade(linItem, index + 1, lstQtds)) {
                                    break items;
                                }
                                break;
                        }
                    }

                }

                //Validar se o preço total corresponde ao preço unitário x quantidade, 
                // caso não correspondam deve passar para o próximo produto e 
                // guardar o produto inválido para posteriormente alertar o fornecedor.
                if (totalLin != unitaryPrice * quantity) {
                    addWarning(new WarningLog(index + 1, "Preço total não coerente com o preço unitário."));
                    break;
                }

                if (lstQtds.size() > 0) {
                    // Validar as quantidades necessário verificar os métodos de conversão
                    if (!UnitConvertResult.validaQuantidades(lstQtds, linProd.getPeso())) {
                        addWarning(new WarningLog(index + 1, "Quantidades incoerentes."));
                        break;
                    }

                }

                sucessLin += 1;
            }
        }

        return count != 0 && count == sucessLin;
    }

    /**
     * Validar a linha do produto no XML Valida o código de produto, a gramagem
     * e o tipo de folha
     *
     * @param product
     * @param linha
     * @return
     */
    private boolean validaProduto(Element product, int linha, String idFornecedor) {
        boolean valid = true;
        String idProductBuyer = "", idProductSupp = "";
        int gramagem = 0, length = 0, width = 0;

        if (product.hasChildNodes()) {
            NodeList childItens = product.getChildNodes();
            for (int index = 0; index < childItens.getLength(); index++) {
                Element lin = XMLHelper.getChildElement(childItens, index);
                if (lin != null) {
                    switch (lin.getNodeName()) {
                        case "ProductIdentifier":
                            if (lin.hasAttributes()) {
                                if ("PartNumber".equals(XMLHelper.getAttributeValue(lin, "ProductIdentifierType"))) {
                                    if ("Buyer".equals(XMLHelper.getAttributeValue(lin, "Agency"))) {
                                        idProductBuyer = lin.getTextContent();
                                    }
                                    if ("Supplier".equals(XMLHelper.getAttributeValue(lin, "Agency"))) {
                                        idProductSupp = lin.getTextContent();
                                    }
                                }
                            }
                            break;
                        case "Paper":
                            if (lin.hasChildNodes()) {
                                NodeList paperChildItens = lin.getChildNodes();
                                for (int indexPaper = 0; indexPaper < paperChildItens.getLength(); indexPaper++) {
                                    Element paper = XMLHelper.getChildElement(paperChildItens, indexPaper);
                                    if (paper != null) {
                                        switch (paper.getNodeName()) {
                                            case "PaperCharacteristics":
                                                if (paper.hasChildNodes()) {
                                                    if (paper.getElementsByTagName("BasisWeight").getLength() > 0) {
                                                        Element basisWeight = XMLHelper.getChildElement(paper.getElementsByTagName("BasisWeight"));
                                                        if (basisWeight.getElementsByTagName("DetailValue").getLength() > 0) {
                                                            if ("GramsPerSquareMeter".equals(XMLHelper.getAttributeValueByTagName(basisWeight, "DetailValue", "UOM"))) {
                                                                gramagem = Integer.parseInt(XMLHelper.getValueByTagName(basisWeight, "DetailValue"));
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case "Sheet":
                                                if (paper.hasChildNodes()) {
                                                    if (paper.getElementsByTagName("SheetConversionCharacteristics").getLength() > 0) {
                                                        Element sheetConversionCharacteristics = XMLHelper.getChildElement(paper.getElementsByTagName("SheetConversionCharacteristics"));
                                                        if (sheetConversionCharacteristics.getElementsByTagName("SheetSize").getLength() > 0) {
                                                            Element sheetSize = XMLHelper.getChildElement(sheetConversionCharacteristics.getElementsByTagName("SheetSize"));
                                                            if (sheetSize.getElementsByTagName("Length").getLength() > 0) {
                                                                Element lengthElement = XMLHelper.getChildElement(sheetSize.getElementsByTagName("Length"));
                                                                if (lengthElement.getElementsByTagName("Value").getLength() > 0) {
                                                                    length = Integer.parseInt(XMLHelper.getValueByTagName(lengthElement, "Value"));
                                                                }
                                                            }
                                                            if (sheetSize.getElementsByTagName("Width").getLength() > 0) {
                                                                Element widthElement = XMLHelper.getChildElement(sheetSize.getElementsByTagName("Width"));
                                                                if (widthElement.getElementsByTagName("Value").getLength() > 0) {
                                                                    width = Integer.parseInt(XMLHelper.getValueByTagName(widthElement, "Value"));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }

        if ("".equals(idProductBuyer) && "".equals(idProductSupp)) {
            addWarning(new WarningLog(linha, "Produto não tem identificação nem de \"Buyer\" nem de \"Supplier\"."));
            valid = false;
        }

        if (gramagem == 0) {
            addWarning(new WarningLog(linha, "Produto não tem gramagem definida."));
            valid = false;
        }

        if (length == 0 || width == 0) {
            addWarning(new WarningLog(linha, "Produto não tem tipo de folha definido."));
            valid = false;
        }

        // Validar se valores da 
        // gramagem(papercharacteristics) e tipo de folha(sheetsize) existe na base de dados, 
        // caso não coincidam deve passar para o próximo produto e guardar o produto inválido para 
        // posteriormente alertar o fornecedor.
        Gramagem myGramagem = new Gramagem(gramagem);

        if (!myGramagem.isValid()) {
            addWarning(new WarningLog(linha, "Gramagem não é válida."));
            valid = false;
        }
        TipoFolhas myTipoFolha = new TipoFolhas(length, width);

        if (!myTipoFolha.isValid()) {
            addWarning(new WarningLog(linha, "Tipo de folha não é válida."));
            valid = false;
        }

        if (valid) {
            // Validar se o produto existe na aplicação,
            // deve validar se a gramagem e o tipo de folha coincidem com o que está na aplicação,
            // caso não coincidam deve passar para o próximo produto e guardar o produto inválido para
            // posteriormente alertar o fornecedor.
            linProd = new Produtos(myGramagem.getIdGramagem(), myTipoFolha.getIdTipoFolha(), idFornecedor);
            if (!linProd.isValid()) {
                addWarning(new WarningLog(linha, "Produto não é válido."));
                valid = false;
            }

            // Validar se o produto exista na BD
            // deve validar se o idProductBuyer é igual ao codProduto
            if (!linProd.getCodProduto().equals(idProductBuyer)) {
                addWarning(new WarningLog(linha, "Produto não é válido.\nCódigo de produto \"Buyer\" não corresponde ao código de produto registado."));
                valid = false;
            }
        }

        return valid;
    }

    /**
     * Validar a linha do preço unitário no XML Valida tipo de moeda e unidade
     * da quantidade
     *
     * @param price
     * @param linha
     * @return
     */
    private boolean validaPrecoUnitario(Element price, int linha, double precoUnitario) {
        boolean valid = true;

        double qtdUnitaria = 0;
        String tipoMoeda = "", UOM = "";

        if (price.hasChildNodes()) {
            if (price.getElementsByTagName("PricePerUnit").getLength() > 0) {
                Element pricePerUnit = XMLHelper.getChildElement(price.getElementsByTagName("PricePerUnit"));
                if (pricePerUnit.getElementsByTagName("CurrencyValue").getLength() > 0) {
                    precoUnitario = Double.parseDouble(XMLHelper.getValueByTagName(pricePerUnit, "CurrencyValue"));
                    tipoMoeda = XMLHelper.getAttributeValueByTagName(pricePerUnit, "CurrencyValue", "CurrencyType");
                }
                if (pricePerUnit.getElementsByTagName("Value").getLength() > 0) {
                    qtdUnitaria = Integer.parseInt(XMLHelper.getValueByTagName(pricePerUnit, "Value"));
                    UOM = XMLHelper.getAttributeValueByTagName(pricePerUnit, "Value", "UOM");
                }
            }
        }

        if ("".equals(tipoMoeda)) {
            addWarning(new WarningLog(linha, "Preço unitário não tem tipo de moeda definido."));
            valid = false;
        }
        if ("".equals(UOM)) {
            addWarning(new WarningLog(linha, "Preço unitário não tem unidade de medida definida."));
            valid = false;
        }

        if (qtdUnitaria <= 0) {
            addWarning(new WarningLog(linha, "Preço unitário não tem quantidade unitária definida."));
            valid = false;
        }
        // Valida preço unitário são negativos, 
        // caso sejam negativos deve passar para o próximo produto e guardar o produto inválido para 
        // posteriormente alertar o fornecedor.
        if (precoUnitario < 0) {
            addWarning(new WarningLog(linha, "Preço unitário inferior a 0."));
            valid = false;
        }

        return valid;
    }

    /**
     * Validar a taxa de IVA no XML
     *
     * @param iva
     * @param linha
     * @return
     */
    private boolean validaIVA(Element iva, int linha, double total) {
        boolean valid = true;

        String tipoMoeda = "", localISO = "";
        double taxa = 0.0;
        if (iva.hasChildNodes()) {
            if (iva.getElementsByTagName("MonetaryAdjustmentStartAmount").getLength() > 0) {
                Element monetaryAdjustmentStartAmount = XMLHelper.getChildElement(iva.getElementsByTagName("MonetaryAdjustmentStartAmount"));
                if (monetaryAdjustmentStartAmount.getElementsByTagName("CurrencyValue").getLength() > 0) {
                    total = Double.parseDouble(XMLHelper.getValueByTagName(monetaryAdjustmentStartAmount, "CurrencyValue"));
                    tipoMoeda = XMLHelper.getAttributeValueByTagName(monetaryAdjustmentStartAmount, "CurrencyValue", "CurrencyType");
                }
            }

            if (iva.getElementsByTagName("TaxAdjustment").getLength() > 0) {
                Element taxAdjustment = XMLHelper.getChildElement(iva.getElementsByTagName("TaxAdjustment"));
                if (taxAdjustment.getElementsByTagName("TaxPercent").getLength() > 0) {
                    taxa = Double.parseDouble(XMLHelper.getValueByTagName(taxAdjustment, "TaxPercent"));
                }
                if (taxAdjustment.getElementsByTagName("TaxLocation").getLength() > 0) {
                    localISO = XMLHelper.getValueByTagName(taxAdjustment, "TaxLocation");
                }
            }
        }

        // Validar se IVA é positivo e se a tag existe, 
        // caso não seja positivo ou a tag não exista 
        // deve passar para o próximo produto e guardar o produto inválido 
        // para posteriormente alertar o fornecedor.
        if (taxa < 0) {
            addWarning(new WarningLog(linha, "IVA não definido."));
            valid = false;
        }

        IVA myIVA = new IVA(taxa, localISO);
        if (!myIVA.isValid()) {
            addWarning(new WarningLog(linha, "IVA não é válida."));
            valid = false;
        }

        return valid;
    }

    /**
     * Validar a quantidade no XML Valida a unidade e a quantidade
     *
     * @param qtd
     * @param linha
     * @return
     */
    private boolean validaQuantidade(Element qtd, int linha, double quantidade, ArrayList<UnitConvertResult.infoQuantity> lst) {
        boolean valid = true;

        String UOM = "";

        if (qtd.hasChildNodes()) {
            if (qtd.getElementsByTagName("Value").getLength() > 0) {
                if (!"".equals(XMLHelper.getAttributeValueByTagName(qtd, "Value", "UOM"))) {
                    UOM = XMLHelper.getAttributeValueByTagName(qtd, "Value", "UOM");
                }
                quantidade = Double.parseDouble(XMLHelper.getValueByTagName(qtd, "Value"));
            }
        }

        if ("".equals(UOM)) {
            addWarning(new WarningLog(linha, "Unidade da quantidade inválida."));
            valid = false;
        }

        if (quantidade <= 0) {
            addWarning(new WarningLog(linha, "Quantidade inválida."));
            valid = false;
        }

        if (valid) {
            lst.add(new UnitConvertResult.infoQuantity(quantidade, UOM));
        }

        return valid;
    }

    /**
     * Validar a informação da quantidade XML
     *
     * @param info
     * @param linha
     * @param lst
     * @return
     */
    private boolean validaInfoQuantidade(Element info, int linha, ArrayList<UnitConvertResult.infoQuantity> lst) {
        boolean valid = true;

        String UOM = "";
        double quantidade = 0.0;

        if (info.hasChildNodes()) {
            if (info.getElementsByTagName("Value").getLength() > 0) {
                if (!"".equals(XMLHelper.getAttributeValueByTagName(info, "Value", "UOM"))) {
                    UOM = XMLHelper.getAttributeValueByTagName(info, "Value", "UOM");
                }
                quantidade = Double.parseDouble(XMLHelper.getValueByTagName(info, "Value"));
            }
        }

        if ("".equals(UOM)) {
            addWarning(new WarningLog(linha, "Unidade da quantidade inválida."));
            valid = false;
        }

        if (quantidade <= 0) {
            addWarning(new WarningLog(linha, "Quantidade inválida."));
            valid = false;
        }

        if (valid) {
            lst.add(new UnitConvertResult.infoQuantity(quantidade, UOM));
        }

        return valid;

    }

    
}
