/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author tiagosantos
 */
public class XMLHelper {
    
    public static Element getChildElement(NodeList nodeParent){
        return getChildElement(nodeParent, 0);
    }
    
    public static Element getChildElement(NodeList nodeParent, int index){
        Node node = nodeParent.item(index);
        if (node.getNodeType() == Node.ELEMENT_NODE){
           return (Element) node;
        }
        return null;
    }
    
    public static String getValueByTagName(Element elemento, String tagName){
        return elemento.getElementsByTagName(tagName).item(0).getTextContent();
    }
    
    public static String getValueByTagName(Element elemento, String tagName, int index){
        return elemento.getElementsByTagName(tagName).item(index).getTextContent();
    }
    
    public static String getAttributeValueByTagName(Element elemento, String tagName, String attribute){
        return elemento.getElementsByTagName(tagName).item(0).getAttributes().getNamedItem(attribute).getNodeValue();
    }
    
    public static String getAttributeValue(Element elemento, String attribute){
        return elemento.getAttributes().getNamedItem(attribute).getNodeValue();
    }
    
}
