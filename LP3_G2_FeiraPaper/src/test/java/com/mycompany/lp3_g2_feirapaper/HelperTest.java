/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.util.Optional;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author helder.oliveira
 */
public class HelperTest {
    
    public HelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validarPassword method, of class Helper.
     */
    @Test
    public void testValidarPassword() {
        System.out.println("validarPassword");
        String strPassword = "Admini$trad0r";

        boolean result = Helper.validarPassword(strPassword);
        assertTrue(result);
    }
    
    /**
     * Test of validarUsername method, of class Helper.
     */
    @Test
    public void testValidarUsername() {
        System.out.println("validarUsername");
        String strUsername = "Helder";

        boolean result = Helper.validarUsername(strUsername);
        assertTrue(result);
    }

    /**
     * Test of validarChangeUsername method, of class Helper.
     */
    @Test
    public void testValidarChangeUsername() {
        System.out.println("validarChangeUsername");
        String strUsername = "Administrador";

        boolean result = Helper.validarChangeUsername(strUsername);
        assertTrue(result);
    }

    /**
     * Test of validarNome method, of class Helper.
     */
    @Test
    public void testValidarNome() {
        System.out.println("validarNome");
        String strUsername = "Aurélio Gosmão";

        boolean result = Helper.validarNome(strUsername);
        assertTrue(result);
    }

    /**
     * Test of validarIdentificador method, of class Helper.
     */
    @Test
    public void testValidarIdentificador() {
        System.out.println("validarIdentificador");
        String strIdentificador = "1234";

        boolean result = Helper.validarIdentificador(strIdentificador);
        assertTrue(result);
    }

    /**
     * Test of validarTextoComRegex method, of class Helper.
     */
    @Test
    public void testValidarTextoComRegex() {
        System.out.println("validarTextoComRegex");
        String strUsername = "T£st3_PWD";
        String regexValidator = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*\\W).{6,}$";

        boolean result = Helper.validarTextoComRegex(strUsername, regexValidator);
        assertTrue(result);
    }

    /**
     * Test of validarInteger method, of class Helper.
     */
    @Test
    public void testValidarInteger() {
        System.out.println("validarInteger");
        String strNum = "542353";

        boolean result = Helper.validarInteger(strNum);
        assertTrue(result);
    }

    /**
     * Test of cifrar method, of class Helper.
     */
    @Test
    public void testCifrar() throws Exception {
        System.out.println("cifrar");
        String salt = "s0mRIdlKvI";
        String plainText = "Admini$trad0r";
        
        String expResult = "601e043c8470e7c30e40ee6dbbc6a40a";
        
        String result = Helper.cifrar(salt, plainText);
        assertEquals(expResult, result);
    }

    /**
     * Test of gerarSalt method, of class Helper.
     */
    @Test
    public void testGerarSalt() throws Exception {
        System.out.println("gerarSalt");
        int expResult = 10;
        
        String result = Helper.gerarSalt();
        assertEquals(expResult, result.length());
    }

    /**
     * Test of insertString method, of class Helper.
     */
    @Test
    public void testInsertString() {
        System.out.println("insertString");
        String originalString = "Admini$trad0r";
        String stringToBeInserted = "s0mRIdlKvI";
        Integer index = 0;
        
        String expResult = "As0mRIdlKvIdmini$trad0r";
        
        String result = Helper.insertString(originalString, stringToBeInserted, index);
        assertEquals(expResult, result);
    }

    /**
     * Test of ReferenceExists method, of class Helper.
     */
    @Test
    public void testReferenceExists() {
        System.out.println("ReferenceExists");
        String reference = "D44235124";
        
        boolean result = Helper.ReferenceExists(reference);
        assertFalse(result);
    }

    /**
     * Test of SupplierExists method, of class Helper.
     */
    @Test
    public void testSupplierExists() {
        System.out.println("SupplierExists");
        String supplier = "353653";
        
        boolean result = Helper.SupplierExists(supplier);
        assertFalse(result);
    }
    
}
