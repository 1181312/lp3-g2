/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author helder.oliveira
 */
public class UnitConvertTest {
    
    public UnitConvertTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of fromReamsToSheets method, of class UnitConvert.
     */
    @Test
    public void testFromReamsToSheets() {
        System.out.println("fromReamsToSheets");
        int reamsQuantity = 5;
        int expResult = 2500;
        int result = UnitConvert.fromReamsToSheets(reamsQuantity);
        assertEquals(expResult, result);
    }

    /**
     * Test of fromReamsToGrams method, of class UnitConvert.
     */
    @Test
    public void testFromReamsToGrams() {
        System.out.println("fromReamsToGrams");
        int reamsQuantity = 5;
        double sheetWeight = 0.123;
        double expResult = 307.5; 
        double result = UnitConvert.fromReamsToGrams(reamsQuantity, sheetWeight);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of fromSheetsToReams method, of class UnitConvert.
     */
    @Test
    public void testFromSheetsToReams() {
        System.out.println("fromSheetsToReams");
        int sheetsQuantity = 2990;
        int expResult = 5;
        int result = UnitConvert.fromSheetsToReams(sheetsQuantity);
        assertEquals(expResult, result);
    }

    /**
     * Test of fromSheetsToGrams method, of class UnitConvert.
     */
    @Test
    public void testFromSheetsToGrams() {
        System.out.println("fromSheetsToGrams");
        int sheetsQuantity = 2750;
        double sheetWeight = 0.123;
        double expResult = 338.25;
        double result = UnitConvert.fromSheetsToGrams(sheetsQuantity, sheetWeight);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of fromGramsToReams method, of class UnitConvert.
     */
    @Test
    public void testFromGramsToReams() {
        System.out.println("fromGramsToReams");
        String totalWeightUnit = "Kilogram";
        double totalWeight = 300;
        double sheetWeight = 10;
        int expResult = 60;
        int result = UnitConvert.fromGramsToReams(totalWeightUnit, totalWeight, sheetWeight);
        assertEquals(expResult, result);
    }

    /**
     * Test of fromGramsToSheets method, of class UnitConvert.
     */
    @Test
    public void testFromGramsToSheets() {
        System.out.println("fromGramsToSheets");
        String totalWeightUnit = "Kilogram";
        double totalWeight = 300;
        double sheetWeight = 10;
        int expResult = 30000;
        int result = UnitConvert.fromGramsToSheets(totalWeightUnit, totalWeight, sheetWeight);
        assertEquals(expResult, result);
    }
  
    /**
     * Test of fromReamsToAny method, of class UnitConvert.
     */
    @Test
    public void testFromReamsToAny() {
        System.out.println("fromReamsToAny");
        int reamsQuantity = 60;
        double sheetWeight = 10.0;
        UnitConvertResult result = UnitConvert.fromReamsToAny(reamsQuantity, sheetWeight);
        assertThat(result, instanceOf(UnitConvertResult.class));
    }

    /**
     * Test of fromSheetsToAny method, of class UnitConvert.
     */
    @Test
    public void testFromSheetsToAny() {
        System.out.println("fromSheetsToAny");
        int sheetsQuantity = 30000;
        double sheetWeight = 10.0;
        UnitConvertResult result = UnitConvert.fromSheetsToAny(sheetsQuantity, sheetWeight);
        assertThat(result, instanceOf(UnitConvertResult.class));
    }

    /**
     * Test of fromGramsToAny method, of class UnitConvert.
     */
    @Test
    public void testFromGramsToAny() {
        System.out.println("fromGramsToAny");
        String totalWeightUnit = "Kilogram";
        double totalWeight = 300.0;
        double sheetWeight = 10.0;
        UnitConvertResult result = UnitConvert.fromGramsToAny(totalWeightUnit, totalWeight, sheetWeight);
        assertThat(result, instanceOf(UnitConvertResult.class));
    }
    
}
