/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.io.File;
import java.net.URL;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author helder.oliveira
 */
public class XMLHelperTest {
    
    Document xml;
    
    public XMLHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        URL url = getClass().getResource("/tests/enunciado.xml");
        File xmlFile = new File(url.getPath());

        
        XMLValidator validator = new XMLValidator();
        xml = validator.XMLFileIsValid(xmlFile);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getChildElement method, of class XMLHelper.
     */
    @Test
    public void testGetChildElementByNodeList() {
        System.out.println("getChildElement");
        NodeList nodeParent = xml.getElementsByTagName("OrderConfirmation");
        
        Element result = XMLHelper.getChildElement(nodeParent);
        assertTrue(result != null);
    }

    /**
     * Test of getChildElement method, of class XMLHelper.
     */
    @Test
    public void testGetChildElementByNodeListAndInt() {
        System.out.println("getChildElement");
        NodeList nodeParent = xml.getElementsByTagName("OrderConfirmation");
        int index = 0;

        Element result = XMLHelper.getChildElement(nodeParent, index);
        assertTrue(result != null);
    }

    /**
     * Test of getValueByTagName method, of class XMLHelper.
     */
    @Test
    public void testGetValueByTagNameByElementAndString() {
        System.out.println("getValueByTagName");
        Element order = XMLHelper.getChildElement(xml.getElementsByTagName("OrderConfirmation"));
        Element header = XMLHelper.getChildElement(order.getElementsByTagName("OrderConfirmationHeader"));
        String tagName = "OrderConfirmationReference";
        
        String expResult = "TS12901";
        
        String result = XMLHelper.getValueByTagName(header, tagName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getValueByTagName method, of class XMLHelper.
     */
    @Test
    public void testGetValueByTagNameByElementAndStringAndIndex() {
        System.out.println("getValueByTagName");
        
        Element order = XMLHelper.getChildElement(xml.getElementsByTagName("OrderConfirmation"));
        Element line = XMLHelper.getChildElement(order.getElementsByTagName("OrderConfirmationLineItem"), 0);
        Element product = XMLHelper.getChildElement(line.getElementsByTagName("Product"));
        String tagName = "ProductIdentifier";
        int index = 0;
        
        String expResult = "339437148";
        
        String result = XMLHelper.getValueByTagName(product, tagName, index);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAttributeValueByTagName method, of class XMLHelper.
     */
    @Test
    public void testGetAttributeValueByTagName() {
        System.out.println("getAttributeValueByTagName");
        Element order = XMLHelper.getChildElement(xml.getElementsByTagName("OrderConfirmation"));
        Element header = XMLHelper.getChildElement(order.getElementsByTagName("OrderConfirmationHeader"));
        Element supplier = XMLHelper.getChildElement(header.getElementsByTagName("SupplierParty"));
        Element nameAddress = XMLHelper.getChildElement(supplier.getElementsByTagName("NameAddress"));
        String tagName = "Country";
        String attribute = "ISOCountryCode";
        
        String expResult = "PT";
        
        String result = XMLHelper.getAttributeValueByTagName(nameAddress, tagName, attribute);
        assertEquals(expResult, result);
    }
}
