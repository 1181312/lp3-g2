/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import model.Utilizadores;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.w3c.dom.Document;

/**
 *
 * @author helder.oliveira
 */
public class XMLValidatorTest {
    
    File xmlFile;
    
    public XMLValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        URL url = getClass().getResource("/tests/enunciado.xml");
        xmlFile = new File(url.getPath());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of XMLFileIsValid method, of class XMLValidator.
     */
    @Test
    public void testXMLFileIsValid() {
        System.out.println("XMLFileIsValid");
        XMLValidator instance = new XMLValidator();

        Document result = instance.XMLFileIsValid(xmlFile);
        assertThat(result, instanceOf(Document.class));
    }

    /**
     * Test of validateStructeFromXMLFile method, of class XMLValidator.
     */
    @Test
    public void testValidateStructeFromXMLFile() {
        System.out.println("validateStructeFromXMLFile");
        XMLValidator instance = new XMLValidator();

        boolean result = instance.validateStructeFromXMLFile(xmlFile);
        assertTrue(result);
    }

    /**
     * Test of validateHeaderFromXMLFile method, of class XMLValidator.
     */
    @Test
    public void testValidateHeaderFromXMLFile() {
        System.out.println("validateHeaderFromXMLFile");
        XMLValidator instance = new XMLValidator();
        
        boolean result = instance.validateHeaderFromXMLFile(xmlFile, "11223");
        assertTrue(result);
    }

    /**
     * Test of validateLinesFromXMLFile method, of class XMLValidator.
     */
    @Test
    public void testValidateLinesFromXMLFile() {
        System.out.println("validateLinesFromXMLFile");
        XMLValidator instance = new XMLValidator();
     
        boolean result = instance.validateLinesFromXMLFile(xmlFile, "11223");
        assertTrue(result);
    }
    
}
