/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lp3_g2_feirapaper;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author helder.oliveira
 */
public class JSONValidatorTest {
    
    File jsonFile;
    
    public JSONValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        URL url = getClass().getResource("/tests/enunciado.json");
        jsonFile = new File(url.getPath());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of JSONFileIsValid method, of class JSONValidator.
     */
    @Test
    public void testJSONFileIsValid() {
        System.out.println("JSONFileIsValid");
        JSONValidator instance = new JSONValidator();
        
        JSONObject result = instance.JSONFileIsValid(jsonFile);
        assertThat(result, instanceOf(JSONObject.class));
    }

    /**
     * Test of validateStructeFromJSONFile method, of class JSONValidator.
     */
    @Test
    public void testValidateStructeFromJSONFile() {
        System.out.println("validateStructeFromJSONFile");
        JSONValidator instance = new JSONValidator();
        
        boolean result = instance.validateStructeFromJSONFile(jsonFile);
        assertTrue(result);
    }

    /**
     * Test of validateHeaderFromJSONFile method, of class JSONValidator.
     */
    @Test
    public void testValidateHeaderFromJSONFile() {
        System.out.println("validateHeaderFromJSONFile");
        JSONValidator instance = new JSONValidator();

        boolean result = instance.validateHeaderFromJSONFile(jsonFile, "44556");
        assertTrue(result);
    }

    /**
     * Test of validateLinesFromJSONFile method, of class JSONValidator.
     */
    @Test
    public void testValidateLinesFromJSONFile() {
        System.out.println("validateLinesFromJSONFile");
        JSONValidator instance = new JSONValidator();

        boolean result = instance.validateLinesFromJSONFile(jsonFile, "44556");
        assertTrue(result);
    }
    
}
