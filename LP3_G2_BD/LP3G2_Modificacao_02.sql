/****** Object:  Table [dbo].[Documentos]    Script Date: 19-11-2019 18:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Documentos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Documentos](
	[idDocumento] [int] IDENTITY(1,1) NOT NULL,
	[nrDoc] [varchar](15) NOT NULL,
	[Data] [datetime] NOT NULL,
	[idFornecedor] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[IVA]    Script Date: 19-11-2019 18:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IVA]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[IVA](
	[idIva] [int] IDENTITY(1,1) NOT NULL,
	[taxa] [decimal](18, 2) NOT NULL,
	[idPais] [int] NOT NULL,
 CONSTRAINT [PKIva] PRIMARY KEY CLUSTERED 
(
	[idIva] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Movimentos]    Script Date: 19-11-2019 18:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Movimentos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Movimentos](
	[idMovimento] [int] IDENTITY(1,1) NOT NULL,
	[idDocumento] [int] NOT NULL,
	[idProduto] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[pCusto] [decimal](18, 4) NOT NULL,
	[qt] [int] NOT NULL,
	[valido] [bit] NOT NULL,
	[idIva] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idMovimento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKIvaPaises]') AND parent_object_id = OBJECT_ID(N'[dbo].[IVA]'))
ALTER TABLE [dbo].[IVA]  WITH CHECK ADD  CONSTRAINT [FKIvaPaises] FOREIGN KEY([idPais])
REFERENCES [dbo].[Paises] ([idPais])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKIvaPaises]') AND parent_object_id = OBJECT_ID(N'[dbo].[IVA]'))
ALTER TABLE [dbo].[IVA] CHECK CONSTRAINT [FKIvaPaises]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKMovimentosDocumentos]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimentos]'))
ALTER TABLE [dbo].[Movimentos]  WITH CHECK ADD  CONSTRAINT [FKMovimentosDocumentos] FOREIGN KEY([idDocumento])
REFERENCES [dbo].[Documentos] ([idDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKMovimentosDocumentos]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimentos]'))
ALTER TABLE [dbo].[Movimentos] CHECK CONSTRAINT [FKMovimentosDocumentos]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKMovimentosProdutos]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimentos]'))
ALTER TABLE [dbo].[Movimentos]  WITH CHECK ADD  CONSTRAINT [FKMovimentosProdutos] FOREIGN KEY([idProduto])
REFERENCES [dbo].[Produtos] ([idProduto])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKMovimentosProdutos]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimentos]'))
ALTER TABLE [dbo].[Movimentos] CHECK CONSTRAINT [FKMovimentosProdutos]
GO
