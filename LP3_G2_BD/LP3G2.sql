--ALTER TABLE Utilizadores DROP CONSTRAINT FKUtilizadoresCidades;
--ALTER TABLE Cidades DROP CONSTRAINT FKCidadesPaises;
--ALTER TABLE Utilizadores DROP CONSTRAINT FKUtilizadoresTiposUtilizadores;
--ALTER TABLE ProdutosFornecedores DROP CONSTRAINT FKProdutosFornecedoresProdutos;
--ALTER TABLE ProdutosFornecedores DROP CONSTRAINT FKProdutosFornecedoresUtilizadores;
--ALTER TABLE Produtos DROP CONSTRAINT FKProdutosTiposFolhas;
--ALTER TABLE Produtos DROP CONSTRAINT FKProdutosGramagens;
--DROP TABLE Utilizadores;
--DROP TABLE Paises;
--DROP TABLE Cidades;
--DROP TABLE TiposUtilizadores;
--DROP TABLE Produtos;
--DROP TABLE ProdutosFornecedores;
--DROP TABLE TipoFolhas;
--DROP TABLE Gramagens;
CREATE TABLE Utilizadores (
  idUtilizador     int IDENTITY(1, 1) NOT NULL, 
  username         varchar(32) NOT NULL, 
  hashMD5          varchar(32) NOT NULL, 
  saltMD5          varchar(10) NOT NULL, 
  nome             varchar(255) NOT NULL, 
  morada1          varchar(255) NULL, 
  morada2          varchar(255) NULL, 
  idCidade         int NULL, 
  codPostal        varchar(20) NULL, 
  idTipoUtilizador int NOT NULL, 
  ativo            bit DEFAULT '1' NOT NULL, 
  CONSTRAINT PKUtilizadores 
    PRIMARY KEY (idUtilizador));
CREATE TABLE Paises (
  idPais         int NOT NULL, 
  ISO            varchar(2) NOT NULL, 
  descricao      varchar(255) NOT NULL, 
  codPostalRegex varchar(40) NOT NULL, 
  CONSTRAINT PKPaises 
    PRIMARY KEY (idPais));
CREATE TABLE Cidades (
  idCidade  int NOT NULL, 
  idPais    int NOT NULL, 
  descricao varchar(255) NOT NULL, 
  CONSTRAINT PKCidades 
    PRIMARY KEY (idCidade));
CREATE TABLE TiposUtilizadores (
  idTipoUtilizador int NOT NULL, 
  descricao        varchar(50) NOT NULL, 
  CONSTRAINT PKTiposUtilizadores 
    PRIMARY KEY (idTipoUtilizador));
CREATE TABLE Produtos (
  idProduto   int IDENTITY(1, 1) NOT NULL, 
  idTipoFolha int NOT NULL, 
  idGramagem  int NOT NULL, 
  peso        float(10) NOT NULL, 
  CONSTRAINT PKProdutos 
    PRIMARY KEY (idProduto));
CREATE TABLE ProdutosFornecedores (
  idProduto           int NOT NULL, 
  idFornecedor        int NOT NULL, 
  idProdutoFornecedor int NOT NULL, 
  CONSTRAINT PKProdutosFornecedores 
    PRIMARY KEY (idProduto, 
  idFornecedor));
CREATE TABLE TipoFolhas (
  idTipoFolha int NOT NULL, 
  descricao   varchar(3) NOT NULL, 
  largura     float(10) NOT NULL, 
  altura      float(10) NOT NULL, 
  CONSTRAINT PKTiposFolhas 
    PRIMARY KEY (idTipoFolha));
CREATE TABLE Gramagens (
  idGramagem int NOT NULL, 
  descricao  varchar(4) NOT NULL, 
  CONSTRAINT PKGramagens 
    PRIMARY KEY (idGramagem));
ALTER TABLE Utilizadores ADD CONSTRAINT FKUtilizadoresCidades FOREIGN KEY (idCidade) REFERENCES Cidades (idCidade);
ALTER TABLE Cidades ADD CONSTRAINT FKCidadesPaises FOREIGN KEY (idPais) REFERENCES Paises (idPais);
ALTER TABLE Utilizadores ADD CONSTRAINT FKUtilizadoresTiposUtilizadores FOREIGN KEY (idTipoUtilizador) REFERENCES TiposUtilizadores (idTipoUtilizador);
ALTER TABLE ProdutosFornecedores ADD CONSTRAINT FKProdutosFornecedoresProdutos FOREIGN KEY (idProduto) REFERENCES Produtos (idProduto);
ALTER TABLE ProdutosFornecedores ADD CONSTRAINT FKProdutosFornecedoresUtilizadores FOREIGN KEY (idFornecedor) REFERENCES Utilizadores (idUtilizador);
ALTER TABLE Produtos ADD CONSTRAINT FKProdutosTiposFolhas FOREIGN KEY (idTipoFolha) REFERENCES TipoFolhas (idTipoFolha);
ALTER TABLE Produtos ADD CONSTRAINT FKProdutosGramagens FOREIGN KEY (idGramagem) REFERENCES Gramagens (idGramagem);
