-- Paises
INSERT INTO [Paises] ([idPais], [ISO],[descricao],[codPostalRegex]) VALUES (1, 'PT','Portugal','^\d{4}(-\d{3})?$')

--Cidades
INSERT INTO [Cidades] ([idCidade],[idPais],[descricao]) VALUES (1,1,'Santa Maria da Feira')
INSERT INTO [Cidades] ([idCidade],[idPais],[descricao]) VALUES (2,1,'Oliveira de Azemeis')
INSERT INTO [Cidades] ([idCidade],[idPais],[descricao]) VALUES (3,1,'S�o Jo�o da Madeira')


--TipoFolhas
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (0,'A0',841,1189)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (1,'A1',594,841)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (2,'A2',420,594)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (3,'A3',297,420)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (4,'A4',210,297)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (5,'A5',148,210)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (6,'A6',105,148)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (7,'A7',74,105)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (8,'A8',52,74)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (9,'A9',37,52)
INSERT INTO [TipoFolhas]([idTipoFolha],[descricao],[largura],[altura]) VALUES (10,'A10',26,37)

--Gramagens
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (75,'gsm')
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (80,'gsm')
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (90,'gsm')
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (100,'gsm')
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (110,'gsm')
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (120,'gsm')
INSERT INTO [Gramagens]([idGramagem],[descricao]) VALUES (160,'gsm')

--Tipos Utilizadores
INSERT INTO [TiposUtilizadores]([idTipoUtilizador],[descricao]) VALUES (1,'Administrador')
INSERT INTO [TiposUtilizadores]([idTipoUtilizador],[descricao]) VALUES (2,'Operador')
INSERT INTO [TiposUtilizadores]([idTipoUtilizador],[descricao]) VALUES (3,'Fornecedor')

--Produtos
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 75,75.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 75,37.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 75,18.75)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 75,9.38)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 75,4.69)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 75,2.34)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 75,1.17)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 75,0.59)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 75,0.29)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 75,0.15)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 75,0.07)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 80,80.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 80,40.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 80,20.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 80,10.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 80,5.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 80,2.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 80,1.25)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 80,0.63)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 80,0.31)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 80,0.16)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 80,0.08)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 90,90.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 90,45.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 90,22.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 90,11.25)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 90,5.63)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 90,2.81)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 90,1.41)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 90,0.70)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 90,0.35)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 90,0.18)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 90,0.09)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 100,100.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 100,50.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 100,25.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 100,12.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 100,6.25)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 100,3.13)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 100,1.56)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 100,0.78)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 100,0.39)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 100,0.20)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 100,0.10)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 110,110.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 110,55.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 110,27.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 110,13.75)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 110,6.88)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 110,3.44)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 110,1.72)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 110,0.86)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 110,0.43)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 110,0.21)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 110,0.11)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 120,120.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 120,60.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 120,30.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 120,15.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 120,7.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 120,3.75)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 120,1.88)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 120,0.94)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 120,0.47)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 120,0.23)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 120,0.12)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (0, 160,160.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (1, 160,80.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (2, 160,40.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (3, 160,20.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (4, 160,10.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (5, 160,5.00)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (6, 160,2.50)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (7, 160,1.25)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (8, 160,0.63)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (9, 160,0.31)
INSERT INTO [dbo].[Produtos]([idTipoFolha],[idGramagem],[peso]) VALUES (10, 160,0.16)


--Utilizadores
INSERT INTO [dbo].[Utilizadores]([username],[hashMD5],[saltMD5],[nome],[morada1],[morada2],[idCidade],[codPostal],[idTipoUtilizador],[ativo])
     VALUES
           ('admin'
           ,'601e043c8470e7c30e40ee6dbbc6a40a' --password: Admini$trad0r
           ,'s0mRIdlKvI'
           ,'Sr. Norberto Faria Sentido'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,1
           ,1)


INSERT INTO [dbo].[Utilizadores]([username],[hashMD5],[saltMD5],[nome],[morada1],[morada2],[idCidade],[codPostal],[idTipoUtilizador],[ativo])
     VALUES
           ('operador'
           ,'4445be3c950df637620aa243cd7f5435' --password: Op�rad0r
           ,'r9nRLdsGvI'
           ,'Oper�rio 1'
           ,'Rua Feira Paper'
           ,NULL
           ,1
           ,'4321-321'
           ,2
           ,1)


--IVA
INSERT INTO [dbo].[IVA](taxa, idPais)
VALUES(23.00,1)
INSERT INTO [dbo].[IVA](taxa, idPais)
VALUES(0.00,1)