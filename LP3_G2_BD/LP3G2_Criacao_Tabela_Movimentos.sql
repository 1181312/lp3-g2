/****** Object:  Table [dbo].[Movimentos]    Script Date: 08/11/2019 21:42:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Movimentos](
	[nrDocumento] [int] NOT NULL,
	[idProduto] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[idFornecedor] [int] NULL,
	[pCusto] [decimal](18, 4) NOT NULL,
	[qt] [int] NOT NULL,
	[valido] [bit] NOT NULL,
 CONSTRAINT [PK_Movimentos] PRIMARY KEY CLUSTERED 
(
	[nrDocumento] ASC,
	[idProduto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Movimentos] ADD  CONSTRAINT [DF_Movimentos_valido]  DEFAULT ((0)) FOR [valido]
GO

ALTER TABLE [dbo].[Movimentos]  WITH NOCHECK ADD  CONSTRAINT [FK_Movimentos_Produtos] FOREIGN KEY([idProduto])
REFERENCES [dbo].[Produtos] ([idProduto])
GO

ALTER TABLE [dbo].[Movimentos] NOCHECK CONSTRAINT [FK_Movimentos_Produtos1]
GO

ALTER TABLE [dbo].[Movimentos]  WITH CHECK ADD  CONSTRAINT [FK_Movimentos_Utilizadores] FOREIGN KEY([idFornecedor])
REFERENCES [dbo].[Utilizadores] ([idUtilizador])
GO

ALTER TABLE [dbo].[Movimentos] CHECK CONSTRAINT [FK_Movimentos_Utilizadores]
GO


